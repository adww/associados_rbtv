---
layout: edition
title: O Traço de União da Áudio-descrição Versos e Controvérsias
date: 2009-12-01
categories: edition 1
cover: 01.png
issue: 01
release: 2009
authors:
 - Francisco J. Lima 
 - Rosângela A. F. Lima 
 - Paulo A. M. Vieira 

tags:
--- 

## Resumo
O presente artigo versa sobre a áudio-descrição, defendendo esse recurso assistivo como meio de acessibilidade à informação, à comunicação, à educação e à cultura para as pessoas com deficiência, mormente para as com deficiência visual. Ilustra o uso da áudio-descrição em vários países e diz como este direito tem sido requerido pelas pessoas com deficiência. Apresenta razões para que se escreva áudio-descrição com o hífen, colocando em debate a controvérsia sobre a grafia desse vocábulo, quando grafado sem o hífen. Conclui fazendo a assertiva de que a áudio-descrição é recurso para a acessibilidade e que “Acessibilidade” no cinema, no teatro e na televisão implica na oferta de áudio-descrição às pessoas com deficiência visual. Ao apresentar tal argumento, o presente artigo defende que com a áudio-descrição “Se você não vê, poderá ouvir; Se você não ouve, poderá ler; e Se você não lê, poderá compreender”. Acrescenta que, se o leitor ainda não se convenceu de que a áudio-descrição é acessibilidade à informação, à comunicação, à educação e à cultura, ele deve convencer-se desse direito e praticar essa acessibilidade, promovendo a cidadania, dignidade e respeito aos direitos da pessoa humana, também nos museus, no cinema, nos teatros e na televisão. 

## Abstract
The current article deals with audio description, upholding this assistive resource as a medium of accessibility to: information; communication; education and culture for people with disability, especially visually impaired people. It presents the employment of audio description in many countries, and shows how this assistive tool has been demanded by people with disability. It also points out grammatical reasons for writing the Portuguese word “audio-descrição” with hyphen, putting in debate the controversy around the spelling of this word, when written without hyphen. This article closes with the statement that audio description is a resource for accessibility which in cinemas, theaters, and television implies the offering of audio description for people with visual impairment, and doing so, the current article emphasizes: “If you don’t see you will be able to hear. If you can’t hear you will be able to read. If you can’t read you will be able to understand”. It adds: in case the reader isn’t yet convinced that audio description is accessibility to: information; communication; education and culture, he should get convinced of this right, and putting it into practice promotes: citizenship, dignity and respect for the rights of the human person, in museums, cinemas, theater rooms and television. 

## 1- Introdução

A narração (oral ou escrita) dos eventos tem sido uma opção há muito utilizada para dar a conhecer a quem deles não participou, viu ou observou.

Como sabemos das “Histórias” e lendas antigas, seriam através dos olhos e da fala de terceiros que se conheceriam as novas terras, a respeito das quais os viajantes que delas voltavam, contavam suas experiências e impressões. Contudo, tais narrativas eram entrecortadas por interpretações, muitas vezes imprecisas, isso quando não eram fantasiosas. Mesmo os registros, em forma de imagens pintadas, esculpidas ou tecidas, podiam representar o que se acreditava ter acontecido ou visto e não o que verdadeiramente ocorrera. 

Com o advento das tecnologias de registro audiovisuais, o gravador, a filmadora e as câmeras fotográficas, o acesso aos elementos sonoros e visuais dos eventos passaram a ser freqüentes e amplos, com mais precisão e fidedignidade da imagem vista.

No entanto, na medida em que se ia deixando a narração e se assumindo mais a apreciação da imagem, as pessoas com deficiência visual passavam a ficar mais e mais de fora do mundo das informações, da educação, da comunicação em geral, e do lazer, quando as imagens eram o foco das apresentações.

Nas últimas décadas, contudo, um movimento internacional, desencadeado nos Estados Unidos vem mudando essa situação.

Esteado na técnica da tradução visual e com o princípio de que todos devem ter pleno acesso à informação, à comunicação, à cultura, à educação e ao lazer, bem como com o entendimento de que cabe ao indivíduo decidir sobre o que quer, como quer, quando quer ter a acesso a tudo isso, começou-se a desenvolver o que veio a ser chamado de áudio-descrição.

Assim como a ortografia desse vocábulo apresenta um traço de união que nos remete a uma nova construção, a partir da composição de elementos distintos e com significados diversos bem conhecidos, o real sentido da áudio-descrição também nos remete a uma nova compreensão do direito à informação e à comunicação. Por conseguinte, o significado dos vocábulos áudio e descrição é bem mais que a união dos dois elementos que o compõem, não sendo, portanto, a mera narração de imagens visualmente inacessíveis aos que não enxergam. A áudio-descrição implica em oferecer aos usuários desse serviço as condições de igualdade e oportunidade de acesso ao mundo das imagens, garantindo-lhes o direito de concluírem por si mesmos o que tais imagens significam, a partir de suas experiências, de seu conhecimento de mundo e de sua cognição. 

Destarte, se de um lado as imagens como estão, são inacessíveis às pessoas com deficiência visual, de outro, expectadores com essa deficiência estão desejosos de acesso aos construtos visuais das obras e dos eventos que fazem parte de seu dia-a-dia.

> “Pessoas com deficiência reivindicam no Congresso legendas e áudio-descrição na televisão.
> MADRI, 23 de Setembro (EUROPA PRESS) -
> O presidente do Comitê Espanhol de Representantes de Pessoas com Deficiência (Cermi), Luis Cayo, solicitou nesta terça na Câmara dos Deputados a propositura de uma “legislação que obrigue as televisões, os meios áudio-visuais a fazer suas transmissões com legendas, em língua de sinais e com áudio-descrição.
> Durante sua intervenção na Comissão para as políticas de inclusão da pessoa com deficiência, Cayo destacou que as televisões acessíveis às pessoas com deficiência funcionam há vários anos em países como os Estados Unidos, Grã Bretanha ou França, todavia “na Espanha é um tema pendente”.
> “Esperamos que nesta legislatura seja saudada esta dívida”, asseverou. 

Sim, elas estão desejosas e conscientes do direito que têm à esse acesso. 

> RESOLUÇÃO 96-13 -TELEVISÃO DIGITAL
> 
> CONSIDERANDO que a televisão desempenha um importante papel para a educação e inclusão social e cultural e é o passatempo mais popular das pessoas com deficiência visual;
> 
> CONSIDERANDO que as pessoas com deficiência visual perdem informações cruciais devido ao grande conteúdo visual de programas de televisão;
> 
> CONSIDERANDO que a áudio-descrição (sic) (uma narração descrevendo ações, linguagem corporal, expressões faciais e cenários) já possibilita que milhares de pessoas com deficiência usufruam dos programas de televisão em alguns países; 
> 
> CONSIDERANDO que pesquisas de opinião mostram que a vasta maioria das pessoas com deficiência visual querem e precisam de áudio-descrição de programas de televisão; 
> 
> CONSIDERANDO que o artigo 27 da Declaração Universal dos Direitos Humanos reconhece o direito de todos de fazerem parte da vida cultural da comunidade e beneficiarem-se de avanços científicos;
> 
> CONSIDERANDO que em alguns países a legislação sobre transmissão televisiva reconhece o direito das pessoas com deficiência visual de usufruírem da televisão com áudio-descrição;
> 
> CONSIDERANDO que o advento da televisão digital (que fará mais canais disponíveis) fornece uma solução técnica para a transmissão e recepção da áudio-descrição; e 
> 
> CONSIDERANDO que os receptores digitais devem ser facilmente utilizáveis por pessoas com deficiência visual a fim de que a televisão digital lhes seja acessível;
> 
> Esta Quarta Assembléia Geral da União Mundial de Cegos, em convenção na cidade de Toronto, Canadá, decide: 
> 
> a. xigir dos legisladores que reconheçam o direito das pessoas com deficiência visual de usufruírem da televisão e que dediquem uma parte da capacidade de transmissão digital ao som da áudio-descrição à medida que e quando a televisão digital estiver sendo introduzida; <br>
> 
> b. Exigir dos canais de televisão que assumam um compromisso para com seus espectadores com deficiência no sentido de introduzirem serviços tão logo um método de transmissão do som da áudio-descrição torne-se disponível <br>
> 
> c. Exigir a cooperação dos fabricantes de receptores para que incorporem as necessidades das pessoas com deficiência visual ou com baixa visão no projeto dos receptores digitais. <br>
> 
> Quarta Assembléia da União Mundial de Cegos (WORLD BLIND UNION) <br>
> TORONTO, CANADÁ <br>
> 26-30 AGOSTO 1996


Por toda a parte, à busca por uma Sociedade Inclusiva, as pessoas com deficiência têm ocupado espaços e falado para serem escutadas. Suas falas, ainda que um tanto abafadas pelos modelos tradicionais de exclusão e de segregação, estão, contudo, sendo ouvidas cada vez mais. De fato, no que concerne ao acesso à imagens visuais, as pessoas com deficiência visual vêm logrando êxito, seja no estrangeiro, seja no Brasil. E isso pode ser visto nas programações de teatros e cinemas ao redor do mundo. 

No Caribe:

> Em Cuba, filmes com sistema de áudio-descrição para pessoas com deficiência visual.
> 
> ELIZABETH LÓPEZ CORZO
> 
> A Amostra de Cinema Cubano, com cinco filmes para cegos e pessoas com baixa visão foi apresentada hoje, na cidade de La Habana, mediante o sistema de áudio-descrição, método inovador na América Latina devido ao enfoque mais integral que lhe outorga a ilha.
> 
> José Luis Lobato, cineasta e promotor do projeto, mencionou que na Argentina houve um precedente dessa iniciativa mas, agora, Cuba é o único país que trabalha em profundidade com uma amostra cinematográfica desse tipo.
> 
> Este é um evento de importância mundial, uma vez que a nação caribenha o desenvolve tendo em vista o usufruto conjunto de videntes e não-videntes, completamente diferente do que ocorre em outras partes do mundo, que só se destina aqueles com deficiência visual e também aqui se promove a visita ao cinema como um centro cultural fora do âmbito doméstico, disse ele.
> 
> Carlos Abel Ramirez, chefe do Departamento de Cultura, Educação e Relações Públicas da Associação Nacional dos Cegos, classificou como muito positivo o projeto devido ao seu cunho social e humano, pois o objetivo desta entidade é a reabilitação integral de seus afiliados e de seu ambiente social.
> 
> Acrescentou que se amplia o conhecimento das pessoas com deficiência, enquanto o povo também se instrui sobre as necessidades dessas pessoas e em como podem ser satisfeitas.
> 
> O sistema supre, com a voz de um locutor, a falta de percepção das imagens, através de descrições sonoras complementares que detalham os gestos dos personagens, sua aparência, paisagens, e outros, e só foi desenvolvida nos Estados Unidos, Espanha e Itália.
> 
> Robert Smith, Vice Presidente do Instituto Cubano de Arte e Indústria Cinematográficas, afirmou que este é um método, que merece uma atenção para alcançar seu objetivo comunicativo, mantendo a sua integridade artística. Ele disse que em breve começarão a distribuir fitas em todos os conselhos de Cuba, através dos Centros Provinciales del Cine.
> 
> A continuidade do projeto é de extrema vontade de cultura do país, junto com versões de outros programas na telinha disse Jorge Gonzalez, chefe do Departamento de dublagem do Instituto Cubano de Rádio e TV, e comentou que a apresentação está prevista durante o verão, a partir da coleta em um canal nacional.
> 
> Até a data terão sido finalizadas as áudio-descrições de Viva Cuba, La bella del Alhambra, Clandestinos, Páginas del diario de Mauricio e Bailando Cha Cha Chá, enquanto se continua trabalhando nos títulos Se permuta, El brigadista e Fresa y Chocolate. 

No Havaí:

> Teatro Diamond Head para a Pessoa com Deficiência Visual
> 
> O Teatro Diamond Head está oferecendo áudio-descrição para as pessoas com deficiência visual no segundo domingo de cada apresentação ao vivo do teatro.
> 
> A seguinte lista de datas refere-se às suas apresentações durante esta estação. 
> 
> Título do Filme e Data da Apresentação
> 
> Meet Me in St. Louis - December 9 2007
> 
> Barefoot in the Park - February 10 2008
> 
> Flower Drum Song - March 30 2008
> 
> The Producers The Wizard of Oz - May 25 2008
> 
> The Wizard of Oz - July 20, 2008
> 
> Todas as apresentações são as 16:00 e oferecemos um disconto de $5 para as poltronas de $22 e $32, para as pessoas com deficiência visual.
> 
> Contato:
> 
> Melanie Garcia
> 
> Box Office Manager/Volunteer Coordinator
> 
> Diamond Head Theatre
> 
> 520 Makapuu Ave.
> 
> Honolulu, HI 96816
> 
> (808) 733-0277, x.310 

Na Catalunha:

> TV3 oferece 'La gran pellícula' com áudio-descrição para pessoas com deficiência visual através da TDT.
> 
> Desde 16 de fevereiro a TV3 transmite através da TDT, 'La Gran Pel.lícula' com a áudio-descrição para pessoas com deficiência visual. A televisão da Catalunha habilitou um canal de áudio específico para a áudio-descrição, como parte de um projeto que prevê um aumento progressivo da programação acessível a pessoas com deficiência visual. 
> 
> Toda semana, 'A Gran Pel.lícula’, que é apresentado sexta-feira à noite, irá incorporar comentários descritivos para que as pessoas com deficiência visual possam acompanhar tudo o que acontece e que só é apresentado através da imagem. Com este serviço de áudio, pessoas com deficiência visual podem obter informação sobre todos os elementos necessários para não perder o fio da meada do filme e também informações sobre as atitudes e expressões dos personagens, movimentos, paisagens, o figurino, e assim por diante.
> 
> TV3 fez os primeiros lançamentos de filmes com áudio-descrição no final da década de 80, e mais tarde ofereceram-se séries como 'plats bruts','Majoria absoluta’ e ‘L’un per l’autre’. Mas o projeto que agora se coloca em prática planeja oferecer uma programação regular e estável dirigida às pessoas com deficiência visual e ir ampliando-a. Para tornar possível esta expansão, a Televisão da Catalunha, criou um sistema tecnológico específico e apropriado. As áudio-descrições até agora vinham sendo transmitidas através do sistema dual, e isso implicava na eliminação da versão original ou o som estéreo do programa. 
> 
> Agora, graças às TDT, TVC, se pode destinar um canal de áudio exclusivamente para a áudio-descrição. Para ter acesso a este serviço apenas necessita-se de escolher o canal correspondente dentro do menu de áudios do receptor de TDT.
> 
> Há muitos anos, a acessibilidade é um objetivo prioritário da TVC. Diariamente, são oferecidos espaços informativos traduzidos para a língua de sinais catalã, e desde os primórdios do teletexto tem-se incentivado de modo muito importante a legendagem destinada às pessoas com deficiência auditiva.
> 
> A Legendagem foi inaugurada no ano de 1990, e desde então a oferta tem aumentado ano após ano. No ano de 2006, foram emitidas um total de mais de 10.000 horas legendadas no conjunto dos diferentes canais (analógicos e digitais). No que concerne à programação, nestes momentos é possível continuar com as legendas em qualquer tipo de programas: informativos, documentários, desenhos animados, séries, filmes, programas culturais e de entretenimento, etc.
> 
> No ano de 2003, também foi desenvolvido um sistema tecnológico próprio que permite legendar em tempo real programas ao vivo. O programa “Àgora” é oferecido com legendas, desde há quase dois anos, graças a este sistema, e em 2007, começou-se também a legendar o programa “El temps”. 

Em Portugal:

> TV Cabo e Canal Lusomundo Gallery adaptam filmes Portugueses para pessoas cegas
> 
> 
> No próximo dia 24 de Fevereiro, o canal Lusomundo Gallery apresenta o 15º filme português com áudio-descrição(sic)
> 
> A TV Cabo/Canal Lusomundo Gallery exibiu o primeiro filme com áudio-descrição em 3 Dezembro de 2004. O filme "O Pátio das Cantigas" tornou-se assim o primeiro filme português a incorporar a áudio-descrição. 
> 
> No próximo dia 24 de Fevereiro, o filme “O Grande Elias”, de Arthur Duarte, torna-se o 15º filme com áudio-descrição sincronizada à disposição no canal Lusomundo Gallery da TV Cabo. 
> 
> A um ritmo de um filme por mês, o leque dos filmes portugueses vão desde os clássicos, sendo o mais velho de 1935 ("As Pupilas do Senhor Reitor"), até aos filmes contemporâneos, com realizações datadas de 2003, como é o caso de "A Selva" de Leonel Vieira. 
> 
> Lista dos filmes com áudio-descrição 
> - "O Pátio das Cantigas" de Francisco Ribeiro (Ribeirinho), 1941 Duração: 125 min. Estreou a 3 Dezembro 2004 (Dia Internacional das Pessoas com Deficiência) 
> - "O Costa do Castelo" de Arthur Duarte, 1943 Duração: 125 min. Estreou a 28 Janeiro 2005 
> - "O Leão da Estrela" de Arthur Duarte, 1947 Duração: 110 min. Estreou a 20 Fevereiro 2005 
> - "O Querido Lilás" de Artur Semedo, 1987 Duração: 104 min. Estreou a 30 Março 2005 
> - "A Vida é Bela" de Luís Galvão Teles, 1982 Duração: 109 min. Estreou a 29 de Abril 2005 
> - "Um Crime de Luxo" de Artur Semedo, 1991, Duração: 87 min. Estreou a 20 Maio 2005 
> - "A Selva" de Leonel Vieira, 2003 Duração: 120 min. Estreou a 19 Junho 2005 
> - “Fado, História d’uma Cantadeira” de Perdigão Queiroga, 1947 Duração: 108 min. Estreou a 21 Julho 2005 
> - “As Pupilas do Senhor Reitor” de Leitão de Barros, 1935 Duração: 102 min. Estreou a 26 Agosto 2005 
> - “A Vizinha do Lado” de António Lopes Ribeiro, 1945 Duração: 115 min. Estreou a 29 Setembro 2005 
> - “Sonhar é Fácil” de Perdigão Queiroga, 1951 Duração: 94 min. Estreou a 30 Outubro 2005 
> - “O Pai Tirano” de António Lopes Ribeiro, 1941 Duração: 118 min. Estreou a 30 Novembro 2005 
> - “Maria Papoila” de Leitão de Barros, 1937 Duração: 98 min. 21 Dezembro 2005 
> - “Os Três da Vida Airada” de Perdigão Queiroga, 1952 Duração: 98 min. Estreou a 25 Janeiro 2006 
> 
> Próxima estreia
> 
> - “O Grande Elias” de Arthur Duarte, 1950, Duração: 124 min. Estreia a 24 de Fevereiro 2006. 

No Brasil:

> Imagine o que não se pode ver
> 
> A experiência pernambucana
> 
> Espetáculo O menino que contava estrelas foi o primeiro a disponibilizar a áudio-descrição no Recife.
> 
> Mariana, oito anos, é esperta. Morena, cabelos cacheados, inquieta. Nasceu com deficiência visual, o que não está impedindo que ela comece - como toda criança - a compreender o mundo que a cerca. Faz balé. "Danço muito bem. Vou me apresentar no teatro da UFPE. Você conhece?", pergunta a pequena. Mariana já está acostumada à aúdio-descrição. Viu Irmãos de Fé, do padre Marcelo Rossi, primeiro filme que ofereceu o recurso no país. Na televisão, assiste a programas educativos que permitem que ela construa as imagens que é impossibilitada de ver. No último mês de outubro, a garotinha era uma das mais curiosas na platéia da peça O menino que contava estrelas, que esteve em cartaz no Teatro Joaquim Cardozo, no Recife. 
> 
> Mariana nasceu com deficiência visual, faz balé, assiste TV e gostou da peça Completando a "lista dos pioneiros", o espetáculo foi o primeiro no Recife que disponibilizou a áudio-descrição. "A parte que eu mais gostei foi a do dragão.
> 
> O menino encontrava um dragão de verdade. Ficou se tremendo". 
> 
> A áudio-descrição da peça foi realizada pelos alunos do curso Imagens que falam, detradução visual com ênfase em áudio-descrição, que está sendo ministrado pelo professor Francisco Lima. Cego desde que nasceu, há 44 anos, o professor - doutor em psicofísica sensorial - lida profissionalmente com imagens desde 1996. "Trabalhei com a produção de desenhos que podem ser reconhecidos pelo tato, um recurso que é desconsiderado muitas vezes pelos próprios educadores".
> 
> Atualmente, o professor tem se dedicado a difundir a áudio-descrição que, no caso do teatro, é feita ao vivo. "Repetimos a experiência na peça Os cegos, que também estava no Joaquim Cardozo, e pretendemos continuar fazendo isso. Inicialmente, com as peças deste teatro", explica.
> 
> No campo da arte, além dos palcos e do cinema, a áudio-descrição pode ser utilizada também nos museus. "Seria uma descrição das obras que as outras pessoas conseguem enxergar", complementa Francisco. Para tornar as artes plásticas completamente acessíveis para pessoas cegas, no entanto, os recursos sensoriais devem ser estimulados. Por que não, por exemplo,disponibilizar reproduções das obras para que os deficientes possam tocá-las? 
> 
> "É preciso pensar em quais barreiras os deficientes enfrentam. Cada tipo de deficiência é uma barreira diferente. Temos uma lei no Recife, por exemplo, obrigando a fazer maquetes dos prédios públicos importantes, que possam ser tocadas, para os deficientes conhecerem a arquitetura do lugar. Mas isso não sai do papel", denuncia Anderson Tavares, idealizador da consultoria Visibilidade, que trabalha com acessibilidade. 
> 
> Diário de Pernambuco, 
> 
> Imagine o que não se pode ver 
> 
> Pollyanna Diniz - pollyannadiniz.pe@diariosassociados.com.br 

Muito embora venham sendo várias as ações não governamentais voltadas a respeitar o direito de as pessoas com deficiência assistir a um filme, a uma peça teatral ou de visitar um museu com acessibilidade; não obstante os esforços das pessoas com deficiência visual em fazer o Ministério das Comunicações entender que acessibilidade não é opção, mas sim necessidade e direito; a despeito da clareza solar de nosso ordenamento jurídico na garantia do direito à informação, à educação e à cultura, as emissoras de TV brasileiras, representadas pela ABERT, vêm fazendo lobby (e sendo acolhidas pelo Mini-Com) contra a implantação da lei que assegura esse direito aos milhares de cidadãos com deficiência, aqui incluídas pessoas com dislexia, com deficiência intelectual e física.

E não só elas, mas também as pessoas idosas (com dificuldade de leitura) e as pessoas analfabetas, uma vez que, no cinema e nos DVDs os filmes são, em grande parte, legendados e não dublados, o que impede a todas essas pessoas o acesso ao constitucional direito ao lazer que dessas obras poderia advir, caso tivessem áudio-descrição, esse recurso que é definido pela PORTARIA Nº 310, DE 27 DE JUNHO DE 2006 do Ministério das Comunicações como sendo:

> 3.3. Áudio-descrição (sic): corresponde a uma locução, em língua portuguesa, sobreposta ao som original do programa, destinada a descrever imagens, sons, textos e demais informações que não poderiam ser percebidos ou compreendidos por pessoas com deficiência visual. 

A oferta da áudio-descrição eliminaria e/ou minimizaria a atitude excludente de nossa sociedade que ainda não percebeu que não é a deficiência que incapacita a pessoa, mesmo quando e onde lhe impõe limites. Quem incapacita uma pessoa com deficiência é a própria sociedade que não lhe respeita o direito de acesso aos bens e serviços disponíveis às pessoas sem deficiência e que um contribuinte com deficiência, também ajuda pagar com seus impostos.

Logo não se está falando de filantropia ou privilégio quando se reclama pela a oferta da áudio-descrição, nem de um remendo desta. Estamos falando da provisão de um serviço com qualidade para todos a toda hora, pois a toda hora uma pessoa vidente pode ligar a televisão e assistir a um programa. Assim, para que tenha mesma igualdade de acesso a essa programação é que as pessoas com deficiência pleiteiam a áudio-descrição e a acessibilidade a ela associada.

> São 10:00 h e Joe, que é cego, quer ver televisão. A mulher e o filho de Joe, que usufruem de capacidade de visão, não estão disponíveis para ler o guia dos programas. 
> 
> Mesmo que estivessem disponíveis, Joe tentará ser independente e não quer aceder a essa informação através da sua família. É claro que pode ir de canal em canal e esperar cada intervalo para perceber através do áudio que programa é que está a ver. Isso é exatamente o que ele e outros cegos têm feito ao longo dos anos, porque o guia da programação tradicional é puramente visual. E num universo de mais de 200 canais, Joe dispenderia todo o seu tempo navegando, em vez de se divertir com um programa específico. Mas agora, Joe já não é obrigado a navegar inutilmente, graças aos serviços desenvolvidos associados à sua STB.
> 
> Barreiras dos Media Convergentes para os Indivíduos Que São Cegos ou Têm Baixa Visão 

De modo a responder ao direito de acesso à comunicação e à informação, portanto, surge uma técnica, e um profissional que a emprega: a áudio-descrição e o áudio-descritor, bem como são desenvolvidas tecnologias para a aplicação dessa técnica.

Todavia, a áudio-descrição não é uma descrição qualquer, despretensiosa, sem regras, aleatória. Trata-se de uma descrição regrada, adequada a construir entendimento, onde antes não existia, ou era impreciso; uma descrição plena de sentidos e que mantém os atributos de ambos os elementos, do áudio e da descrição, com qualidade e independência. É assim que a áudio-descrição deve ser: a ponte entre a imagem não vista e a imagem construída na mente de quem ouve a descrição. 

Logo, a união dos sentidos se dá por uma ponte em cujas extremidades estão a imagem e a descrição. Essa ponte, o áudio-descritor, vem conduzir a imagem que sem a descrição será inacessível às pessoas com deficiência visual, mas que, com a áudio-descrição, tomará sentido.

Com efeito, para Lívia Mota (2008), a áudio-descrição 

> “...é um recurso de acessibilidade que permite que as pessoas com deficiência visual possam assistir e entender melhor filmes, peças de teatro, programas de TV, exposições, mostras, musicais, óperas e outros, ouvindo o que pode ser visto. É a arte de transformar aquilo que é visto no que é ouvido,o que abre muitas janelas para o mundo para as pessoas com deficiência visual. Com este recurso, é possível conhecer cenários, figurinos, expressões faciais, linguagem corporal, entrada e saída de personagens de cena, bem como outros tipos de ação, utilizados em televisão, cinema, teatro, museus e exposições”. (grifos nossos) http://www.saci.org.br/index.php?modulo=akemi&parametro=22027

Estando disponível nos Estados Unidos e Europa, há umas 3 décadas, no Brasil, a áudio-descrição ainda é motivo de espanto para quem, pela primeira vez ouve falar ou lê a respeito dessa tecnologia assistiva de acesso à comunicação/informação.

> Mercado inclusivo
> 
> 10/Dez/2009
> 
> Nunca ouvi falar em audiodescrição. Até o dia em que uma amiga publicitária, Neide Cavalcanti, disse que estava se formando na segunda turma do curso de extensão de formação de audiodescritores ou imagens que falam do Centro Estudos Inclusivos, que fica no Centro de Educação Inclusiva (CEI) da UFPE. A técnica é usada para pessoas com deficiência visual, com baixa visão e para os print deisability, os que têm dificuldade de leitura, como analfabetos, crianças e disléxicos, por exemplo. 
> 
> O coordenador do CEI, Francisco Lima, define a disciplina como uma técnica de descrição voltada aos eventos visuais, imagens, fotos, filmes, tornando-os compreensíveis a pessoas cegas, por intermédio da fala ou escrita de quem descreve tais eventos.”
> 
> Moema Luna é jornalista e responsável pela coluna JC Marketing e Comunicação
> 
> http://jc3.uol.com.br/comercial/coluna.php?canal=5&dth=2009-12-10

O próprio Ministério das Comunicações e outros têm dado provas de não entender/saber o alcance dessa tecnologia e do que ela significa em termos de direitos humanos. Exemplo disso é a deplorável e controversa Portaria 661/08 que protelou a obrigatoriedade de as televisões oferecerem áudio-descrição em suas programações, conforme manda nosso ordenamento jurídico lei 10098/00 e decreto 5296/04.

Segundo Quico (2005), 

> Num estudo realizado em 1998, a American Foundation for the Blind verificou que a maioria das pessoas com deficiências visuais que tinham utilizado o serviço de Áudio-Descrição consideraram este como muito útil, bem como davam preferência aos conteúdos com Áudio-Descrição (sic). Os principais benefícios do serviço citados por pessoas invisuais ou com deficiências visuais graves foram os seguintes:
> - ficar a conhecer os ambientes visuais do programa,
> - compreender melhor os materiais televisivos,
> - sentir-se independente,
> - sentir-se igual a uma pessoa sem deficiências visuais,
> - sentir satisfação,
> - alivio dos espectadores sem deficiência visual com quem assistiam aos programas. 

E o desconhecimento sobre os benefícios da áudio-descrição perpassa outras áreas sociais, como a da educação que, por seu ministério não obriga que os livros didáticos estejam disponíveis aos alunos com deficiência visual com a descrição dos elementos visuais, muitas vezes necessários para o completo entendimento e conseqüente aprendizagem do conteúdo desses livros.

## Em defesa da áudio-descrição

Pelo caráter incipiente da áudio-descrição no Brasil, até mesmo entre alguns dos áudio-descritores brasileiros, verifica-se controvérsia, por exemplo, quanto à grafia do termo áudio-descrição, o qual tem aparecido escrito como “áudiodescrição”, “áudio descrição” e, mais freqüentemente, na forma de “audiodescrição”. E como se grafa a palavra áudio-descrição, então?

Como se pode ver pela grafia adotada neste artigo, o autor optou por a grafar com hífen, mantendo o acento agudo no a do primeiro elemento do léxico.

Ao dizermos áudio-descrição, estamos dizendo de áudio e estamos dizendo de descrição. Os termos mantêm individualmente seu sentido original, porém, constituindo novo sentido, numa nova unidade semântica. Quanto à prosódia e à grafia das palavras em separado, elas são mantidas, logo não havendo razão que justificasse as unir na grafia ou as escrever em separado, sem hífen. Isto é, a junção dos termos áudio e descrição pelo hífen leva ao entendimento de uma nova construção semântica, com sentido próprio, sem que cada termo se destitua por completo de seu sentido original. 

Fundir áudio e descrição, sem a correta grafia, portanto, pode levar a uma idéia imprecisa do que, de fato, ela é, um novo termo, com elementos constitutivos conhecidos, mas que mantém a acentuação e demais atributos gráficos dos elementos em separado e, por conseguinte, com sentido próprio e distinto do significado que têm os termos áudio e descrição individualmente. 

Quando se une diferentes elementos com hífen e não se perde o sentido original de cada elemento constitutivo, constrói-se na relação uma nova unidade semântica com sentido próprio e concernente ao novo vocábulo. Não se dar conta disso é um equívoco que pode levar a conclusões errôneas.

No extrato abaixo vê-se exemplo do erro que o autor pode cometer, quando a despreocupação com o uso da grafia correta ou o aligeiramento na decisão de que escrita adotar dão lugar à pesquisa, ao estudo e, enfim, à investigação científica minuciosa.
 
> Você está com uma pressa danada e quer saber se "salário mínimo" se grafa com hífen ou sem ele. Numa consulta ao dicionário, você rapidamente vê "salário-mínimo" com hífen e se dá por satisfeito: vai usar a expressão com o hífen. 
> 
> Nada disso! Numa consulta mais atenta, você verá que "salário-mínimo" com hífen tem um significado diferente do que está imaginando. A expressão não significa o valor mínimo que o trabalhador brasileiro deve receber como salário. Nesse caso, devemos grafar sem o hífen: "salário mínimo". "Salário-mínimo" é uma expressão popular que possui outro sentido: "Esse time é salário-mínimo" = "Esse time é muito fraco, não vale nada".
> 
> Quando queremos nos referir à remuneração mínima dos assalariados, utilizamos as palavras "salário" e "mínimo" no sentido básico delas, o que não acontece na expressão com hífen, que é usada para adjetivar alguma coisa. 

O excelso, polêmico e não menos criticado gramático e filólogo da língua portuguesa Napoleão Mendes de Almeida, confirma o uso de hífen, quando os elementos constitutivos de um vocábulo mantém prosódia e grafia originais, formando nova palavra com sentido próprio. E Napoleão concorda com essa regra, sem deixar de seu purismo no que tange às normas gramaticais e ortográficas, bem como sem deixar de ser crítico ferrenho do uso de hífen, dizendo que esse traço-de-união “é desnecessário e mero ‘enfeite’ que deve ser eliminado”.

> Justifica-se o hífen em algumas das palavras compostas por justaposição, a saber, naquelas formadas por dois termos que têm significação própria quando isolados e, no se unirem para formar o composto, conservam ambos, além da significação, a grafia e a prosódia que lhes são peculiares: guarda-chuva, couve-flor, papel-moeda, amor-perfeito. “Justifica-se” ficou dito, mas não nos esqueçamos de que esse enfeite existe quase que exclusivamente no nosso idioma. (Almeida, 1996:244-245)

Alinhados com o Gramático, sustentamos que o uso do hífen não é exclusivo de nossa Língua, como se pode verificar em outros idiomas como no inglês e francês. 

Com efeito, também nesses idiomas há os que adotam o traço-de-união ao grafarem áudio-descrição. Seria mesmo por “enfeite”?

> Audio-Description
>
> Bernd Benecke
> 
> Bayerischer Rundfunk, Munich, Germany
> 
> RÉSUMÉ
> 
> L’audio-description est présentée principalement selon deux axes: son développement récent et les principales étapes de réalisation de ce mode de transfert linguistique.
> 
> (A áudio-descrição é apresentada principalmente segundo dois eixos: seu desenvolvimento recente e as principais etapas de realização deste modo de transferência lingüística). 
> 
> ABSTRACT
> 
> This paper deals mainly with two aspects of audio-description: the development of this mode of language transfer and the main steps in the preparation of an audio-description.
> 
> (Este artigo trata principalmente de dois aspectos da áudio-descrição: o desenvolvimento deste modo de tradução lingüístico E os passos principais no preparo da áudio-descrição).
> 
> MOTS-CLÉS/KEYWORDS
> 
> audio-description, blind and visually-impaired people, German TV channels, recording
> 
> (Palavras chave áudio-descrição, pessoas cegas e com deficiência visual, canais de televisão alemã, gravação) 

> Centre Dramatique Régional de Tours
> 
> DE GAULLE EN MAI 
> 
> Textes organisés par Jean-Louis Benoit
> 
> Extraits du Journal de l’Elysée de Jacques Foccart
> 
> Avec Jean-Marie Frin, Arnaud Décarsin, Luc Tremblais, Laurent Montel, Dominique Compagnon
> 
> Ce sont des extraits du journal de Jacques Foccart consacré à cette période qui nous ont permis de construire ce spectacle (...)
> 
> Représentation avec audio-description
> 
> Jeudi 11 décembre 2008
> 
> (Estes são extratos do jornal de Jaques Foccart dedicados a este período que nos permitiram construir este espetáculo: (...)
> 
> Representação com áudio-descrição. 
> 
> Quinta, 11 de dezembro de 2008 
> 
> Bringing ballet to the blind - News - audio-description at the Kentucky Center for the Arts - Brief Article
> 
> Dance Magazine, 
> 
> June, 2002
> 
> by Janet Weeks
> 
> Dancers aren't the only ones warming up before The Nutcracker performances at the Kentucky Center for the Arts in Louisville. Out front, a trained audio-description volunteer also prepares for a live performance. As the flowers waltz, the volunteer, settled into the light booth or some other suitable space, broadcasts an auditory scene for blind patrons who listen from their seats using a receiver and earphone. The center's audio-description program celebrates its tenth anniversary this season and for the first time it's offering the service for modern dance as well as ballet devotees.
> 
> (Trazendo Ballet aos cegos – Notícia – Áudio-descrição no Centro de Artes de Kentucky- Breve artigo. Os dançarinos não são os únicos a se aquecerem antes da apresentação do “The Nutcracker”, no Kentucky Center for the Arts de Louisville. Lá na frente, um voluntário treinado em áudio-descrição também se prepara para uma performance ao vivo) 

Ficando demonstrado o uso desse sinal gráfico em outros idiomas, em mesma expressão de que fazemos uso, a saber, áudio-descrição e ficando demonstrado que, até mesmo o grande Gramático brasileiro Napoleão Mendes de Almeida reconhece a justificativa para o uso de hífen em vocábulo composto, conforme citado, passemos a analisar, pois, o léxico áudio-descrição, quanto ao significado em separado de seus elementos e a relação destes quando juntos.

Segundo o popular Dicionário do Aurélio:

> Áudio: [De audi(o)- (q.v.).] S.m. 2. Cin. A parte sonora de um filme.
> 
> Descrição: [Do lat. Descriptione.] S.f. 1. Ato ou efeito de descrever. 2. Exposição circunstanciada feita pela palavra falada ou escrita. (FERREIRA, Aurélio Buarque de Holanda,2004)

Aqui, há de se notar que a descrição se presta “pela palavra falada ou escrita”, portanto não sendo de se admirar que desse termo derivem áudio-descrição, a descrição escrita de imagens estáticas (de fotografias, pinturas, esculturas etc.), ou das expressões, figurinos em filmes e peças teatrais, tanto quanto a parte sonora de um filme, em processo de legendagem para o sistema close caption, voltado aos indivíduos surdos, cujo acesso aos áudios do filme se dá, por exemplo, pela descrição dos sons necessários à compreensão da obra e que não são deduzidos da informação visual ou escrita da cena. 

Consoante o excelso lexicógrafo Caldas Aulete:

> Descrição: s.f. discurso por meio do qual se descreve ou representa alguma coisa ou pessoa; narração circunstanciada; enumeração dos caracteres que distinguem uma pessoa ou coisa.(Aulete, 1968) 

Observemos que a narração a que o termo descrição nos remete, quando da áudio-descrição é uma narração dos atributos visuais não compreendidos dos diálogos e efeitos sonoros do filme, o que deixa patente a manutenção desse significado ao se constituir a palavra áudio-descrição (descrição com palavras, narração, dos eventos imagéticos, das características da cena ou dos elementos dela).

Portanto, o termo descrição é adequadamente aplicado nessa construção do vocábulo para melhor determinar que a descrição não seja confundida com interpretação. Com efeito, na áudio-descrição, descreve-se o que se vê e não o que se pensa ou que se acha ter visto.

A áudio-descrição vem completar, ampliar o conhecimento que se pode alcançar de uma dada cena ou filme, mas será a cognição que fará a diferença, de fato. Assim, o papel do áudio-descritor é levar à mente do usuário do serviço, por meio da descrição, oral ou escrita, aquilo que ele vê, da forma que vê, com a maior completude e exatidão que o tempo lhe permitir, dentro de regras e premissas profissionais, sólidas e éticas.

Para sustentarmos com maior profundidade este entendimento, tomemos mais uma vez o que vem significar áudio e descrição, agora na definição do renomado dicionário Michaelis:

> áudio
> 
> áu.dio2
> 
> sm (lat audio) Telev 1 Faixa do espectro reservada ao som, em contraposição ao vídeo. 2 Coluna do script destinada às falas e anotação de sons. 
> 
> descrição
> 
> des.cri.ção
> 
> sf (lat descriptione) 1 Ação ou efeito de descrever. 2 Lit Tipo de composição que consiste em enumerar as partes essenciais de um ser, geralmente adjetivas, de modo que o leitor ou ouvinte tenha, desse ser, a imagem mais exata possível. (grifos nossos) 

Como fica patente, a relação de script, de descrição circunstanciada, de narração pela fala em registro oral ou pelo próprio registro escrito, bem como a relação de tradução da imagem em som, tornando-o “oposição à imagem”, mas correspondente ao filme, torna o vocábulo áudio-descrição um termo com sentido específico e inovador, cujos elementos assumem o significado da tradução visual mediado pelo audiodescritor.

Por assim dizer, então, o áudio-descritor é o (hífen) que une a obra ao expectador, dando novo sentido a ambos, o sentido da acessibilidade, da cidadania, do respeito que cada espectador deve ter ao se deparar com uma obra, seja ela cultural, educacional ou outra.

Entretanto, mais que uma questão gramatical o hífen da áudio-descrição é o traço de união entre a audição das pessoas com deficiência visual e a imagem visual pretendida pelo autor; entre a inacessibilidade e a compreensão; entre o desrespeito e o reconhecimento de direitos.

Ainda assim, lancemos olhos ao novo acordo ortográfico da língua portuguesa, previsto para entrar em vigor no ano que nos soma à porta, e vejamos o que ele nos diz:

Segundo o Acordo Ortográfico da Língua Portuguesa, apresentado por Ernani Terra, o emprego do hífen (-)

> continua a ser usado nas palavras compostas, na ligação dos pronomes oblíquos enclíticos (colocados depois da forma verbal) e mesoclíticos (colocados no meio da forma verbal) ao verbo e na ligação dos sufixos de origem tupi: couve-flor, segunda-feira, entregá-lo, entregá-lo-íamos, sabiá- guaçu. 

O autor observa que “no entanto, as palavras em que se perdeu a noção de composição deverão ser escritas sem o hífen”, o que não é o caso do vocábulo áudio-descrição, em que se mantém o sentido dos elementos em separado. 

O autor observa ainda que não se usa hífen quando:

a. quando o prefixo termina em vogal e o segundo elemento começa por r ou s: antirreligioso, antissemita, contrarregra, cosseno
b. quando o prefixo termina em vogal e o segundo elemento começa por vogal diferente dela: antiaéreo, autoestrada, coeducação.
	
Em todos esses casos, certificamos que não é acolhida a justificativa da grafia de áudio-descrição sem o (hífen).

Ao consultarmos o site http://www.colegiocastroalves.g12.br, verificaremos que: 

| Nova Regra                                                                                                                | Regra Antiga                                                                            | Como será																		|
|---------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|
| Não usamos mais hífen em, compostos que, pelo uso, perdeu-se a noção de composição                                        | manda-chuva, pára-quedas,pára-quedista, pára-lama, pára-brisa, pára-choque, pára-vento  | mandachuva, paraquedas,paraquedista, paralama, parabrisa, parachoque, paravento |

**Obs: o uso do hífen permanece em palavras, compostas que não contêm elemento de ligação e constitui unidade sintagmática,e semântica**, mantendo o acento próprio, bem como,naquelas que designam espécies botânicas e zoológicas: ano-luz, azul-escuro,médico-cirurgião, conta-gotas, guarda-chuva, segunda-feira, tenente-coronel,beija-flor, couve-flor, erva-doce, mal-me-quer, bem-te-vi etc.[1],[1] http://www.colegiocastroalves.g12.br/<L%C3%8DNGUA_PORTUGUESA class="doc">						</L%C3%8DNGUA_PORTUGUESA>

Também aqui, nada se vê que obsta o uso do hífen em áudio-descrição. Pelo contrário, se confirma que nos casos em que os elementos mantêm sentido e grafia originais, porém constituindo nova unidade semântica com sentido próprio, a indicação é de se fazer uso desse traço-de-união. 

E é pela união que devemos optar no caso da áudio-descrição, uma vez que mais que a controvérsia sobre a grafia deste vocábulo, o debate a ser feito é o da defesa de as pessoas com deficiência terem acesso à áudio-descrição, como meio de resposta social à barreira comunicacional que os filmes e outros recursos televisivos, de cinema, de teatros e outros impõem às pessoas cegas  ou com baixa visão, quando o recurso da A-d não é oferecido.

Logo, não nos debatamos pelo que há de controvérsia na A-d, mas nos detenhamos no que nela nos une. Em outras palavras, não nos descuidemos do traço que nos une na áudio-descrição: o direito humano de se ter acesso com igualdade de condições e oportunidade aos bens e serviços devidos a todos, sejamos pessoas com deficiência ou não.

Ademais, áudio-descrição é nada menos que acessibilidade, e acessibilidade é áudio-descrição, significando que, como dizem os poetas, 

Se você não vê, poderá ouvir;

Se você não ouve, poderá ler;

Se você não lê, poderá compreender.”

Entretanto, se você ainda não se convenceu de que áudio-descrição é direito à informação, à comunicação, à cultura, lazer e educação, convença-se de que ela é tudo isso e pratique a acessibilidade, promovendo a cidadania, dignidade e respeito à pessoa humana, também nos museus, no cinema, nos teatros e na televisão. 

## Referências Bibliográficas

ALMEIDA, Napoleão Mendes de. **Dicionário de Questões Vernáculas**. 3. Ed. São Paulo: Editora Ática, 1996. p. 244-245.

AULETE, Caldas. **Dicionário Contemporâneo da Língua Portuguesa**. 5ª ed. Rio de Janeiro, Delta S.A., 1968.

FERREIRA, Aurélio Buarque de Holanda. **Novo dicionário Aurélio da Língua Portuguesa**. 3ª ed. Revista e atualizada. Curitiba, ed. Positivo, 2004.

MOTTA, Maria Villela de Mello. **Audiodescrição – recurso de acessibilidade para a inclusão cultural das pessoas com deficiência visual**, 2008. In: [http://www.saci.org.br/index.php?modulo=akemi&parametro=22027][b1]

QUICO, Célia. Acessibilidade e Televisão Digital e Interactiva: o caso particular do serviço de Áudio-Descrição destinado a pessoas invisuais ou com deficiências visuais graves". In: **Estratégias de Produção em Novos Media**, Edição COFAC/ Universidade Lusófona de Humanidades e Tecnologias. ISBN: 972-8881-08-8, 2005.


[b1]: http://www.saci.org.br/index.php?modulo=akemi&parametro=22027