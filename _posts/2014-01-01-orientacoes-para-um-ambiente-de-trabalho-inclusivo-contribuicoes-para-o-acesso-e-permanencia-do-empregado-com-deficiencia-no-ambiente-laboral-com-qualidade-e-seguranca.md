---
layout: edition
title: Orientações para um ambiente de trabalho inclusivo&#058; contribuições para o acesso e permanência do empregado com deficiência no ambiente laboral, com qualidade e segurança
date:  2014-01-1
categories: edition 17
cover: 17.png
issue: 17
release: 2014
authors:
 - Francisco José De Lima
 - Rosângela A. F. Lima
 - Clarissa M. De Araújo
 - Gustavo M. E. Azevedo
 - Lívia Guedes
tags:
 - teste

--- 


Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem iste, nihil quae animi. Neque, eius, reprehenderit? Laboriosam minima numquam perferendis adipisci. Amet consequuntur exercitationem nisi officia quaerat? Deleniti, tempora, fuga.

- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!

![teste][foto1]
<span>teste</span>

1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
2. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
3. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
4. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!
5. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque quia aliquid molestias temporibus similique porro, animi possimus tempore esse! Maiores veritatis autem hic ratione fugiat doloremque quae reiciendis sit eaque, excepturi voluptatum, sequi aperiam natus, eligendi. Accusantium libero nesciunt velit esse iure et fuga doloremque illum! Vel libero eos earum quaerat quae quibusdam ab voluptas!


> Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!

Lorem ipsum dolor sit amet, consectetur adipisicing elit. At repudiandae voluptatum natus, amet eos fugit quidem sed vero atque minima quaerat esse repellat illo aspernatur dolor adipisci quae vitae possimus quam, aperiam dignissimos maxime. Eveniet, debitis rerum. Rerum numquam magnam, in. Ducimus in, ad eaque temporibus omnis placeat accusantium nihil!


[foto1]: https://cdn-images-2.medium.com/max/800/1*ylMMiHjmLjBYGi0ahRAy1g.png "teste123"