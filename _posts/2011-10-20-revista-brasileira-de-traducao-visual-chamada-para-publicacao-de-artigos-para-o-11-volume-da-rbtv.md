---
layout: article
title: Revista Brasileira de Tradução Visual&#058; chamada para Publicação de Artigos para o 11 volume da RBTV.
except: 
urlsource: 
author: RBTV
extrainfo: 
date: 2011-10-20
categories: article
tags:
- Acessibilidade
- Tecnologia

---

Nestes dias, você perceberá mudanças nas diferentes seções, entre elas, na seção de Política de Submissão, na Política de Seção, nas Diretrizes para Autores, entre outras melhorias, como, por exemplo, a criação de uma nova seção para publicações. Trata-se da seção [Cartas ao Editor][1], onde se publica o diálogo científico em que se promovem comentários, críticas ou discussões a respeito dos temas abordados nos artigos publicados na própria Revista, ou que, tendo relação com o conteúdo desta, venham versar sobre outros temas de interesse geral. Nessa seção é possível uma réplica dos autores do artigo em comento, a qual será publicada junto à carta, ou em número subsequente da revista.

O leitor da RBTV poderá encontrar, a partir de agora, ao pé da página principal, um link para participar de um grupo de discussão sobre o estudo da áudio-descrição, assunto que a RBTV tem tratado com profundidade e frequência, tornando-se referência na publicação científica/acadêmica nessa área, no Brasil.

Esse grupo de estudos, o [Áudio-Descrição em Estudo][2], criado e acompanhado pelo atual editor-chefe da Revista Brasileira de Tradução Visual, Professor Francisco Lima, vem como mais um passo na busca de difundir a áudio-descrição como um direito de acessibilidade comunicacional devida às pessoas com deficiência e tendo como base que a áudio-descrição é recurso assistivo que deve ser provido com ética, qualidade e profissionalidade aos usuários que dela podem se beneficiar.

Com o apoio a esse grupo de estudos, a RBTV está estimulando mais uma via para estudo dessa técnica de tradução visual intersemiótica. Nas palavras do Professor Francisco Lima: 

“Agora, com uma lista no Googlegroups, vamos ter um espaço em que estudamos, partilhamos ideias, opiniões e conhecimento a respeito da áudio-descrição; um espaço em que buscamos conhecer sobre quem são os usuários da a-d, os direitos a eles pertinentes e muito mais.

“Neste espaço refletimos sobre as questões adjacentes à prestação do serviço de áudio-descrição, discutimos a produção de roteiros áudio-descritivos, a locução e a recepção da áudio-descrição, bem como os valores éticos e o empoderamento, no gênero tradutório chamado de tradução visual, em que se insere
a áudio-descrição.

“Este espaço é para você que quer estudar sobre áudio-descrição e que quer construir uma sociedade mais inclusiva e menos excludente.

Assim, se você é áudio-descritor, deseja tornar-se um, ou é usuário da áudio-descrição, venha participar de nosso grupo. Se você é produtor/fornecedor do serviço de áudio-descrição, venha participar conosco da construção do conhecimento que a nós todos interessa. Se você é professor, produtor cultural, profissional da comunicação, das artes, ou apenas um cidadão desejoso de ver a inclusão cultural, educacional, laboral e de lazer das pessoas com deficiência tornar-se realidade, também por intermédio da áudio-descrição, pode unir-se a nós que será bem-vindo.

“A áudio-descrição é assunto sério e precisa de pessoas sérias, éticas e profissionais como você, para que possamos quebrar barreiras atitudinais e pôr fim ao apartheid social e cultural que perpassam tantos 'lugares' em nossa sociedade.”

Com estas ações e outras, oportunamente divulgadas pela RBTV, estamos buscando cumprir um papel social de uma revista científica que é, ao nosso ver, mais amplo que a divulgação do conhecimento acadêmico, a saber, o de apoiar a sociedade na sua transformação para melhor, para uma sociedade mais inclusiva e menos excludente, em todas as áreas.

Venha participar também:
Publique conosco, leia nossos artigos e participe de nosso grupo de estudos.

**Comitê Editorial da RBTV**

**Observação:** Para participar do grupo de discussão Áudio-Descrição em Estudo, você pode inscrever-se através da página da RBTV, preenchendo seu e-mail, na caixa apropriada, no rodapé do site, ou visitando a página do grupo Áudio-Descrição em Estudo, no Google Groups.

[1]: http://www.rbtv.associadosdainclusao.com.br/index.php/principal/about/editorialPolicies#sectionPolicies
[2]: http://groups.google.com/group/audio-descricao-em-estudo?hl=pt-BR