---
layout: post
title: “Acessibilidade não é privilégio, é direito”, diz jornalista sobre audiodescrição na TV Folha
except: 
urlsource: http://www.portalimprensa.com.br/noticias/brasil/68024/acessibilidade+nao+e+privilegio+e+direito+diz+jornalista+sobre+audiodescricao+na+tv+folha
author: Christh Lopes | Portal Imprensa
extrainfo: com supervisão de Vanessa Gonçalves
date: 2015-05-02
categories: news
tags:
- Acessibilidade
- Tecnologia

---

Desde março, a jornalista Melina Cardoso se prepara para colocar em prática o aprendizado sobre acessibilidade em peças audiovisuais. Na última terça-feira (9/9), a TV Folha exibiu a primeira reportagem dela com audiodescrição e legendas, tendo como personagem o radialista Alberto Pereira, que conta a história do cão-guia Simon, que está próximo de se aposentar.

![Melina Cardoso exibindo um largo sorriso][melinda]
<span>Melina Cardoso estuda para implantar audiodescrição em matéria da TV Folha (Crédito:Arquivo pessoal)</span>

Na tarefa de ser os olhos do internauta, a repórter ressalta o apoio do veículo durante o processo de estudos na Universidade Federal de Juiz de Fora (MG). “A ideia é colocar em prática o que estou estudando, até porque é um direito das pessoas com deficiência ter acesso a notícias de forma clara, principalmente os vídeos da TV Folha, que são tão visuais, cheios de informações”, salienta Melina.
 
O projeto segue uma característica do jornal, que disponibilizou nos últimos dias um leitor de software para pessoas com deficiência visual nas matérias, a fim de facilitar a leitura de notícias veiculadas no portal do diário. O objetivo agora é trabalhar para conseguir oferecer a cada semana um vídeo acessível. “A acessibilidade não é um privilégio, ela é um direito”, afirma.

![Cão-guia branco olhando para fora do carro com a cabeça apioada no joelho de homem mexendo no celular][caoguia]
<span>Reportagem acessível disserta sobre aposentadoria de cão-guia</span>

A jornalista procura compartilhar as experiências adquiridas ao longo do curso com os colegas de redação e a importância de tornar o material do canal acessível ganha espaço a cada dia. Com o sucesso da matéria sobre Simon, há mais uma motivação para manter a iniciativa. “Este recurso é algo muito recente”, diz a jornalista, que teve ajuda de uma professora nesta primeira reportagem.
 
“Existem termos que não podem ser usados na audiodescrição e formas de fazer um roteiro que necessita de uma supervisão”, destaca.  

Como primeira lição, ela nos conta que a linguagem precisa se adaptar à audiodescrição, “que nada mais é do que narrar o que você está vendo". "Só que evita adjetivos, pois a pessoa que está ouvindo chegará à conclusão se a cena é bonita, feia, legal ou chata”.
 
À IMPRENSA, Melina explica que o método não entrega a notícia "mastigada" para o telespectador. "Eu explico o que está passando na cena e a pessoa chega às suas próprias conclusões”, completa. 

Para esta primeira pauta com audiodescrição, a jornalista aproveitou um tema sugerido durante a Copa, mas que estava na gaveta. Afinal, como ela mesmo diz, "nada mais justo do que colocar acessibilidade numa matéria, cujo personagem principal é uma pessoa com deficiência visual”..
 
Assista ao vídeo:

[melinda]: http://www.portalimprensa.com.br/content_file_storage/2014/09/10/Melina_Cardoso.png
[caoguia]: http://www.portalimprensa.com.br/content_file_storage/2014/09/10/Simon_cao_guia.png