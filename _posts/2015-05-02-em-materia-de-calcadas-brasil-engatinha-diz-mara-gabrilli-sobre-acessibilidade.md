---
layout: post
title: Em matéria de calçadas, Brasil engatinha, diz Mara Gabrilli sobre acessibilidade
except: 
urlsource: http://www.ebc.com.br/cidadania/2015/05/em-materia-de-calcadas-brasil-engatinha-diz-mara-gabrilli-sobre-acessibilidade
author: Leticia Constant | EBC
date: 2015-05-02
categories: news
tags:
- Acessibilidade
- Tecnologia

---

Trinta de abril foi o Dia Mundial da Acessibilidade. Um tema que não é prioritário nas políticas públicas, mesmo nos países mais avançados. 

Sair de casa para ir à padaria, comprar o jornal, ir ao cinema, jantar com os amigos, ver o show do cantor favorito ou curtir uma exposição, tuudo isso parece fácil, simples, mas não pra todo mundo. De cadeira de rodas, por exemplo, dependendo da cidade em que vivemos, sair é um verdadeiro combate.

Na Europa, as principais capitais são bem estruturadas para receber pessoas com necessidades especiais, mas também têm limites graves como, por exemplo, os metrôs de Paris, que são inviáveis para os cadeirantes.

## Acessibilidade no Brasil

A deputada Mara Gabrilli, primeira tetraplégica a ocupar um cargo federal, é uma militante incansável, responsável pela Lei de Acessibilidade em São Paulo, que obriga os comerciantes a regularizar os estabelecimentos, permitindo o acesso e proteção de crianças, adolescentes, idosos e pessoas com necessidades especiais.

Ela acha que nos últimos dez anos, quando foi criada a primeira secretaria da pessoa com deficiência no Brasil, o país avançou em políticas públicas para inclusão e para as pessoas com deficiência. "O Brasil tem um dos ordenamentos jurídicos mais ricos do mundo no que diz respeito às pessoas com deficiência, mas ainda há muitos caminhos a percorrer e preconceitos a derrubar, preconceitos da iniciativa privada, do terceiro setor, das iniciativas individuais da sociedade e do setor público", diz Mara.

O Brasil não tem nenhuma cidade modelo em termos de acessibilidade, mas vale destacar alguns locais que investiram na iniciativa.

"Existe uma cidade bem interessante, a cidade de Socorro, no interior de São Paulo, que tem uma vocação para o esporte radical das pessoas com deficiência", diz Mara, explicanqo que por este motivo as pousadas  os hotéis, os restaurantes, acabaram se adequando. "Também não dá para dizer que a cidade é 100% acessível, mas tem características bem bacanas que vale a pena conhecer. Além de Socorro, temos pequenas cidades e outras maiores como, por exemplo, Uberlândia, no Triângulo Mineiro, que foi a primeira a ter uma frota completa de ônibus para atender passageiros com necessidades especiais. Mas também não dá pra dizer que é uma cidade modelo", observa.

As calçadas são fundamentais para as pessoas que precisam de acessibilidade especial. Mara Gabrilli sorri ao falar do estado das calçadas brasileiras. "Em matéria de calçada, o Brasil ainda engatinha e engatinha caindo. Não tem nenhuma cidade que a gente possa dizer que é modelo. São Paulo tem alguns pontos onde quem vem do interior ou de outros estados, usando uma cadeira de rodas, um cego, ou alguém que usa maletas, fica maravilhado. Mas São Paulo é muito grande, e do mesmo jeito que a gente tem pontos extremamente acessíveis, você vai na periferia e não tem nada...", conclui Mara Gabrilli.

A arte de vivenciar as necessidades especiais

O acesso inserido na vida cotidiana é um tema que vem despertando interesse em outros campos. Um exemplo é o escritório de cenografia e conteúdo Folguedo, sediado no Rio de Janeiro, que apresentou recentemente na Casa da Ciência a exposição "Cidade Acessível" em que os visitantes se colocaram na situação vivida por cadeirantes, cegos, idosos, surdos ou velhos.

Leonardo Bumgarten de Freitas, um dos sócios do escritório, fala sobre a mostra. " A ideia é criar essa sensibilização nas pessoas, essa consciência. A gente entende que se você vive uma situação, tem muito mais possibilidade de incorporar isso de uma forma verdadeira, então, se você passar pela dificuldade, óbvio que uma dificuldade controlada num ambiente seguro,você vai entender quem tem uma dificuldade grande;  e quando vai para o espaço real da cidade consegue imaginar o cotidiano dessas pessoas. E não é só na questão do cadeirante, na questão da deficiência, mas acho que numa questão mais ampla da sensibilidade para todos, de fato; entender que todos têm uma necessidade - temporária, de maior ou menor grau -, na coisa de enxergar, de conseguir apertar um botão, ouvir informação, se localizar...Há uma série de questões envolvidas além do circular, além do andar", reflete Leonardo.

Na mostra Cidade Acessível os visitantes foram convidados a experimentar, vivenciar, se colocar no lugar do outro, escolhendo restrições e barreiras como, por exemplo, cobrir os olhos com uma faixa, andar de cadeira de rodas e outras opções.

## Blogs

A solidariedade entre os próprios necessitados de acesso especial também contribui para facilitar o cotidiano. Em diversos blogs, cadeirantes contam suas experiências de viagens, dão dicas de locais de lazer equipados para recebê-los, informam leis e direitos e as última novidades em acessórios como capas para cadeiras de rodas ou bengalas eletrônicas.