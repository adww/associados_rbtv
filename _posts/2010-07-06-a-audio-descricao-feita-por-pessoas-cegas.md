---
layout: article
title: A Áudio-descrição feita por Pessoas Cegas
except: 
urlsource: 
author: Illiam Schiff e Morton A. Heller
extrainfo: 
date: 2010-07-06
categories: article
tags:
- Acessibilidade
- Tecnologia

---

> “Lisa Helen Hoffman, que é cega, é uma das poucas consultoras e formadoras em áudio-descrição com essa deficiência, nos Estados Unidos. Ela traz sua experiência de pessoa cega para o campo da áudio-descrição. Ela já prestou consultoria para mais de 40 produções no Teatro Geva em Rochester, Nova Iorque e para uma produção no Teatro Nacional Real (Royal National Theatre) em Londres, na Inglaterra. Ela formou-se em áudio-descrição no “Jerome Lawrence and Robert E. Lee Theatre Research Institute” na Universidade Estadual de Ohio e é pósgraduada pela Universidade de Buffalo.

Nascida com retino blastoma, cancer no olho, a Srta. Hoffman é cega desde os 2 anos.

Durante suas muitas idas à cidade de Nova Iorque, para tratamento médico, um momento de lazer propiciado por um espetáculo na Broadway despertou-lhe a paixão pelo teatro, quando ainda de tenra idade. Ela adorava ouvir a atuação, o canto e a música. Para que tivesse acesso aos elementos visuais do que assistia, sua família lhe sussurrava as descrições das produções.

Agora, é ela quem propicia acesso a espetáculos teatrais a outras pessoas com deficiëncia visual, a fim de que possam apreciá-los de forma independente. 

Junto à comunidade de pessoas com deficiência de Rochester, no estado de Nova Iorque, a Srta. Hoffman vem atuando como secretária correspondente da Divisão local do “American Council of the Blind” e como Vice-Presidente da “Advocacy and Education for the Regional Center for Independent Living”. Em nível nacional, ela é membro titular da Audio-Description International (ADI) e participante do Primeiro Fórum sobre Carreiras Artísticas para Pessoas com Deficiência. Ela faz parte da Association for Theatre and Accessibility (ATA).”

Para saber mais, veja a página de Lisa Helen Hoffman, clicando aqui.

Comunique-se com a RBTV:rbtv@associadosdainclusao.com.br