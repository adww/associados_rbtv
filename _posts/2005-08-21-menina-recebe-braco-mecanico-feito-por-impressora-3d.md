---
layout: post
title: Menina recebe braço mecânico feito por impressora 3D
except: Grupo de voluntários ajuda crianças que perderam mãos ou braços
urlsource: http://veja.abril.com.br/noticia/vida-digital/menina-de-8-anos-ganha-braco-mecanico-feito-por-impressora-3d
author: Redação Revista Veja
date: 2015-08-21
categories: news
tags:
- Acessibilidade
- Tecnologia
- Impressora 3D
- Implantes cibernetico

---
O grupo de voluntários E-Nable providenciou um braço mecânico feito por uma impressora 3D a uma menina de 8 anos, de Bristol, na Inglaterra. A garota Isabella, que não teve o sobrenome revelado, é uma das crianças que se beneficiaram deste projeto colaborativo, no qual mais de 1.000 desenvolvedores de equipamentos feitos por impressora 3D se ofereceram para ajudar quem não tem mão ou braço.

Os criadores do projeto afirmam que as próteses feitas via impressora 3D ajudam a segurar e pegar objetos, mas são mais simples que as próteses convencionais, que têm mais funcionalidades e sistema mais complexo.

**Confira o vídeo**
<iframe width="690" height="388" src="https://www.youtube.com/embed/I3cf49c_WjE" frameborder="0" allowfullscreen></iframe>