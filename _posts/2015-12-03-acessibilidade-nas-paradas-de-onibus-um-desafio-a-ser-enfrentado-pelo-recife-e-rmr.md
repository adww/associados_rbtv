---
layout: post
title: Acessibilidade nas paradas de ônibus&#058; um desafio a ser enfrentado pelo Recife e RMR
except: 
urlsource: http://noticias.ne10.uol.com.br/jc-transito/noticia/2015/12/03/acessibilidade-nas-paradas-de-onibus-um-desafio-a-ser-enfrentado-pelo-recife-e-rmr-584390.php
author: Júlio Cirne, do JC Trânsito | NE10
date: 2015-12-03
categories: news
tags:
- Acessibilidade
- Tecnologia

---

![Mulher em pé com camisa listrada e bolsa cateiro ao lado de homem cadeirante de pepe negra e camisa vermelha][foto1]

Se ser usuário de transporte coletivo na Região ​M​etropolitana do ​R​ecife (RMR)​ já é difícil para quem desfruta de mobilidade plena, a situação fica ainda pior quando se​ tem alg​uma deficiência. Nes​te​ 3 de dezembro, quando é comemorado o ​D​ia Internacional da Pessoa com Deficiência, o JC Trânsito traça um panorama da situação das paradas de ônibus nos principais pontos ​da ​RMR, no que diz respeito às políticas de acessibilidade para atender às necessidades especiais desse grupo que representa 26,75% da população pernambucana, de acordo com o Censo de 2010 do IBGE.​ Infraestrutura precária nos pontos, equipamentos quebrados nos ônibus, abrigos sem proteção do sol e da chuva, além de desrespeito são alguns desses problemas enfrentados diariamente pelos deficientes físicos. 

Veja, na galeria de fotos abaixo, a situação de algumas paradas de ônibus no Centro do Recife:

Um dos locais de maior fluxo de pessoas no Recife, a área Central, principalmente os bairros do Derby e da Boa Vista, é o local que mais recebe críticas de quem precisa se locomover de ônibus para resolver as obrigações cotidianas. Mãe do cadeirante e paratleta Jackson da Silva Ramos, de 25 anos, a dona de casa Lindacy Maria da Silva Ramos, com 48 anos, reclama principalmente da infraestrutura das paradas de ônibus, que não estão preparadas para atender às necessidades especiais de pessoas com deficiência.

"São muito estreitas. Essa (parada de ônibus) aqui da Praça do Derby é maior um pouco, mas a maioria normalmente é pequena e descoberta. Quando chove a gente se molha, quando faz sol também é ruim, pois passamos muito tempo esperando o coletivo no calor”, disse dona Lindacy. Quando conversou com o JC Trânsito, na manhã des​s​​​a quarta-feira (2), a dona de casa vinha de uma consulta médica com Jackson no bairro do Derby. Morando em Jardim São Paulo,​ bairro Zona Oeste do Recife,​ mã​e​ e filho contam que demoram cerca de duas horas quando precisam sair ​de lá para chegar à área central.

Apesar de todas as dificuldades, Jackson não se deixa vencer por elas. ​E​ste ano o jovem começou a jogar basquete, através de um programa de inclusão oferecido pela Universidade Federal de Pernambuco (UFPE). Mesmo com o lugar dos treinamentos sendo mais próximo de sua casa, Jackson mantém a mesma reclamação da mãe: “A parada não é boa e o motorista sempre para longe de onde estamos para subir no ônibus”, disse o jovem. “Isso quando o carro está preparado para ele”, reiterou dona Lindacy, dizendo que alguns veículos ainda não possuem o equipamento necessário para que cadeirantes embarquem nos ônibus.

Jackson encontra dificuldades para embarcar no ônibus, pois o motorista para muito longe de onde ele está. Confira o vídeo:

![Homem andando na cadeira derodas visto de costa][foto2]

De acordo com dados do Grande Recife Consórcio de Transporte, responsável pela operação do transporte coletivo no Recife e RMR, cerca de 5% da frota de 3 mil ônibus cadastrados ainda não contam com o elevador da porta do meio. Veja mais números sobre o transporte abaixo:

!["aaa"][foto3]
<span>Ilustração: Guilherme Castro/NE10</span>

[foto1]: http://imagens4.ne10.uol.com.br/ne10/imagem/noticia/2015/12/03/normal/46edbeba30045752d8ad43185ff0f53a.jpg
[foto2]: http://thumb.mais.uol.com.br/15697575-xlarge.jpg
[foto3]: http://www2.uol.com.br/JC/_ne10/foto/infografico-jc-transito.jpg