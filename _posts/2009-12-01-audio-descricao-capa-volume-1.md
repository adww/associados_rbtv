---
layout: edition
title: Em Defesa da Áudio-descrição&#058; contribuições da Convenção sobre os Direitos da Pessoa com Deficiência
date: 2009-12-01
categories: edition 1
cover: 01.png
issue: 01
release: 2009
authors:
 - Ernani Guedes
 - Lívia Guedes 
tags:
--- 

!['Áudio-descrição abaixo'][capa1]

Fotografia em preto e branco nas dimensões 15 cm x 21 cm e formato retrato, onde se vêem, em primeiro plano, duas figuras humanas - um homem e uma mulher - em um flagrante de carnaval.

No segundo plano, mais ao longe, há pessoas desfocadas, em frente a uma construção de tijolos aparentes, com duas janelas.

Vê-se, em parte, o lado esquerdo de uma mulher jovem, de rosto arredondado, de olhos escuros, a qual fita um ponto à esquerda além da foto. Ela usa chapéu de tonalidade clara e aba circular com laço de fita sobre a copa. A mulher tem a pele morena e traja uma fantasia carnavalesca com mangas volumosas, formadas em parte por tecido e em parte por lantejoulas circulares e brilhantes. Seus lábios, em um suave sorriso, deixam-lhe à mostra os dentes. Ela abraça um rapaz por trás, recostando-lhe a face direita no braço esquerdo, pouco abaixo do ombro.

O homem é jovem, tem rosto alongado, cabelos curtos e crespos e a pele morena. Está com a face voltada para o rosto da mulher que o abraça. Os lábios do homem são grossos e estão levemente abertos. Ele traja uma camisa clara com desenhos de coqueiros no lado inferior esquerdo e usa um cordão escuro com pingente. Na mão direita, segura um pano à altura do abdômen.


[capa1]: {{site.baseurl}}/img/covers/pt-br/01.jpg "Capa da {{site.name}} #1"