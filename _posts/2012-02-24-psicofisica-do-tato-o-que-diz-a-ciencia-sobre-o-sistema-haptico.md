---
layout: article
title: Psicofísica do Tato&#058; O que Diz a Ciência sobre o Sistema Háptico.
except: 
urlsource: 
author: Illiam Schiff e Morton A. Heller
extrainfo: 
date: 2012-02-24
categories: article
tags:
- Acessibilidade
- Tecnologia

---

PREFÁCIO

Os editores querem mostrar que não existe “uma única teoria” do tato. O campo não chegou ao ponto onde nós tenhamos uma harmonia teórica, porque muitas questões permanecem não resolvidas. Nós devemos notar que a háptica compartilha dessa condição com quase todos os outros campos da percepção e da psicologia. Alguns pesquisadores têm adotado uma abordagem gibsoniana (Epstein, Hughes, Schneider, & Bach-y-Rita, 1989; Solomon & Turvey, 1988). Outros têm feito uso de tipos de paradigmas experimentais de processamento de informações (e.g. Horner & Craig, 1989; Manning, 1989); ainda muitos outros pesquisadores não se encaixam num único modelo teórico.

Há mais coisas relativas à psicologia do tato do que “a mão pode alcançar”. Embora muitas pessoas possam não associar o tato à filosofia, existe uma ampla literatura abrangendo questões filosóficas relacionadas à percepção tátil. Claro, a filosofia é apenas uma das muitas áreas relacionadas, também inclui o interesse em identificação de desenhos, reconhecimento de objetos, fisiologia sensorial, relações intermodais, e questões relativas ao desenvolvimento.

O fenômeno sensorial pode ser estudado a partir de muitos pontos-de-vista, como ilustrado na primeira parte deste volume. Os autores desses capítulos discutem várias técnicas psicofísicas usadas para investigar a acuidade de nossos sentidos, a magnitude de nossas sensações, e a natureza das qualidades táteis que nós experienciamos. 

Métodos alternativos de estudarmos o fenômeno sensorial, tratados nesta primeira parte, também incluem introspecção e fisiologia. 

Cholewiak e Collins tratam da base sensorial e fisiológica do tato: uma pesquisa tradicional que tem envolvido tentativas de ligar órgãos receptores na pele com classes de sensações. Seguintes a estes, os próximos dois capítulos, por Stevens e por Rollman, abrangem a sensibilidade térmica à dor. Ambas as áreas envolvem substanciais componentes afetivos. Stevens trata sobre a sensibilidade termal, e Rollman fornece um interessante relato dos problemas envolvidos na mensuração da dor, assim como abrange as principais teorias da sensibilidade à dor. 

A segunda parte é concernente às relações intermodais e o desenvolvimento do tato. Warren e Rossano dão uma visão panorâmica de trabalho em relações intermodais e a influência da visão no tato. No seu capítulo, Bushnell e Boudreau focalizaram a atenção em relações intermodais na infância e early childhood.

A percepção tátil de desenhos é tratada na terceira parte deste livro. Apelle trata sobre a influência da atividade motora na percepção da forma e os atributos da forma. Seu relato é orientado pela idéia de que a natureza dos nossos movimentos manuais determinam o que nós percebemos. Sherrick fornece uma descrição perceptiva da pesquisa sobre estimulação vibrotátil, uma área que tem se demonstrado útil para a comunicação através da pele. Além disso, ele descreve a aplicação de estimuladores vibrotáteis para resolver problemas impostos por limitações visuais ou auditivas. Finalmente, o capítulo de Foulke apresenta uma visão panorâmica da pesquisa sobre a leitura em Braille.

A parte final deste livro trata da percepção tátil em pessoas cegas; uma área que tem fascinado os filósofos por centenas de anos. Embora uma parte das pesquisas sobre percepção tátil em pessoas cegas tem se preocupado com a solução de problemas aplicados, os capítulos nesta parte são principalmente dedicados à evidência empírica sobre questões teóricas em háptica. Heller fornece uma discussão geral sobre a percepção tátil em indivíduos cegos. Kennedy, Gabias, e Nicholls descrevem o desenho de gravuras por pessoas cegas. Millar reconcilia a capacidade de desenhar de crianças cegas com uma aparente/visível dificuldade na interpretação deles de desenhos em linhas em relevo.

CAPÍTULO 1

INTRODUÇÃO

Morton A. Heller Winston-Salem State University

Este livro é sobre o sentido do tato. Abrangemos aqui desde detalhes de fisiologia até questões de comunicação, cognição, e representação. A psicologia do tato inclui a sensitividade cutânea, cinestese, e háptica. O termo háptica incorpora informação cutânea e cinestésica (Revesz, 1950). 

Através da háptica, nós obtemos informações sobre objetos manipulando-os ativamente, com input covariantes cutâneos e cinestésicos (Gibson, 1966). A década de 80 assistiu a um vigoroso debate teórico e desafiadoras descobertas empíricas sobre o tato e a gama de percepções que ele permite. Nós examinaremos o pensamento contemporâneo sobre esses assuntos. Em alguns casos, a discussão esperará para seções mais à frente neste livro. Eu peço a indulgência do leitor nesta questão, e paciência. O leitor interessado fará uma jornada intelectual pelos capítulos deste volume. 

O tato pode envolver formas de perceber e representar a realidade que muitas pessoas uma vez pensaram que era uma reserva exclusiva da visão e audição. Apenas na história recente, as pessoas têm tentado usar o tato como um canal para a leitura, sinalização manual de fala, gravuras, e música (via vibração). Algumas dessas idéias têm sido incrivelmente bem-sucedidas.

A mão é um instrumento extraordinário, mas não é o órgão exclusivo do sentido do tato. Sendo as sensações a fonte da percepção, como muitos pesquisadores têm implícita e explicitamente sustentado, então nós podemos experienciar sensações táteis com toda a superfície de nossa pele. Se, por outro lado, o tato é um conjunto de atividades produzindo vários tipos de informações relativas à estrutura, estado, localização de superfícies, substâncias, e objetos no ambiente, nós usamos muitos tipos de informações hápticas para orientar as nossas atividades. Os pés, por exemplo, fazem uso de informação textural a respeito das superfícies ao andarmos sobre elas. Pense sobre a dificuldade que a pessoa tem de andar sobre o gelo escorregadiço. Nós mudamos nossa passada como resposta a mudanças em fricção, textura da superfície, dureza e temperatura. Pedestres cegos fazem uso de informações hápticas vindas de uma bengala ao andarem mundo afora. Os lábios são instrumentos maravilhosos para se adquirir informação sobre a forma, substância, e talvez intenção e são uma grande fonte de informação sobre forma. 

Critchley (1971, p. 118) mostra uma fotografia de uma criança cega lendo Braille com o nariz. Esta é uma indicação a mais da utilidade de outras superfícies da pele para a percepção de padrões. Nós nos apoiamos muito fortemente no input tátil sobre grande parte de nossa pele ao estarmos envolvidos em muitas atividades perceptuais. Este apoio no sentido do tato muitas vezes passa despercebido, mas em vez de diminuirmos sua importância, deve estimular a consideração quanto a quão limitados têm sido nossos estudos sobre o tato. 

Podemos nos conscientizar de nossa dependência do sentido do tato quando algo o faz funcionar mal, e somente então é que nós reconhecemos a sua importância. Nem a pessoa com hanseníase , nem o acometido pelo diabetes duvidam da importância do input tátil. O tato serve para nos advertir sobre o perigo iminente ou imediato, por exemplo, através da sensibilidade à dor. Além disso, nossa capacidade de manuseio e de exploração do mundo, como fazemos, exige input tátil. Isto fica óbvio quando nós observamos o processo de reaprendizagem quando os astronautas pela primeira vez andaram sobre a superfície da Lua. Até mesmo para sentar utilizamos informação tátil, apenas para nos dizer a mudar de posição. O fato de nos apoiarmos no tato muitas vezes passa despercebido por causa da atenção à percepção visual, e porque nós tendemos a pensar sobre o papel performatório da mão, em vez de sua função sensorial (Gibson, 1962, 1966). Nós usamos nossas mãos para obtermos informações táteis como também para manipularmos objetos. Contudo, muitas de nossas informações táteis vêm de partes do corpo que não as mãos (ver Stevens, 1990). A tendência de indentificar o tato principalmente com as mãos, e a íntima ligação entre performance e percepção, pode ter contribuído para esse viés.

A falta histórica de interesse em formal e seriamente estudar o tato deve ser mencionada. Tradicionalmente, os psicólogos têm tido a tendência de enfatizar o estudo da percepção de padrões visuais ao tentar resolver questões epistemológicas. Muitas das pesquisas sobre a visão têm focalizado na tentativa de entender o reconhecimento de padrões através da percepção de formas. Por exemplo, mais de mil experimentos foram realizados sobre ilusões de ótica, tais como as conhecidas setas de Muller-Lyer. O sentido do tato não parece operar tão eficientemente quanto a visão na detecção de formas de contorno. O tato é, de longe, mais lento, tipicamente scaneia seqüencialmente, e tem um “campo de visão” muito mais restrito. Isto tem levado muitos pesquisadores a achar que o tato é menos importante do que a visão – um modo mais primitivo de perceber que a visão. Nós devemos também notar que a postulada predominância da visão e audição é apenas recente, e que historicamente, o tato tem sido considerado como predominante. Além do mais, muitos diriam que o tato ainda é predominante, pelo menos em termos como uma prova de existência para objetos, isto é, nós testamos a realidade de uma miragem ou imagem onírica tentando tocá-la. 

Este livro tem muito a oferecer a pesquisadores da visão e estudantes de psicologia em geral. Ele sustenta que a investigação do tato é importante e valiosa. Poucos de nós estamos querendo meramente olhar para o nosso mundo. Nossas vidas seriam curtas, de fato, se nós só pudéssemos olhar e não tocar; muitas espécies iriam certamente desaparecer. E mais, muitos dos eventos mais importantes de nossa vida envolvem o sentido do tato. O tato possui um poderoso componente afetivo como também cognitivo. Nós sentimos dor ou prazer, e estes parecem essenciais à existência. Não se pode pensar num indivíduo sem o sentido do tato. Imagine toda a superfície de nossa pele sempre sob o efeito de anestesia! Rollman fornece uma detalhada discussão sobre a dor em seu capítulo neste volume. 

Além do mais, os componentes cognitivos/afetivos da experiência tátil parecem diferentes da experiência visual. Nós podemos prontamente sentir que algo é quente ou frio, suave ou duro, áspero ou macio, além de seus atributos espaciais. Nós podemos sentir a sensualidade de um pedaço muito macio de nogueira, e a fria e impessoal dureza do metal. Uma estilha incita intensas reações emocionais às quais faríamos bem em atender. 

Enquanto nós dependemos de nosso sentido do tato, algumas pessoas se apóiam muito mais intensamente no input tátil. Pessoas cegas usam informação tátil para ler e escrever Braille, para examinarem mapas, e para muito de sua cognição espacial. Pessoas surdas e cegas são ainda mais dependentes de informações táteis, já que lhes falta o contato acústico com o mundo. 

O indivíduo com deficiência múltipla e lesões na coluna pode estar quase completamente limitado ao input tátil. Em seu provocativo livro de ficção “Johnny Got His Gun”, Dalton Trumbo (!970) descreveu a difícil situação de um veterano que perdeu quase todo o input sensorial devido a um trauma físico; Johnny não podia ouvir, nem ver, nem falar, mas aprendeu a se comunicar com uma enfermeira batendo o código Morse com o pouco que restou de sua cabeça. A enfermeira podia falar com ele imprimindo mensagens na pele de seu peito com o dedo dela (pp. 197-199). O indivíduo surdo e cego pode fazer uso de impressão na palma da mão (POP) para se comunicar com pessoas que não são familiarizadas com a sinalização manual, a saber, pessoas com ou sem o sentido da visão (Heller, 1986). Três capítulos neste volume tratam da percepção e desenhos em adultos (Heller, Kennedy et. Al.) E crianças (Millar).

O QUE E POR QUE OS PESQUISADORES SOBRE A VISÃO DEVERIAM SABER SOBRE O TATO

Se todos os sentidos trabalhassem exatamente como o sistema visual, poderia não ser tão importante estudar os sentidos não-visuais. Nós poderíamos aprender tudo sobre percepção apenas estudando a visão. O problema não é, de perto, tão simples, contudo. Muitos aspectos de objetos e espaço podem ser conhecidos igualmente bem através da visão ou tato, mas alguns não podem (ver Warren & Rossano, neste volume). Texturas muito delicadas ou variações de superfícies, por exemplo, são percebidas com precisão pelo tato, mesmo quando a visão falha (Heller, 1989 a.). Além do mais, nós podemos muitas vezes sentir uma estilha (para o nosso desconforto!) quando o mesmo objeto não é visível sem ampliação óptica. O tato pode ser especialmente adequado para a percepção de características do objeto tais como dureza ou suavidade (ver Lederman & Klatzky, 1987). Ou substâncias de substâncias (e.g. pedrinha na comida ou óleo de motor). Alguns aspectos da condutividade termal só podem ser revelados através do tato. O capítulo de Stevens fornece uma discussão sobre a sensação termal. Claro, nós podemos também obter informações sobre a dureza de superfícies vendo e ouvindo eventos que os envolvem. Além do que tato e visão podem nem sempre operar da mesma maneira (ver Day, 1990; Over, 1968). Alguns pesquisadores têm enfatizado limitações do tato, principalmente por causa da natureza seqüencial do processamento (Revesz, 1950; ver Balakrishnan, Klatzky, Loomis, & Lederman, 1989). Mais recentemente, contudo, os pesquisadores têm demonstrado os efeitos da superioridade da palavra na leitura do Braille (Krueger, 1982 b). O efeito se refere à mais rápida e mais precisa detecção de uma letra numa palavra do que numa não-palavra. Como veremos, este é um assunto teórico em curso, que é considerado no capítulo sobre a leitura de Braille por Foulke. 

Enquanto a visão e o tato podem produzir alguns percepts equivalentes, é no campo da experiência sensorial onde as diferenças aparecem mais obviamente. Não há dúvida de que as reações afetivas surgem pela visão. Muitos de nós temos experimentado a grande emoção trazida à tona por um pôr-do-sol, uma cachoeira, ou outro panorama natural de grande beleza. Nós todos conhecemos o impacto emocional da visão de uma pessoa muito atraente. 

As conseqüências afetivas do tato diferem dramaticamente da visão, mas são dignas de investigação por si mesmas. Isto tem incitado o estudo de sensações táteis e sensibilidade. Cholewiak e Collins trata sobre a sensitividade tátil e a fisiologia em seu capítulo. Rollman fornece um relato interessante sobre a sensibilidade à dor, em termos tanto de teoria quanto de aplicação. 

Finalmente, deve ser apontado que pesquisadores interessados no tato têm estudado muitas das mesmas questões teóricas que têm direcionado a pesquisa em visão e outras áreas.

A pesquisa sobre o tato pode fornecer evidências convergentes em alguns problemas teóricos muito intrincados. A pesquisa sobre o tato possui algumas vantagens singulares para o estudo de questões teóricas básicas. O tato é bem adequado para a investigação do papel do movimento revelatório na percepção, isto é, de que mudanças na informação do estímulo são reveladas pela manipulação ativa. É fácil imobilizar ou controlar o movimento da mão, mas tal controle é, na melhor das hipóteses, árduo para a visão, a menos que você limite o campo de visão como através de uma fenda. Contudo, nós devemos notar que a maior inércia mecânica da mão (versus o olho) pode fazer a mão mais fácil de controlar e estudar, mas ela também faz a mão muito mais “desajeitada” para o observador. 

Além disso, a pesquisa tátil nos permite estudar as relações entre visão e um outro sentido. Berkley (1709/1974, p. 302) afirmou que “as idéias de espaço, exterioridade e coisas colocadas a uma distância não são, estritamente falando, objeto da visão... eu nem mesmo vejo a própria distância, nem nada que eu admito estar distante.” Ele sugeriu que nós aprendemos a associar experiências visuais e auditivas com idéias tangíveis. Este tipo de discussão tem levado alguns indivíduos a acreditarem que o tato educa a visão e a audição (e.g. Diderot). Trabalhadores em comportamento motor têm estado interessados na influência da orientação visual na performance de destreza durante algum tempo. A orientação visual pode auxiliar o tato através da provisão de informação redundante, e por movimento de sintonia fina, a fim de otimizar a assimilação da informação. A orientação visual da mão pode ajudar a pessoa a enfrentar a alteração de padrões ao tentar identificar o Braille (Heller, 1989c). 

QUESTÕES TEÓRICAS

Muito da pesquisa do tato tem sido direcionado para a solução de questões teóricas fundamentais. 

Equivalência Sensorial

Os sentidos da visão e do tato podem nos dar as mesmas informações sobre objetos e eventos? Uma versão da tradição empiricista sustenta que os sentidos são originalmente separados e têm que aprender a tratar a informação como equivalente. Nós podemos concluir isso quando nós aprendemos a, digamos, anexar a mesma resposta ou rótulo a alguma coisa que tocamos e sentimos (ver Revesz, 1950). Pesquisas recentes sobre memória, especialmente aquela adotando uma abordagem de processamento de informação, têm tendido a enfatizar as diferenças de modalidade.

Os psicólogos gestálticos tenderam a enfatizar a noção de que nós obtemos estruturas equivalentes de modalidades de visão e tato (ver Marks, 1978). A visão ecológica de Gibson (1966) tem favorecido percepts amodais, isto é, a noção de que a percepção transcende a experiência sensorial de modalidade específica. Atenção a sensações atípicas, e fenômenos subjetivos como a dor ou frio, levam-nos a notar as diferenças entre as modalidades. Assim, nós vemos a cor, mas sentimos a dor. Quando nós identificamos objetos, contudo, pode importar pouco que sentido obtém a “informação” relevante. O capítulo de Warren e Rossano preocupa-se com relações intermodais, especialmente a relação entre a visão e o tato. 

Representação: Imagem Visual, Cognição no Cego, e Imagem Tátil

Há pouca dúvida de que a aprendizagem desempenha um papel fundamental no desenvolvimento de habilidades táteis. Basta observar uma pessoa que é habilidosa no uso desse sentido, usando Braille, ou uma bengala para se compreender o tremendo papel que a aprendizagem deve desempenhar.

Papel do Movimento Receptor: Tato Ativo versus Tato Passivo

Pesquisadores do tato têm, por muito tempo, ficado fascinados pelo papel do movimento na percepção (Katz, 1989; Revesz, 1950). Gibson (1962, 1966) tem sido mais amplamente citado para esta discussão entre o tato ativo e passivo. De acordo com Gibson, o tato é passivo quando o observador não se move e a informação é imposta sobre a pele. O tato ativo consiste de um movimento auto-produzido que permite ao perceptor obter informação objetiva sobre o mundo. Gibson argumentou que o tato passivo é atípico de experiências “normais”. Nós, na maioria das vezes obtemos informações sobre o mundo através de movimento intencional. 

O movimento ativo permite que se atinja percepts “amodais” que não estão ligados a uma modalidade específica, e permite a interpretação de que muito do que se aprende na percepção tátil são estratégias ou exploratórias ou manipulatórias que revelam informações mais úteis. 

Várias pessoas podem usar o Optacon, um aparelho que traduz o input visual (letras e números) para um mostrador vibratório. Essas pessoas movem ativamente a câmera do Optacon com a mão direita, mas o dedo indicador fica passivo no display vibrotátil.

Muitos pesquisadores têm enfatizado a natureza ativa do tato. A partir desse ponto de vista, a percepção é uma realização que pode depender de performance treinada. Esta tradição é consistente com uma tendência recente em direção a uma ênfase da natureza ativa da cognição; embora o local de atividade seja principalmente na cabeça, em vez de na mão, onde ela tende a ser metaforicamente colocada por pesquisadores na tradição gibsoniana. 

Percepção Total versus Percepção Parcial Processamento Serial versus Processamento Paralelo

Nós processamos input tátil em forma serial ou paralela? Por exemplo, a pessoa pode pensar se o leitor do Braille assimila um carácter por vez, ou percebe a palavra como uma unidade perceptual básica?

Dominância Sensória

Nós temos um conflito intersensório apenas se nós acreditamos que sentimos um objeto unitário, mas os sentidos não nos dizem a mesma história sobre este objeto único. Isto pode acontecer se nós observarmos um remo na água, para tomar o clássico exemplo de John Locke. A aparência do remo é distorcida pela juntura do ar e da água. O remo parece torto, mas a sensação é de que ele é reto. Em que sentido nós acreditamos? (ver Welch & Warren, 1980)?

Um sentido com maiores poderes de alerta, a saber a audição, pode ser dominado pela visão durante o efeito do ventriloquismo; a audição pode ser ignorada quando “boa” informação visual está disponível (Schiff & Oldak, 1990).

Uma pessoa pode interpretar os experimentos de dominância com indicando que não é um sentido que é dominado, em vez disso, um atributo, por exemplo, direção. Assim, quando nós caímos vítimas do “efeito do ventriloquismo”, nós erramos em nossa crença de que o boneco disse alguma coisa. Este é um erro na fonte de fala, não no que foi dito.

Efeitos de Lateralidade e Especialização Hemisférica

Pesquisadores do tato, como da visão, têm estado preocupados com a especialização hemisférica. A pessoa pode pensar que a mão esquerda seria melhor para a percepção espacial, se o lado direito do cérebro fosse especializado para esta habilidade. Tem havido demonstrações de vantagens da mão esquerda para a leitura de print-on-palm (Heller, 1986).

Muitos pesquisadores acreditam que o hemisfério direito é especializado para o processamento do Braille e de outras informações táteis.

A literatura sobre a leitura do Braille sugere que a lateralização pode mudar com a experiência intelectual e cognitiva (Harris, 1980; Karavatos, Kaprinis, & Tzavaras, 1984).

BREVE HISTÓRIA 

Os médicos têm estado interessados na percepção da dor, e desordens do funcionamento tátil. 

Diderot também fornece uma ilustração do conflito intersensório de como lentes de aumento podem levar os sentidos da visão e do tato a um conflito, de modo que as impressões visuais de um objeto são diferentes. 

Diderot descreveu a importância das exigências de memória no sentido do tato, e acreditava que uma impressão de formas depende da retenção de sensações componentes.

Fenômeno Sensorial e Psicofísica

Weber (1934/1978) deve ser considerado o pai do estudo sistemático da sensitividade da pele. Ele dedicou tempo considerável à investigação da resolução espacial da superfície da pele. Além disso, Weber forneceu um relato abrangente sobre a variação em sensibilidade a propriedades termais, peso, e outras propriedades táteis. 

No transcurso de suas investigações empíricas da sensação cutânea, Weber formulou o que mais tarde se tornou sua famosa “lei” . A lei de Weber: a capacidade da pessoa de discriminar diferenças entre um estímulo padrão e de comparação é uma função constante da magnitude do padrão. Assim, a pessoa precisa de uma diferença muito maior entre os pesos para discriminá-los com sucesso quando o padrão pesa 100 gramas do que quando o padrão pesa 20 gramas. A proporção, claro, permanece constante ao longo de uma amplitude moderada de estímulo. Fechner mais tarde traduziu esses princípios numa fórmula matemática. Nós devemos lembrar que este importante avanço em psicologia dependeu em grande parte da investigação dos sentidos do tato e cinestesia.

Baseado em evidência empírica, Weber sugeriu que a mão esquerda é mais sensitiva ao peso e propriedades termais dos objetos (ver Harris, 1980; Heller et al., 1990).

Teorias da Sensação

Os pesquisadores têm muitas vezes tentado associar os receptores na pele com qualidades sensórias específicas. 

Nós sabemos que o corpúsculo paciniano responde à vibração. Outras estruturas estão relacionadas à sensação termal.

Henry Head distinguiu entre a sensibilidade epicrítica e protopática. Ele achava que os dois tipos de sensibilidade eram servidos por mecanismos diferentes de transmissão neural. A sensibilidade epicrítica envolve o tato leve, discriminação fina de temperatura, e localização espacial (Sinclair, 1967). A sensação protopática envolve diferentes fibras nervosas e serve à dor e extremos de temperatura. A teoria pode representar uma supersimplificação, e a evidência fisiológica não apóia claramente todos os aspectos da teoria (Sinclair, 1967). 

Raízes Clínicas 

Vários autores têm estudado o tato num setting clínico. O tato pode ser suscetível a dramáticas distorções perceptuais devido a traumas na cabeça ou danos neurológicos. O fenômeno do membro fantasma, no qual uma pessoa sente dor numa parte do corpo não existente, é um dos mais admiráveis exemplos de experiência tátil ilusória. 

História Moderna: Katz, Revesz, e Gibson

Katz, Revesz, e Gibson têm tido um forte impacto na pesquisa sobre o tato. Muitos pesquisadores têm sido influenciados pelos seus textos. Um curso comum ao longo do trabalho deles é a ênfase na importância do movimento da mão para a percepção.

Katz (1989) enfatizou o papel essencial do movimento para a percepção tátil. O movimento das mãos ou do estímulo pareciam essenciais para a percepção de texturas. Katz notou que o sentido do tato era superior à visão para julgamentos de atributos tais como a espessura do papel ou a presença de vibração. O tato pode certamente superar a visão em julgamentos de textura (Heller, 1989 a). 

A háptica opera através do princípio estereoplástico, uma tendência de apreender um objetoi de todos os lados. Isso pode apresentar um problema quando nos defrontamos com 2-D. Um scanning quase simultâneo pode ser mais provável com objetos menores que cabem na mão, ou nas duas mãos juntas, ou talvez nos braços.

PARTE I

Fenômeno Sensório

Nesta seção, os autores começam a análise da psicologia do tato considerando-a de maneira clássica, isto é, com um sentido proximal passando uma variedade de sensações (e. g., pressão, dor, calor) para o SNC (sistema nervoso central) quando receptores, ou conjuntos de receptores são ativados por energias de estímulos. Tal visão do tato conceitualiza as questões em torno de nosso conhecimento do mundo através do tato, principalmente como um conjunto de problemas preocupados em como um sistema neural de transdução armazena e converte energias e eventos que ocorrem nas interfaces do ambiente-organismo em códigos neurais para serem analisados pelo cérebro. Enquanto esta não é a única forma possível de conceitualizar a base fundamental para a percepção tátil/háptica (e.g. ver Gibson, 1966) é a adotada pela maioria dos psicólogos, e virtualmente todos os fisiologistas sensoriais. 

Pesquisadores sobre a dor são confrontados com paradoxos difíceis, mas importantes. A dor é um fenômeno “subjetivo”, mas nós muitas vezes tentamos medi-la “objetivamente”. Rollman tem se debatido com várias questões teóricas e aplicadas relativas à percepção da dor."

Gostou de saber deste assunto? Quer saber mais? Então, [compre o livro "The Psychology of Touch" aqui][1]

[1]: http://www.amazon.com/Psychology-Touch-Morton-Heller/dp/0805807519/ref=sr_1_1?ie=UTF8&qid=1330037092&sr=8-1