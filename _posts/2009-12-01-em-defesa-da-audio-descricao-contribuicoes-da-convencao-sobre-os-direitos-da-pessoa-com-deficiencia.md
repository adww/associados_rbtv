---
layout: edition
title: Em Defesa da Áudio-descrição&#058; contribuições da Convenção sobre os Direitos da Pessoa com Deficiência
date:  2009-12-01
categories: edition 1
cover: 01.png
issue: 01
release: 2009
authors:
 - Francisco J. Lima
 - Rosângela A. F. Lima
 - Lívia C. Guedes
tags:
--- 

## Resumo

O presente artigo alerta sobre a necessidade e urgência da divulgação/conscientização dos usuários sobre o  serviço da áudio-descrição. Denuncia que milhares de pessoas ficam diariamente alijadas do direito constitucional ao lazer e à educação, total ou parcialmente, pois a programação televisiva, tanto quanto as de cinema, teatro e das casas de cultura, mostra de artes, feira de artes e de museus, não são acessíveis ao público com deficiência, invariavelmente por falta de acessibilidade física e, certamente, devido às barreiras atitudinais e comunicacionais, advindas da falta da oferta de áudio-descrição  das imagens e outras configurações visuais, que se tornam  inacessíveis às pessoas com deficiência visual, por conta da não oferta desse serviço assistivo. Sustenta o direito de as pessoas com deficiência terem áudio-descrição   na Lei nº 10.098, no Decreto Federal   5.296/2004  e  no Decreto Legislativo 186/2008.  Os autores fazem a assertiva de que o reconhecimento legal, nacional e internacional dos direitos não basta para garantir às pessoas com deficiência o desfrute de todos os seus direitos. Concluem que é urgente  que os operadores do direito tanto quanto os indivíduos com deficiência, detentores do direito à áudio-descrição,  saibam interpretar as leis garantidoras desse serviço assistivo, entendê-lo, respeitá-lo e garanti-lo em todas as suas formas e instâncias.

## Abstract

The current article warns about the need and urgency of making audio description service more widely available , as well as about the need of making users aware of the benefits of such assistive tecnology. It denounces the fact that thousands of people remain lacking their constitutional right to education and entertainment, whether total or partially blind, since programs on television, as well as cinema films, theaters and houses of culture, art exhibts, fairs of arts and museums remain inaccessible to disabled citizens, as a rule, due to lack of physical accessibility and, certainly, also due to attitudinal and communication barriers, wherever there is no offer of audiodescription of images, this way, they become inaccessible to people with disabilities. This article stresses the right to audiodescription based on the following brazilian laws:  "Lei nº 10.098";  "Decreto Federal   5.296/2004" and on "Decreto Legislativo 186/2008". The authors state that legal, national and international, acknowledgement of these rights is not bottom-line in warranting that people with disabilities will actually enjoy them. They conclude that it is urgent that law operators as well as people with disabilities, owners of the right to audiodescription, know how to: interpret the laws which guarantee this assistive service; understand such right; and guarantee it in all its forms and instances.  

## 1 - Introdução

Segundo a ABERT (Associação Brasileira de Emissoras de Rádio e Televisão), já em 2004 existiam 186 geradoras de programação no Brasil, dentre as quais 34 estavam no estado de São Paulo.

De acordo com a Wikipedia, existem no Brasil 10 estações de TV comerciais (Band; RBTV; CNT; Rede Diário; Rede Gazeta; Rede Globo; NGT; Rede Record; SBT; Rede TV);  03  estatais (TV Brasil; NBR; Tv Cultura);  2 legislativas (TV Câmara; TV Senado); 1 judiciária (TV Justiça); 36 segmentadas (Agronegócio: Canal do Boi; Terra Viva; Agro Canal; Novo Canal; Canal Rural; Educativas: TV Cultura; Sesc TV; Canal Futura; TV Escola; Paraná Educativa; Esporte: TV Esporte Interativo; Jovem: MixTV; M1 Station TV; MTV Brasil; Rede União; TV Mundial; Rede 21; PlayTV; Notícias: Record News; RIT Notícias; Religiosas: Rede Gênesis; Rede Gospel; Rede Boas Novas; TV Aparecida; TV Canção Nova; TV Século 21; Rede Vida; RIT TV; Rede Super; TV Mundo Maior; TV Novo Tempo; Rede Familia; Vendas: Rede TV+; Polishop TV; Shoptime.com; Shop Tour).

Isso significa que milhares de horas semanais de transmissão televisiva, englobando lazer, cultura, educação, etc., chegam às casas dos milhões de brasileiros espalhados entre os grandes centros e os locais mais longínquos do nosso país. E destes milhões de brasileiros, 24,6 milhões têm alguma deficiência. Segundo o IBGE (Censo 2000), 14,5% da população total do Brasil apresentam algum tipo de deficiência, são pessoas com ao menos alguma dificuldade de enxergar, ouvir, locomover-se, ou com alguma deficiência física ou mental.

Ainda segundo o IBGE, entre 16,6 milhões de pessoas com algum grau de deficiência visual, quase 150 mil se declararam cegos.

É importante destacar também que a proporção de pessoas com deficiência aumenta com a idade, passando de 4,3% nas crianças até 14 anos, para 54% do total das pessoas com idade superior a 65 anos. À medida que a estrutura da população está mais envelhecida, a proporção de pessoas com deficiência aumenta, surgindo um novo elenco de demandas para atender às necessidades específicas deste grupo.

Dentre as pessoas com deficiência estão também aquelas com deficiência física, que não podem manter-se em posição para assistir televisão (dependendo das informações auditivas mais do que das visuais advindas da televisão).

Todas essas pessoas, porém não só elas, ficam diariamente alijadas do direito constitucional ao lazer e à educação, total ou parcialmente, devido ao fato de que a programação televisiva, tanto quanto a de cinema, teatro e, por vezes, das casas de cultura, mostra de artes, feira de artes, museus, etc., não são acessíveis a esse público, invariavelmente por falta de acessibilidade física e, certamente, devido às barreiras atitudinais e comunicacionais, mormente aquelas advindas das imagens e outras configurações visuais não descritas, portanto, inacessíveis à pessoa com deficiência visual.

Compondo o grupo de pessoas excluídas do acesso aos conteúdos televisivos, de cinema, teatro, museus e outros, estão também as pessoas dislexas ou que são analfabetas, pessoas que  têm dificuldade e, às vezes, estão totalmente impedidas de entender o conteúdo escrito, por exemplo, aqueles encontrados em filmes legendados ou em informações por escrito, disponíveis aos visitantes de museus e similares.

De acordo com dados da Associação Brasileira de Dislexia, as pessoas com esse transtorno ou distúrbio de aprendizagem têm a leitura, a escrita e a soletração comprometidas, sendo este considerado “o distúrbio de maior incidência nas salas de aula”. A ABD aponta, ainda, que “Pesquisas realizadas em vários países mostram que entre 05% e 17% da população mundial é disléxica”. 

Em relação aos analfabetos, estima-se que, no ano de 2002, o número de brasileiros analfabetos chegou a 14,6 milhões, sendo “11,8% da população de 15 anos ou mais de idade, contra 17,2% em 1992. O País tinha 32,1 milhões de analfabetos funcionais, e 65,7% dos estudantes com 14 anos de idade estavam defasados”. Todo  esse contingente de pessoas analfabetas ou alfabetizadas funcionais estão alijadas do direito de acesso à cultura, advindo de filmes legendados, simplesmente pelo fato de esses filmes não terem legendas áudio-descritas. Entretanto, são as pessoas com deficiência  visual que, em grande número, mais se beneficiarão da áudio-descrição e que, sem ela, têm o seu direito de acesso à comunicação, à educação e  à  cultura denegados, parcial ou totalmente. 

Como fica patente, não podemos continuar a ignorar todas essas pessoas e suas necessidades especiais de acesso aos bens e serviços, dentre os quais a cultura e a educação, enquanto bens, e a áudio-descrição, enquanto serviço para aquisição desses bens.

## 2 - Marco Legal

Vindo transformar essa realidade excludente e de negação de direitos constitucionais às pessoas com deficiência visual (cegas ou com baixa visão), com dislexia, com algumas deficiências físicas e intelectuais, bem como de modo a ampliar o acesso à cultura e à educação aos milhares de cidadãos brasileiros analfabetos, em 19 de dezembro de 2000, promulgou-se a Lei Federal nº 10.098, importante lei sobre a acessibilidade comunicacional que, quatro anos mais tarde, seria regulamentada pelo Decreto Federal 5.296, de dezembro de 2004.

> Art. 17. O Poder Público promoverá a eliminação de barreiras na comunicação e estabelecerá mecanismos e alternativas técnicas que tornem acessíveis os sistemas de comunicação e sinalização às pessoas portadoras de deficiência sensorial e com dificuldade de comunicação, para garantir-lhes o direito de acesso à informação, à comunicação, ao trabalho, à educação, ao transporte, à cultura, ao esporte e ao lazer. (Lei Nº. 10.098/2000).

De acordo com o decreto que regulamenta a Lei supracitada:


> **Art. 5** 
> **2**. Caberá ao Poder Público incentivar a oferta de aparelhos de televisão equipados com recursos tecnológicos que permitam sua utilização de modo a garantir o direito de acesso à informação às pessoas portadoras de deficiência auditiva ou visual. 
> 
> Parágrafo único. Incluem-se entre os recursos referidos no caput:  
> I - circuito de decodificação de legenda oculta; 
> II - recurso para Programa Secundário de Áudio (SAP); e 
> III - entradas para fones de ouvido com ou sem fio. 
> 
> **Art. 53**. A ANATEL regulamentará, no prazo de doze meses a contar da data de publicação deste Decreto, os procedimentos a serem observados para implementação do plano de medidas técnicas previsto no art. 19 da Lei no 10.098, de 2000. 
> 
> § 1o O processo de regulamentação de que trata o caput deverá atender ao disposto no art. 31 da Lei no 9.784, de 29 de janeiro de 1999. 
> § 2o A regulamentação de que trata o caput deverá prever a utilização, entre outros, dos seguintes sistemas de reprodução das mensagens veiculadas para as pessoas portadoras de deficiência auditiva e visual: 
> I - a sub-titulação por meio de legenda oculta; 
> II - a janela com intérprete de LIBRAS; e 
> -III - a descrição e narração em voz de cenas e imagens.- (grifo nosso, Decreto Nº. 5.296/2004)

Tais instrumentos legais ampliaram significativamente o conceito de acessibilidade à comunicação, tanto trazendo às pessoas surdas a legenda, em  close caption, e janela com língua de sinais, quanto trazendo às pessoas cegas a áudio-descrição, em canal secundário de áudio (canal sap). Não se omitindo quanto às barreiras comunicacionais em outras instâncias, determinaram que esse acesso deve se dar também em eventos educacionais/acadêmicos, em conferências, congressos, seminários etc., onde quer que imagens sejam exibidas e pessoas com deficiência visual delas necessitem conhecer, para o lazer, educação ou outra razão.

> Art. 59. O Poder Público apoiará preferencialmente os congressos, seminários, oficinas e demais eventos científico-culturais que ofereçam, mediante solicitação, apoios humanos às pessoas com deficiência auditiva e visual, tais como tradutores e intérpretes de LIBRAS, ledores, guias-intérpretes, ou tecnologias de informação e comunicação, tais como a transcrição eletrônica simultânea. (Decreto Nº. 5.296/2004).

Em uníssono com nossa Carta Maior, em junho de 2006, o Ministério das Comunicações publicou a Portaria 310, tornando obrigatória a acessibilidade na programação das televisões abertas, em todo o território nacional. Assim, ficou determinada a obrigatoriedade de veiculação diária de programas com acessibilidade (no caso das pessoas com deficiência visual, pela oferta da áudio-descrição), a princípio com duas horas, devendo progressivamente ir aumentando até chegar a programação total.

O Ministério das Comunicações, ainda, concedeu carência de dois anos para que as emissoras se preparassem para iniciar suas transmissões com áudio-descrição e a legenda oculta em seus programas, respondendo ao previsto pelo referido Decreto e à lei de acessibilidade comunicacional a que ele regulamentava.

> 9.1.1 No prazo de 2 (dois) anos, contado a partir da publicação desta Norma, para as estações transmissoras ou retransmissoras localizadas em cidades com população superior a 1.000.000 (um milhão) de habitantes. (Portaria 310, de 27 de junho de 2006).

Em julho de 2008 o Ministério das Comunicações publicou a portaria 466, agora dando prazo de 90 dias para que as emissoras passassem a incluir a áudio-descrição em seus programas, nos termos da mencionada Portaria 310 de 2006. 

Em 14 de outubro de 2008, porém, o Ministro de Estado das Comunicações, Ministro Hélio Costa, assinou a Portaria nº 661, suspendendo a portaria anterior, não revogando, contudo, o previsto pelo Decreto 5.296 de 2006 e demais dispositivos legais que sustentam a acessibilidade comunicacional, já apresentados neste texto.

Rezava, então, a Portaria de 30 de julho de 2008, na qual o Ministério das Comunicações definia o prazo de noventa dias para que se começasse a oferta de áudio-descrição em canais televisivos:

> Portaria  nº 466, de 30 de julho  de 2008
> 
> O MINISTRO DE ESTADO DAS COMUNICAÇÕES, no uso das atribuições que lhe confere o art. 87, parágrafo único, incisos II e IV, da Constituição,
> 
> Considerando que a Lei no 10.098, de 19 de dezembro de 2000, incumbe ao Poder Público promover a eliminação de barreiras na comunicação e estabelecer mecanismos e alternativas técnicas que tornem acessíveis os sistemas de comunicação às pessoas com deficiência sensorial e com dificuldade de comunicação para garantir-lhes o direito, entre outros, de acesso à informação, à comunicação, à cultura, e ao lazer,
> 
> Considerando que o Decreto no 5.296, de 02 de dezembro de 2004, que regulamenta a mencionada Lei, alterado pelo Decreto no 5.645, de 28 de dezembro de 2005, estabeleceu a competência do Ministério das Comunicações para dispor, em Norma Complementar, acerca dos procedimentos para a implementação dos mecanismos e alternativas técnicas acima referenciados, determinando que esses procedimentos deveriam prever a utilização de subtitulação por meio de legenda oculta, janela com intérprete de LIBRAS e a descrição e narração em voz de cenas e imagens,
> 
> Considerando que, além de investimentos, a implementação desses recursos de acessibilidade pelas exploradoras de serviços de radiodifusão de sons e imagens, nos termos do cronograma constante da Norma no 001/2006, aprovada pela Portaria no 310, de 27 de junho de 2006, requer mão-de-obra especializada em quantidade suficiente para atender a demanda do setor, 
> 
> Considerando o requerimento apresentado pela Associação Brasileira de Emissoras de Rádio e Televisão - ABERT em que noticia ser a quantidade de profissionais especializados na produção do recurso de áudio-descrição, existente atualmente no mercado nacional, insuficiente para atender, nos termos do cronograma supracitado, a demanda do setor de radiodifusão de sons e imagens, e 
> 
> Considerando ainda que, na busca de solução para a questão apresentada, o Ministério das Comunicações, em 23 de julho daquele ano, promoveu reunião com representantes do setor de radiodifusão, do setor de produção de áudio-descrição, do Comitê Brasileiro de Acessibilidade e da União Brasileira de Cegos na qual obteve a garantia, dos representantes do setor de produção de áudio-descrição e do representante do Comitê Brasileiro de Acessibilidade e da União Brasileira de Cegos, de que a demanda requerida pelo setor de radiodifusão poderia ser atendida dentro do prazo de três meses com a formação de, aproximadamente, cento e sessenta profissionais com a qualificação exigida para a produção de áudio-descrição, resolve:
> 
> Art. 1o Conceder o prazo de noventa dias, contado da data de publicação desta Portaria, para que as exploradoras de serviço de radiodifusão de sons e imagens e de serviço de retransmissão de televisão (RTV) passem a veicular, na programação por elas exibidas, o recurso de acessibilidade de que trata o subitem 3.3 da Norma Complementar no 01/2006, aprovada pela Portaria no 310, de 27 de junho de 2006, ficando mantidas as demais condições estabelecidas no subitem 7.1 da mesma Norma.
> 
> Art. 2o Esta Portaria entra em vigor na data de sua publicação.
> 	
> HÉLIO COSTA

## 3 -  O direito à Áudio-descrição no contexto do Decreto Legislativo 186 de 9 de julho de 2008


Seria redundância  legal advogar pelo direito da pessoa com deficiência aos bens e serviços culturais, bem como à equiparação de condições se esse direito fosse de pronto  respeitado. Acontece que não o é! Como confirma a Convenção sobre os Direitos da Pessoa com Deficiência, “(...) as pessoas com deficiência continuam a enfrentar barreiras contra sua participação como membros iguais da sociedade e violações de seus direitos humanos em todas as partes do mundo”. Portanto, também no Brasil.

Fosse nossa Carta Maior respeitada na íntegra,  nenhuma outra lei seria necessária se a pessoa com deficiência fosse, realmente, reconhecida como pessoa, e enquanto tal fosse percebida como tendo direitos, não iguais às demais, mas consoante as suas próprias necessidades e/ou características específicas, visto que é assim que a Constituição Brasileira proclama.

A todos deve ser garantido o direito de ir e vir, às pessoas com deficiência devem adicionalmente ser garantidos os meios/recursos para que exerçam aquele direito.

A todas as crianças é devido o direito à educação, às crianças com deficiência este direito deve ser acompanhado pelo direito de acesso à escola, de acesso aos ambientes educacionais, de acesso aos meios e recursos que viabilizem a educação, etc.  Corrobora nosso entendimento, o fato de  a referida Convenção reconhecer “a importância da acessibilidade aos meios físico, social, econômico e cultural, à saúde, à educação e à informação e comunicação, para possibilitar às pessoas com deficiência o pleno gozo de todos os direitos humanos e liberdades fundamentais” (Decreto Legislativo 186/08).

Assim é que, sustentados na  Convenção, defendemos que o direito à cultura, ao lazer e tudo o mais que advém da aprendizagem pela cultura e da saúde pelo lazer,  devem estar garantidos a todas as pessoas, e isso significa  garanti-los a todas as pessoas, sem qualquer adjetivação. No entanto, a adjetivação de um indivíduo em pessoa com deficiência requer tratamento desigual, sem o que, é sabido, não se promoverá a igualdade dessas pessoas com aquelas sem tal adjetivação.  Portanto, a “re-edição” de dispositivo garantidor  do direito da pessoa com deficiência não se trata de mera redundância. De fato, não fosse legislar pelo direito das crianças com deficiência, pelo direito das mulheres com deficiência, pelo direito dos trabalhadores com deficiência, enfim, pelo direito das pessoas com deficiência, estas não seriam tidas como pessoas, trabalhadores, mulheres ou crianças.

Sumariando e, por assim dizer, re-editando a Carta Universal dos Direitos da Pessoa Humana, agora com a adjetivação de pessoa humana com deficiência, países de todo o mundo se unem para dizer que as pessoas com deficiência são pessoas, e são pessoas com deficiência  que requerem  respeito e cuidado, consoante suas necessidades, porém sem paternalismos e sem privilégios. De fato, reconhecer-lhes os direitos, garantir-lhes o acesso a esses direitos é  efetivamente dever de cada um  dos indivíduos da sociedade universal, e  certamente  não é privilégio e nem paternalismo.

Em uníssono com este entendimento, o Brasil reconhece e ratifica os ditames da Convenção sobre os Direitos das Pessoas com Deficiência, na forma de decreto legislativo com força de emenda constitucional (Decreto Legislativo 186/2008).

Por si só este feito é um dos mais significativos passos no reconhecimento de que o cidadão brasileiro com deficiência é pessoa humana, como  foi definido, pela primeira vez em nosso país, em nossa Constituição de 1988.

Não obstante, o reconhecimento legal, nacional e internacional dos direitos da pessoa com deficiência não é suficiente para garantir a essas pessoas o desfrute de todos os seus direitos. É mister que os operadores do direito tanto quanto os cidadãos com deficiência, detentores desse  direito, saibam interpretá-lo, entendê-lo, respeitá-lo e garanti-lo, em todas as suas formas e instâncias.

Assim é que  se reconhece na Convenção que: “a deficiência é um conceito em evolução e que a deficiência resulta da interação entre pessoas com deficiência e as barreiras devidas às atitudes e ao ambiente que impedem a plena e efetiva participação dessas pessoas na sociedade em igualdade de oportunidades com as demais pessoas”. Em outras palavras, reconhece-se  que a sociedade e suas barreiras atitudinais  impõem incapacidades a essas pessoas, que quando muito teriam limites inerentes às suas deficiências.

Máxime para as pessoas cegas, a advocacia de seus direitos passa pela própria educação dos indivíduos com deficiência visual, quanto aos serviços a que têm direito e dos meios ou vias para alcançá-los. Como afirmamos acima, não se trata de requerer privilégios, mas de saber interpretar os instrumentos jurídicos como ferramentas garantidoras da igualdade de acesso e desfrute  do que está socialmente disponível às pessoas não cegas.

Neste artigo fazemos um recorte do direito de acesso à comunicação, à informação, à cultura, à educação e aos demais bens culturais, por  meio da áudio-descrição de eventos visuais, os quais sem este recurso limitam e/ou impedem o pleno exercício do direito à educação, ao lazer e à cultura em geral.

A Convenção sobre o direito das pessoas com deficiência, por mais de uma vez, nos permite sustentar a tese do direito à áudio-descrição, considerando a intencionalidade da Convenção e dos pressupostos que a sustentam. 

Já no primeiro artigo, a Convenção  nos alerta para o fato de que as pessoas com deficiência encontram barreiras físicas e sociais  que “podem obstruir sua participação plena e efetiva na sociedade em igualdades de condições com as demais pessoas” (Decreto 186/2008).

Não é difícil perceber que a barreira comunicacional, advinda da ausência de áudio-descrição, por exemplo nas peças publicitárias, quanto ao uso de preservativos em que se pretende a educação das pessoas a respeito de DST/Aids, exclui da população alvo dessas peças as pessoas cegas ou com baixa visão a quem tais informações visuais  não chegam.

Ora, o direito à saúde é direito de todas as pessoas, sejam elas sem deficiência ou com essa adjetivação. Não propiciar, portanto, igualdade de acesso à informação para as pessoas com deficiência visual é discriminá-las por razão de deficiência, uma vez que não é a cegueira que as impede de receber a informação, mas o obstáculo  ocasionado pela falta da áudio-descrição, a qual é, em última instância, uma alternativa comunicacional para os eventos visuais.

> "Discriminação por motivo de deficiência" significa qualquer diferenciação, exclusão ou restrição baseada em deficiência, com o propósito ou efeito de impedir ou impossibilitar o reconhecimento, o desfrute ou o exercício, em igualdade de oportunidades com as demais pessoas, de todos os direitos humanos e liberdades fundamentais nos âmbitos político, econômico, social, cultural, civil ou qualquer outro. Abrange todas as formas de discriminação, inclusive a recusa de adaptação razoável;” (Decreto Legislativo 186/2008)

O acesso à comunicação, no sentido mais amplo, está previsto na referida Convenção, conforme se pode ler:


> Artigo 2 
> Definições Para os propósitos da presente Convenção: 
> "Comunicação" abrange as línguas, a visualização de textos, o braille, a comunicação tátil, os caracteres ampliados, os dispositivos de multimídia acessível, assim como a linguagem simples, escrita e oral, os sistemas auditivos e os meios de voz digitalizada e os modos, meios e formatos aumentativos e alternativos de comunicação, inclusive a tecnologia da informação e comunicação acessíveis; (Decreto Legislativo 186/2008)

Consideremos, agora, o artigo terceiro, mormente quando diz:

Princípios gerais. Os princípios da presente Convenção são: 
a) O respeito pela dignidade inerente, a autonomia individual, inclusive a liberdade de fazer as próprias escolhas, e a independência das pessoas. 
c) A plena e efetiva participação e inclusão na sociedade; 
e) A igualdade de oportunidades; 
f) A acessibilidade; (Decreto Legislativo 186/2008) 

Seria flagrante afronta a tais princípios negar a uma pessoa com deficiência visual o direito de, por si só, decidir o que e quando assistir na televisão, cinema ou mesmo num DVD, condicionando-a tomar tal decisão se e quando uma pessoa vidente lhe estivesse disponível para ler a legenda do DVD, descrever  a cena de um filme, ou o número de telefone exibido na tela da televisão.

É mister, então, frisar que cada vez mais as pessoas precisam de conhecimentos culturais gerais, muitos dos quais veiculados na televisão, noticiários, documentários, etc, para a obtenção  de emprego, por exemplo. 

Assim, a áudio-descrição vem constituir-se numa ferramenta de acesso laboral tanto quanto para o lazer e para a educação. Se às pessoas videntes está garantido o  acesso às informações visuais, estas devem, igualmente, serem disponibilizadas às pessoas com deficiência visual. De outra forma, essas pessoas estarão novamente sendo discriminadas por razão de deficiência, já que nem mesmo o conceito de “adaptação razoável” pode servir de justificativa para a não oferta da áudio-descrição.

Considerando as grandes cifras destinadas à produção das obras televisivas e de cinema, o investimento de um percentual mínimo para a áudio-descrição não pode ser justificativa razoável para denegar direito fundamental da pessoa com deficiência visual.

Ademais, uma vez áudio-descrito um filme, por exemplo, a áudio-descrição pode ser agregada como mais um produto derivado de uma dada obra. Por exemplo, poder-se-á em um CD divulgar o áudio original do filme, acrescido da áudio-descrição, o que permitirá que um motorista ouça seu filme no carro, enquanto dirige. A áudio-descrição permitirá com que ele veja em sua mente, aquilo que temporariamente seus olhos não podem alcançar.

Com a adoção  da Convenção como emenda constitucional à nossa Carta Maior, o Brasil se compromete a pesquisar e desenvolver recursos de acessibilidade, eliminando em todas as instâncias, pública ou privadas, barreiras comunicacionais, atitudinais e outras, de modo a respeitar os direitos fundamentais da pessoa com deficiência. 

> f) Realizar ou promover a pesquisa e o desenvolvimento de produtos, serviços, equipamentos e instalações com desenho universal, conforme definidos no Artigo 2 da presente Convenção, que exijam o mínimo possível de adaptação e cujo custo seja o mínimo possível, destinados a atender às necessidades específicas de pessoas com deficiência, a promover sua disponibilidade e seu uso e a promover o desenho universal quando da elaboração de normas e diretrizes; 
> 
> g) Realizar ou promover a pesquisa e o desenvolvimento, bem como a disponibilidade e o emprego de novas tecnologias, inclusive as tecnologias da informação e comunicação, ajudas técnicas para locomoção, dispositivos e tecnologias assistivas, adequados a pessoas com deficiência, dando prioridade a tecnologias de custo acessível; (Decreto Legislativo 186/2008)

Ora, a áudio-descrição se encaixa no previsto uma vez que advém de pesquisa, inclusive acadêmica, registrada em dissertações e teses, bem como em artigos científicos, encontrados em universidades de renome e reconhecimento internacional.

Além disso,  a áudio-descrição permite o acesso a constructos educacionais, por exemplo, na áudio-descrição de uma teleaula, ou de slides apresentados, por exemplo na cadeira de neurofisiologia a alunos de psicologia.

Desconsiderar o custo benefício desse recurso e a viabilidade  de sua implantação é tripudiar sobre nossa Constituição, sobre a emenda que agora dela faz parte e principalmente sobre milhões de pessoas com deficiência visual, com dislexia, com deficiência física e outras.

Especial atenção devemos dar para o papel da áudio-descrição na garantia do direito de igualdade e oportunidade devido às crianças com deficiência. Não podemos dizer que as crianças com deficiência visual terão igualdade de oportunidades, menos ainda, igualdade de condições de decidirem pelo que lhes é de direito, se essas crianças forem impedidas do acesso às informações visuais como aquelas contidas nos materiais didáticos (nos livros que trazem figuras, gráficos, mapas, etc.), nos materiais paradidáticos e destinados ao lazer, os quais trazem fotos, figuras para pintar, entre outros.

Uma criança cega que recebe a áudio-descrição das imagens contidas em seu livro, melhor pode acessar as informações e conceitos dele advindos.

Uma criança com baixa visão que recebe a áudio-descrição de uma figura pode melhor “visualizar” aquilo que está vendo e  cujos detalhes não distingue.

A aquisição dos conceitos de novos vocabulários, bem como a oportunidade de discutir os eventos visuais com seus coleguinhas que enxergam,  podem ser facilitados, mediados ou viabilizados pela áudio-descrição.

Sem ela,  se desconsiderará mais um item  da Convenção, e por conseqüência, aviltar-se-á mais uma vez nossa Carta Maior, ao se descumprir o Decreto 186/08 em seu artigo 7.

> Artigo 7 
> Crianças com deficiência 
> 1. Os Estados Partes tomarão todas as medidas necessárias para assegurar às crianças com deficiência o pleno exercício de todos os direitos humanos e liberdades fundamentais, em igualdade de oportunidades com as demais crianças. 
> 2. Em todas as ações relativas às crianças com deficiência, o superior interesse da criança receberá consideração primordial. (Decreto Legislativo 186/2008) 

O reconhecimento de que não é a deficiência que incapacita a pessoa, mas as barreiras que a ela são impostas, bem como a busca pela independência moral, ética, física, profissional e de toda sorte, vem se somar na tentativa de tornar as pessoas com deficiência livres das peias sociais  que as colocam como dependentes  daqueles que não lhes devem mais do que o respeito.

É sabido, que o ser humano, enquanto ser social depende de sua espécie, no entanto, tal dependência não pode dar vez a uma relação de privação das liberdades mais fundamentais a que toda pessoa humana tem direito: a liberdade de ir e vir, a liberdade de acesso ao trabalho e lazer, a liberdade de acesso às informações e a liberdade de expressar sobre elas.

Como tais liberdades têm sido denegadas, a Convenção traz, com clareza solar, dispositivo que rejeita tal situação. E, ao fazê-lo, fundamenta mais uma vez o pleito por uma áudio-descrição que esteja disponível em todas as instâncias e a todas as pessoas que dela necessitem, para que o acesso à informação e tudo que dele decorre, possa ser desfrutado pelas pessoas cegas, tanto quanto as informações visuais são para as pessoas videntes. Sem meias palavras, a Convenção diz:

> Art 9 – Acessibilidade
> 1. A fim de possibilitar às pessoas com deficiência viver de forma independente e participar plenamente de todos os aspectos da vida, os Estados Partes tomarão as medidas apropriadas para assegurar às pessoas com deficiência o acesso, em igualdade de oportunidades com as demais pessoas, ao meio físico, ao transporte, à informação e comunicação, inclusive aos sistemas e tecnologias da informação e comunicação, bem como a outros serviços e instalações abertos ao público ou de uso público, tanto na zona urbana como na rural. Essas medidas, que incluirão a identificação e a eliminação de obstáculos e barreiras à acessibilidade, serão aplicadas, entre outros, a:
> b) Informações, comunicações e outros serviços, inclusive serviços eletrônicos e serviços de emergência;
> 
> Artigo 21
> Liberdade de expressão e de opinião e acesso à informação Os Estados Partes tomarão todas as medidas apropriadas para assegurar que as pessoas com deficiência possam exercer seu direito à liberdade de expressão e opinião, inclusive à liberdade de buscar, receber e compartilhar informações e idéias, em igualdade de oportunidades com as demais pessoas e por intermédio de todas as formas de comunicação de sua escolha, conforme o disposto no Artigo 2 da presente Convenção, entre as quais:
> a) Fornecer, prontamente e sem custo adicional, às pessoas com deficiência, todas as informações destinadas ao público em geral, em formatos acessíveis e tecnologias apropriadas aos diferentes tipos de deficiência;
> b) Aceitar e facilitar, em trâmites oficiais, o uso de línguas de sinais, braille, comunicação aumentativa e alternativa, e de todos os demais meios, modos e formatos acessíveis de comunicação, à escolha das pessoas com deficiência; (grifo nosso)
> d) Incentivar a mídia, inclusive os provedores de informação pela Internet, a tornar seus serviços acessíveis a pessoas com deficiência; (Decreto Legislativo 186/2008)

Como se vê, sobejamente, o Decreto, e a Convenção que ele aprova, hora em comento, sintetiza a defesa pela dignidade humana da pessoa com deficiência, pela igualdade de condições, pela igualdade de oportunidades, pela igualdade de acesso, pela quebra de barreiras atitudinais, e, fortemente, pela promoção da acessibilidade (física, cultural, comunicacional, entre outras).

A constante defesa desses direitos, ao longo da Convenção, de um lado denuncia o quanto eles vêm sendo negados, de outro, diz da premência de torná-los realidade, num momento histórico, em que milhões de pessoas por todo o mundo são excluídas, desrespeitadas, discriminadas, por razão de deficiência.

Certamente, a áudio-descrição não dará cabo de toda essa mazela social, no entanto, enquanto um serviço mediador  de acesso à cultura, enquanto um serviço assistivo  de baixo custo e enquanto uma ferramenta de acessibilidade comunicacional, contempla os princípios fundamentais desta Convenção  e vem contribuir para a independência das pessoas com deficiência, seu acesso à informação, à educação, ao trabalho e ao lazer.

Em suma, a áudio-descrição é um exemplo claro de que se pode fazer muito, investindo economicamente  pouco, para beneficiar a tantos. 

A A-d, neste diapasão, é  Acesso à Dignidade, é Acesso ao Direito,  é áudio-descrição.

## 4- Da necessidade da áudio-descrição

Pelo cenário até aqui exposto, do desconhecimento de que significa a áudio-descrição para as pessoas que dela se beneficiarão e do desconhecimento que se tem das leis que sustentam a reivindicação pela oferta da áudio-descrição, urge a necessidade de, de um lado divulgar/conscientizar os usuários do serviço da áudio-descrição a respeito da existência deste, de seu direito de o exigir, e de os informar, acima de tudo, a respeito dos benefícios que tal serviço trará às pessoas, mormente àquelas com deficiência visual; de outro lado, é mister que profissionais sejam treinados/capacitados na oferta do serviço de áudio-descrição, para trabalhar nos diversos segmentos, teatro, cinema, televisão, museu e no sistema educacional em geral.

Mais do que isso, contudo, estamos falando de que essa conscientização sobre a oferta da áudio-descrição resultará em propiciar a centenas de milhares de pessoas o acesso ao lazer, à cultura e à própria educação com qualidade, quesitos constitucionais, ainda hoje denegados às pessoas com deficiência, quando as imagens estão no foco da questão.

Assim, crianças cegas não recebem livros com desenhos e outras configurações gráficas bidimensionais; jovens não desfrutam da programação televisiva, adultos são privados de áreas da ciência a qual poderiam estar inseridos; idosos não aproveitam na totalidade os passeios que fazem, todos pela ausência das informações advindas do contato com as imagens ou com o contato precário com elas, quando são pessoas com baixa visão. E estas são muitas, quando se considera que cerca de  95% dos idosos, com mais de 75 anos, terão alguma perda visual. 

> Art. 208. O dever do Estado com a educação será efetivado mediante a garantia de:
> 
> (...) III - atendimento educacional especializado aos portadores de deficiência, preferencialmente na rede regular de ensino; 
> V - acesso aos níveis mais elevados do ensino, da pesquisa e da criação artística, segundo a capacidade de cada um; 
> (...) § 1º - O acesso ao ensino obrigatório e gratuito é direito público subjetivo.
> § 2º - O não-oferecimento do ensino obrigatório pelo Poder Público, ou sua oferta irregular, importa responsabilidade da autoridade competente. 
> (Constituição Federativa do Brasil, 1988).

Na educação, a áudio-descrição permitirá a pessoas cegas pleitear profissões nas áreas da arquitetura, da química, da geografia, das artes, da computação, dentre outras que hoje são vistas como inatingíveis para as pessoas cegas, uma vez que essas profissões fazem uso de grandes porções de conteúdo visual. Não obstante, várias pessoas cegas ou com baixa visão já estão nessas áreas, embora não tenham acesso às imagens por meio de áudio-descrição. E isso exige delas grandes esforços e desprendimento de energia que poderia estar sendo usada para outra atividade, caso a áudio-descrição lhes estivesse disponível.

Para as crianças pequenas, a áudio-descrição faculta o acesso a conceitos novos e à elaboração de conceitos ainda não bem formados; permite a aquisição de novos vocabulários e o acesso ao lazer, o qual será partilhado com as demais crianças que enxergam, levando à inclusão de todas (www.rnib.org.uk).

Exemplo disso é sua aplicação nos jogos (tipo vídeo-games). Crianças cegas poderão jogar e discutir sobre o que jogaram, a que fase chegaram etc., falando com seu coleguinha que enxerga. E ambas as crianças poderão, então, partilhar seu conhecimento sobre o jogo. Nos dias de hoje, essas situações são raras e as crianças cegas ficam com poucas chances de discutir/brincar com seus coleguinhas quando os jogos são o mediador ou assunto da conversa.

Os benefícios da narração descritiva, isto é, da áudio-descrição, pode ser aquilatado a partir de uma áudio-descrição feita durante uma visita ao museu. Nestes casos, a áudio-descrição feita por um áudio-descritor conhecedor da capacidade das pessoas com deficiência visual em apreciar as configurações bidimensionais, permitirá maior e adequada acessibilidade dessas pessoas aos museus e seus conteúdos. Isso se alcançará, por exemplo, pela utilização de recurso de áudio gravado, acessado a partir de um aparelho de CD, em cuja mídia está um roteiro com a descrição das obras apresentadas num dado trajeto.

Na apresentação de slides, tem-se orientado a inclusão de informação sonora, podendo o apresentador gravar previamente o texto ou legenda disponíveis no slide, bem como a descrição das imagens nele contidas. (www.rnib.org.uk).

A áudio-descrição pode, ainda, servir como eliminador de preconceitos e de discriminação como a que recentemente se viu ocorrer quando o TJ do Maranhão negou a participação de candidato cego em concurso para magistratura.

> O presidente do TJ-MA, Raimundo Cutrin, disse, por meio de assessoria de imprensa, que a atividade de juiz é incompatível com a falta de visão. Segundo ele, juízes têm de fazer inspeções, correções e interrogatórios, atividades que exigem “visão apurada”.
> 
> No último dia 7, o CNJ (Conselho Nacional de Justiça) determinou reserva de 5% a 20% das vagas para pessoas com deficiência em todos os concursos para magistratura do país. A decisão não faz ressalva a cegos. A determinação, contudo, dá autonomia aos tribunais para definir os editais, observando a “compatibilidade entre as funções a serem desempenhadas e a deficiência do candidato". http://www1.folha.uol.com.br/folha/cotidiano/ult95u459168. shtml

Para além do fato de que um juiz não deve prejulgar, nem mesmo baseado naquilo que vê, sabendo que a visão pode ser facilmente iludida e sabendo que, se de um lado  é a visão que captura as imagens, de outro, é o cérebro que as analisa e delas tira suas conclusões, apenas por preconceito se pode aduzir que uma pessoa cega não seria capaz de exercer a função de magistrado. Um juiz não deve julgar meramente pelo que lhe é apresentado às vistas, mas julgar pelos fatos que lhes chegam à mente, à razão. Com efeito, alhures, muitos são os exemplos de juízes cegos atuando nas mais diversas áreas: Sir John Fielding (1721-1780), Bill Kempton; Diane Cram; John Lafferty, Thomas “TJ” Loftus; Joseph Donahey; Richard Conway Casey; Richard B. Teitelman,  e uma dezena de outros magistrados.

A “lacuna” de acesso às informações visuais, largamente usada como argumento para discriminar pessoas cegas no ofício do magistrado no Brasil, pode ser tranquilamente suplantada por um áudio-descritor que atue como perito em tradução visual, em auxílio ao juiz. E, como é sabido,  ter o apoio de peritos não é incomum na atuação dos magistrados que, por exemplo,  se valem de perito que interprete fotos de um crime, identificando, por exemplo, se a arma e o corpo estão em posições condizentes com esta ou aquela tese da defesa ou da acusação.

Logo, o áudio-descritor judiciário pode colaborar com o magistrado com deficiência visual ou com dislexia, mas também com eventuais cidadãos com deficiência visual, e.g. pessoas idosas com baixa visão que, contra si, estão sendo usadas imagens numa dada ação, da qual precisem de maiores detalhes da imagem para melhor defender-se. A igualdade de condições no ato de defender-se deve ser perseguida com a maior retidão, sob pena de, não tendo esse direito respeitado, o cidadão com deficiência visual ser discriminado por razão de deficiência (Lei 3.956/2001). 

Com efeito, o excelso desembargador Antonio Fernando Bayma Araújo  reconheceu que se obrara preconceituosamente contra a pessoa com deficiência, donde sentenciou que se suspendesse o concurso que negara a participação de pessoas com deficiência visual.

> Atendendo pleito da seccional maranhense da Ordem dos Advogados do Brasil (OAB/MA), em Mandado de Segurança impetrado na última segunda-feira, o desembargador Antonio Fernando Bayma Araújo concedeu liminar suspendendo o concurso para preenchimento de vagas para o cargo de juiz do Tribunal de Justiça do Maranhão até que seja feita a readaptação do edital do certame, garantindo a participação de portadores de deficiência visual total ou parcial, bem como a realização das provas em “braile”, “ampliada”, “leitura de prova”, utilização de “ledor” ou outros mecanismos de auxílio aos deficientes. 
> 
> O desembargador afirmou que tentar proibir a participação de deficientes visuais no concurso é uma atitude preconceituosa, discriminatória e segregacionista, na medida em que, sem qualquer amparo legal, gera prejuízos não só na órbita material do direito, mas, sobretudo, de ordem moral e pessoal. 
> 
> “A manutenção da regra, sem uma resposta imediata do Poder Judiciário, causa transtorno não só a quem pretende se inscrever no concurso na qualidade de deficiente, mas a toda a sociedade”, assinalou o desembargador Bayma Araújo.
> 
> O desembargador considera que o TJ não pode incorrer na violação a direito líquido e certo consistente no impedimento da participação dos portadores de deficiência visual no concurso para provimento do cargo de juiz de direito substituto de entrância inicial.
> 
> “Direito não se pede, exige-se. A igualdade é para todos, e não para uma parte”, sentenciou o magistrado.
> 
> “A decisão do desembargador Bayma Araújo foi justa e coerente, tendo em vista que o edital do mencionado concurso traz em seu bojo previsão claramente inconstitucional, evidenciando um preconceito injustificáve e promovendo uma acintosa violação aos direitos fundamentais da pessoa humana”, ressaltou o presidente da OAB/MA, José Caldas Gois, ao ser informado da decisão.
> 
> Para a OAB, na parte que proíbe a participação de cegos no concurso, o edital é absolutamente contrário à lei e chega-se mesmo ao ponto em que a regra estabelecida no mesmo implicaria na obrigação de que membros do Poder Judiciário, com base em disposição ilegal, viesse a praticar ato definido em lei como crime, ao indeferir, de plano, por exemplo, o pedido de inscrição de pessoa cega ou portadora de deficiência visual. 
> 
> “Se os empregadores privados não podem discriminar os trabalhadores, para critérios de admissão em razão de serem portadores de deficiência, com muito maior razão não pode o Estado, a Administração Pública e, principalmente, o Poder Judiciário, a quem incumbe, a defesa da Ordem Jurídico Constitucional.
> 
> A norma contida na Constituição visa promover, e não impedir, o ingresso de todos, inclusive dos deficientes, em igualdade de condições, no serviço público”, salientou a OAB no Mandado de Segurança. ( http://imirante.globo.com/jus-ma/plantao/plantao.asp?codigo1=811)


## 5 - Considerações Finais

Como pudemos observar, são várias as áreas em que a áudio-descrição respeita o direito de acesso às mais diversas formas de comunicação/informação a que as pessoas com deficiência têm direito.

Não é desconhecido, por exemplo, a linda, poética e significativa áudio-descrição feita pelo cosmonauta Iuri Alieksieievitch Gagarin, em que, ao ver a terra de onde ninguém jamais vira antes, descreveu nosso planeta como: “A Terra é azul!”, trazendo aos que a terra não podiam ver a informação de que cor ela era. E quantos mundos azuis deixam de ser acessíveis às pessoas com deficiência visual pela ausência da áudio-descrição, em particular, e pelas demais barreiras comunicacionais em geral?

Enumerá-las é quase impossível, tantas são as formas que tomam, mas uma coisa é possível fazer e é disso que estamos tratando aqui: romper com barreiras, para a participação de todos numa sociedade para todos e que não pára todos os que têm alguma deficiência.

Portanto, que atuemos em busca e defesa do recurso da áudio-descrição para que seja mais um serviço de tecnologia assistiva disponível às pessoas com deficiência. E mais:  que esse serviço seja prestado com a qualidade e freqüência que merecem seus usuários: a melhor! 

## BIBLIOGRAFIA: 


AUDETEL,  The European AUDETEL Newsletter: bringing television to life for visually impaired audiences. Audetel Newsletter 1, November 1992, pp. 1-5.

AUDETEL CONSORTIUM. Audetel developments 1992-1995. VHS videocassette (disponible en la ITC), 1995.

BARDISA, Lola. Cómo enseñar a los niños ciegos a dibujar. Madrid: ONCE, 1992.

BRASIL,  Lei Nº. 10.098/2000. Disponível em  http://agenda.saci.org.br

BRASIL, Decreto Nº. 5.296/2004. Disponível em http://agenda.saci.org.br

BRASIL, Portaria 310/2006. Disponível em http://www.mc.gov.br/o-ministerio/legislacao/portarias 

BRASIL, Portaria 466/2008. Disponível em http://www.mc.gov.br/o-ministerio/legislacao/portarias 

BRASIL,  Constituição da República Federativa do Brasil (1988). Disponível em http://www.planalto.gov.br/ccivil_03/constituicao 

BRASIL, Decreto Legislativo 186, de julho de 2008. Disponível em http://www2.senado.gov.br/bdsf/item/id/99423

LIMA, F. J. Ensinando reconhecer desenhos pelo tato: o efeito do treino no desempenho de pessoas cegas na nomeação de figuras examinadas hapticamente. Submetido à Pontifícia Universidade Católica do Paraná (PUCPR), 2004. 

ROYAL NATIONAL INSTITUTE FOR THE BLIND.  Blind and partially sighted adults in Britain: the RNIB survey. Londres: HMSO Publications, 1991.

WEISEN, Marcus. The AUDETEL Project: deliverable 2. Review of current expertise on audiodescription. Londres: Royal National Institute for the Blind, 1992.
