---
layout: post
title: Aplicativo ‘guiaderodas’ faz mapa colaborativo da acessibilidade
except: 
urlsource: http://brasil.estadao.com.br/blogs/vencer-limites/guiaderodas/
author: Luiz Alexandre Souza VENTURA do Blogs Vencer Limites | Estadão
date: 2016-02-19
categories: news
tags:
- Acessibilidade
- Tecnologia

---
Disponível para iOS e Android, o app considera a existência de vagas especiais para estacionamento ou manobrista, condições de circulação interna, banheiro para pessoas com deficiência e mais detalhes em restaurantes, supermercados, lojas, cinemas, farmácias, consultórios, teatros, baladas e outros locais.

![Aplicativo com versções iOS e Android][app]
<span>Aplicativo tem versões para [iOS][ios] e [Android][android]. Imagem: Divulgação</span>

Bruno Mahfuz usa cadeira de rodas há 15 anos e já vivenciou todo tipo de situação na qual a falta de acessibilidade trouxe dificuldades reais. A partir dessas experiências, nem sempre agradáveis, Bruno criou o aplicativo ‘guiaderodas’.

“Percebi que pessoas em diferentes momentos da vida enfrentam problemas similares, seja por limitação provisória ou permanente de exercer suas funções rotineiras. Além disso, algumas facilidades beneficiam muito idosos de uma maneira geral, que mesmo sem nenhuma limitação funcional, tendem a preferir um elevador a uma escada”, explica o empreendedor.

Disponível nas versões para [iOS][ios] e [Android][android], o app faz um mapa colaborativo dos recursos acessíveis de qualquer cidade, com registros em restaurantes, supermercados, lojas, cinemas, farmácias, consultórios, teatros, baladas e outros locais.

![Bruno Mahfuz sentando na cadeira de rodas, vestindo camisa azul, calça cinza e sapato marrom, com um largo sorriso no rosto][bruno]

“A meta é facilitar a vida de todas as pessoas com algum tipo de dificuldade de locomoção, sejam cadeirantes, idosos, gestantes, mães com filhos pequenos ou pessoas com outras limitações físicas e funcionais”, diz Bruno.

Com o ‘guiderodas’, o usuário faz uma avaliação, que dura no máximo 30 segundos, e consegue classificar o estabelecimento como acessível, parcialmente acessível e não acessível. O aplicativo considera a existência de vagas especiais para estacionamento ou manobrista, entrada, condições de circulação interna, banheiro para pessoas com deficiência e outros detalhes.

“São perguntas claras e objetivas que podem ser respondidas por pessoas com dificuldade de locomoção ou não. Com o GPS, o ‘guiaderodas’ favorece a avaliação in loco do estabelecimento. Outros filtros de busca podem ser feitos, seja por algum local específico, categoria ou lugares já marcados como acessíveis”, destaca o criador do app.

<iframe width="550" height="281" src="https://www.youtube.com/embed/544L577Ie1o" frameborder="0" allowfullscreen></iframe>

[app]: http://brasil.estadao.com.br/blogs/vencer-limites/wp-content/uploads/sites/189/2016/02/GUIADERODAS-01-550.jpg
[ios]: https://itunes.apple.com/br/app/guiaderodas/id1070335503?l=en&mt=8
[android]: https://play.google.com/store/apps/details?id=com.parallel30.guiaderodas
[bruno]: http://brasil.estadao.com.br/blogs/vencer-limites/wp-content/uploads/sites/189/2016/02/GUIADERODAS-02-550.jpg