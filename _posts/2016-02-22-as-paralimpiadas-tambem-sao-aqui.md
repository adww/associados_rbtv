---
layout: post
title: As Paralimpíadas também são aqui
except: Você já experimentou vivenciar como é ser um atleta paralímpico?
urlsource: http://educacao.estadao.com.br/blogs/blog-dos-colegios-santi/as-paralimpiadas-tambem-sao-aqui/
author: Blogs Escola Santi | Estadão
date: 2016-02-22
categories: news
tags:
- Paraolimpiadas
- Jogos Olimpicos
- Acessibilidade
- Bairro do Paraíso
- Educação
- Educação Física
- Escola
- Esportes Adaptados
- Paralimpíadas
- Rio 2016

---

Jogar futebol já é difícil, é preciso ter habilidade, boa pontaria, destreza, conduzir a bola, acertar o gol, defender um chute, driblar o adversário, cabecear. Para jogar vôlei então, nem se fala: dar manchete, toque (cuidado para não serem dois), passar para o companheiro, levantar, atacar…  Agora imagine ter que fazer tudo isso sem enxergar, ou estando em uma cadeira de rodas.

O que para alguns parece impossível, para outros se chama simplesmente rotina, pois é isso que os atletas paralímpicos, e todos os demais atletas com necessidades especiais, fazem todos os dias, enquanto treinam e participam de campeonatos nessas e em todas as outras modalidades que compõe o rol de esportes das Paralimpíadas. E foi isso o que os alunos do Fundamental 2 da Escola Santi – localizada no bairro do Paraíso, em São Paulo – tiveram que fazer durante a Semana Cultural e Esportiva.

![Voltei sentado][voleisentado]
<span>Volei sentado</span>

“Nós quisemos expandir os trabalhos com os sentidos e acessibilidade que já vinhamos realizando com os alunos e com a comunidade e adentrar cada vez mais nesse assunto, tão importante para o convívio igualitário, justo e pacífico na sociedade. Com a realização dos jogos paralímpicos, colocamos todos em contato ainda maior com a temática, para produzirmos mais resultados e refletir ainda mais sobre o assunto”, conta o professor de ciências, Stefan Bovolon, que realiza projetos sobre acessibilidade com os alunos.

Primeiro, todos realizaram pesquisas online sobre os jogos paralímpicos, como surgiram, quem são os atletas, quais as modalidades, como serão as Paralimpíadas de 2016 no Rio. Depois, chegou a hora de experimentar em primeira mão algumas das modalidades que os atletas irão disputar: Futebol de 5 (com redução de visão), ping-pong em cadeira de rodas, vôlei sentado, pebolim com braço imobilizado, arremesso sentado, salto em distância com braços imobilizados e, ao final, uma corrida com olhos vendados, guiados por um acompanhante.

![Futebol de 5 (com olhos vendados][futebolvendado]
<span>Futebol de 5 (com olhos vendados)</span>

“_É uma experiência nova porque a gente vê na televisão mas a gente não tem ideia de como é realmente ser um atleta paralímpico, então é interessante, porque a gente consegue descobrir um pouco do universo deles_”, diz a aluna Isabela Tiossi.

As amigas Beatriz Durante e Luiza Pichirilli revelam que as atividades que mais gostaram foram a corrida vendada e o ping-pong sentado. “No ping-pong, é difícil conseguir chegar até a metade da mesa para fazer o ponto, então foi muito legal descobrir como as pessoas que têm essa dificuldade vão desenvolvendo habilidades diferentes para jogar”, dizem.
![Pebolim com braço imobilizado][pebolimimbilizado]

[pebolimimbilizado]: http://educacao.estadao.com.br/blogs/blog-dos-colegios-santi/wp-content/uploads/sites/231/2016/02/12310091_1008132859228698_6231554525062694274_o.jpg "Pebolim com braço imobilizado"
[voleisentado]: http://educacao.estadao.com.br/blogs/blog-dos-colegios-santi/wp-content/uploads/sites/231/2016/02/12339341_1008132952562022_7112052731032651257_o.jpg "Voltei sentado"
[futebolvendado]: http://educacao.estadao.com.br/blogs/blog-dos-colegios-santi/wp-content/uploads/sites/231/2016/02/12309679_1008132709228713_7641188140437939235_o.jpg "Futebol de 5 (com olhos vendados"