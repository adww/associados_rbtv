---
layout: article
title: A Escrita Emancipadora
except: 
urlsource: 
author: 
extrainfo: 
date: 2010-06-24
categories: article
tags:
- Acessibilidade
- Tecnologia

---

"...Valentin Haüy era exatamente a pessoa certa no tempo certo para ter esta inspiração. Nascido em 1745 na pequena vila de Saint-Just-en-Chausèe.

Valentin, com 6 anos, de idade mudou-se para Paris com sua família, que eram tecelões. Ele e seu talentoso irmão, Renè-Just, que se tornou um famoso cientista e fundou o campo da cristalografia, prosperou no meio de tremendas oportunidades educacionais na cidade.

Valentin tornou-se um habilidoso lingüista que falava dez línguas vivas além dos antigos Grego e Hebreu. Porquanto não era rico (ele ganhava sua vida traduzindo e autenticando documentos), ele era bem integrado, em parte, devido a eminência de seu irmão na nova Academia Real de Ciências

Uma vez que Haüy interessou-se pela educação para cego, tornou-se uma autoridade no assunto, visitando pessoas cegas de famílias ricas para saber que métodos eles usavam para lidar com várias dificuldades. Suas próprias energia e habilidade naturais para relações públicas estavam extraordinariamente provadas, isso seria sua sorte.

Na Primavera de 1784, num outro passeio em Paris, ele encontrou o estudante perfeito.

Quando Haüy partiu da igreja de Saint Germain des Pres depois dos serviços, ele pôs uma moeda na mão de um menino cego que mendigava perto da entrada da igreja. Quando o menino instantaneamente pronunciou a denominação corretamente, Haüy teve um discernimento surpreendente: O cego podia aprender muito, talvez até ler, usando o sentido do tato.

O mendigo, 12 anos de idade François Lesueur, tornou-se o primeiro aluno de Haüy.

François era cego desde a infância e tinha passado a maior parte da sua curta vida mendigando nas ruas de Paris para ajudar seus irmãos.

Haüy compensava François pelas perdas do ganho com a esmola, enquanto ele o ensinava a ler usando letras de madeira que ele movia ao redor para formar palavras.

François foi um estudo muito rápido; dentro de seis meses ele aprendeu a decifrar até as fracas impressões do outro lado das páginas escritas.

Haüy o trouxe para a Academia Real, onde suas habilidades surpreenderam os mais altos estudiosos e cientistas da França.

A casa em Rue Saint-Victor

Haüy fez a maior parte desse triunfo, solicitando ajuda às celebridades atuais, tal como Maria Theresia von Paradis, uma jovem cega com uma reputação internacional como um pianista prodígio.

Vivendo a sua própria vida da Lingüística, Haüy era bem informado para saber do interesse de Louis XVI em velhos manuscritos e códigos secretos e solicitou a ajuda financeira do rei, com sucesso.

A princípio, ele administrou a escola em sua casa, mas como o projeto cresceu, conseguiu atrair ajuda real suficiente para alugar um prédio.

Com vinte e quatro alunos, Haüy abriu a primeira escola do mundo para o cego, a Instituição Real para crianças cegas, na 66 Rue Saint-Victor.

O primeiro edifício da escola já tinha na época cerca de 500 anos de idade e tinha suportado o mau uso como entre outras coisas, um orfanato fundado por São Vicente de Paula, o santo padroeiro das sociedades caridosas, e uma casa de má reputação.

O interior era úmido, apertado, e de pobres remendos, com estreita escadaria, salas pequenas e paredes pegajosas.

Apesar das horrendas condições, a escola, que só aceitava estudantes nobres de nascimento ou de grande inteligência, foi um sucesso imediato. Durante dois anos, a Academia de Música promoveu concertos beneficentes para a escola enquanto Haüy mantinha os fundos reais fluindo, levando as crianças para Versailles para entreter o rei no Natal com demonstrações de leituras, aritmética e usando mapas táteis.

Uma vez que a escola tinha quase estabelecido uma imprensa ministrada pelos estudantes para produzir livros em alto-relevo, Haüy fez uma série de capas especiais "amostras".

Para os nobres da Corte, o texto era o reconhecimento do próprio livro de Haüy um ENSAIO EM EDUCAÇÃO DE CEGO...

Uma dessas apresentações na Corte era freqüentada por Marquis d'orvilliers, um nobre de uma pequena vila a leste de Paris - Coupvray.

O "bebê" Braille do interior

Alguns anos mais tarde em Coupvray, nascia Louis Braille, a quarta criança de um fabricante de selas.

Em 1812, com 3 anos de idade, Louis feriu seu olho num acidente enquanto brincava com as ferramentas do seu pai.

Uma lenda local diz que a distração que fez o pai de Louis deixar a sua banca de trabalho sozinha, com suas atrações perigosas para uma criança curiosa que está começando a andar, foi a notícia do exército de Napoleão rumo ao que se tornaria uma eventual catástrofe na Rússia. Apesar (ou talvez por causa) da ajuda religiosa da curadora local, uma velha que primeiro tratou o olho danificado de Louis com água de lírio, seguido por um daqueles doutores de olho da cidade vizinha, a infecção se estabeleceu. Seguiram-se outros tratamentos ineficazes, inclusive uma dose de colamelanos, um laxativo.

No ano seguinte a infecção passou para o outro olho e Louis Braille perdeu toda sua visão.

Para aumentar os problemas da família Braille, a guerra constante de Napoleão com o resto da Europa levou a sua cidade a ser invadida por exércitos – não somente os militares Franceses, mas também seus inimigos, os Prussianos e os Russos.

Por dois anos, de 1814 a 1816, sessenta e quatro soldados diferentes ficaram na modesta casa de três quartos dos Braille.

Suas exigências intermináveis por comida, animais e alojamento causaram severa dificuldade na cidade.

Por volta de 1816 as privações da guerra desgastaram a saúde dos cidadãos e uma epidemia de varíola aconteceu inesperadamente. As pessoas, inclusive o pai de Louis Braille, não confiavam na vacina promovida pelo governo e muitos na cidade adoeceram.

Felizmente, mais ou menos ao mesmo tempo, outras novas pessoas também vieram a Coupvray - um padre, Abbé Jaques Palluy, e um professor, Antoine Bécheret.

Eles conheceram bem Louis e surgiram com a então revolucionária idéia de deixá-lo freqüentar a escola regular. Ambos os pais de Louis sabiam ler e escrever, e seus irmãos mais velhos tinham todos freqüentado a mesma escola quando crianças.

Louis foi muito tempo cativado pelas histórias de sua irmã Catherine lembradas dos seus dias de escola.

Louis ia tão bem na escola que quando o governo decretou os novos métodos da escola local que o teria impedido de continuar sua educação, Bécheret e Palluy abordaram o nobre do local para ajudar a garantir a admissão de Louis a escola para o cego de Valentin Haüy em Paris.

O nobre era Marquis d'orvilliers,

Um sobrevivente da recente epidemia de varíola, que tendo visto os estudantes de Valentin Haüy se apresentarem em Versailles, concordou em escrever ao atual diretor da escola, Sebastian Guillié, e garantir a admissão de Louis com uma bolsa de estudos. Em Fevereiro de 1819, com 10 anos de idade, Louis e seu pai fizeram uma viagem de quatro horas na diligência para Paris.

Louis tornou-se o estudante mais jovem da escola para o cego.

A escola ensinava muitas coisas práticas - tecelagem, crochê, riação, fabricação de calçado, cesto e corda - bem como assuntos acadêmicos.

Já que os estudantes não tinham oportunidade de aprendizagem antes, eles eram essencialmente empregados sem remuneração. Trabalho árduo, supervisionados de perto. Eles usavam uniformes e viviam como espartanos, suas vidas eram reguladas, com um banho por mês, temperatura inadequada e comida ruim, geralmente feijão e papa de aveia. A água potável da escola não era filtrada e vinha direto do rio Sena. Um jantar com pão seco (servido num quarto solitário) era o castigo padrão.

Apesar das dificuldades, Louis se adaptou rápido à escola e fez o primeiro dos muitos amigos lá que ele manteria por toda sua vida, o colega estudante Gabriel Gauthier, um ano mais velho do que ele. Ele precisava de aliados porque os estudantes mais velhos sempre o incomodavam por causa do seu sotaque do interior e o chamavam "bebê Braille" devido à sua idade.

OS PRIMEIROS LIVROS PARA LEITORES CEGOS

O diretor Guillié, que administrava a escola no tempo da admissão de Louis, era um oftalmologista por vocação que fundou a primeira clínica de olho em Paris e sobreviveu a diversas mudanças do governo durante a Revolução Francesa e a atual era Napoleônica.

Nos vinte anos que compreenderam a Revolução Francesa e as Guerras Napoleônicas, morreram quase um milhão de Franceses, a metade deles abaixo de vinte e oito anos de idade.

Durante os piores tempos da Revolução, o próprio edifício da escola foi usado para encarcerar os padres que não cooperassem (inclusive o próprio irmão de Valentin Haüy) que se recusou a jurar lealdade ao novo governo, e 170 deles foram assassinados lá em 1792.

Os nobres que uma vez tinham ajudado a escola estavam mortos, presos ou saindo da França.

A escola foi absorvida pela clínica de olho de Guillié e por um tempo também foi combinada com a escola para o surdo.

Finalmente os estudantes cegos foram forçados a ficar no Quinze-Vingts, agora superlotado, caótico, e principalmente a casa como último recurso para os cegos mendigos anciãos.

O interesse de Guillié em restabelecer a escola para o cego não foi inteiramente humanitário porque quando ele obteve o edifício de volta e o reabriu. Ele requereu apenas os estudantes mais promissores do Quinze-Vingts.

Com poucos professores, Guillié confiou aos estudantes mais velhos a atuarem como tutores ou "repetidores" das lições verbalmente aos estudantes mais jovens.

Embora os "repetidores" não soubessem Guillié teve algum sucesso em restabelecer o apoio do governo para a escola e recebia um pequeno ordenado para os estudantes mais velhos, que ele mesmo embolsava.

Ele instituiu rígidos horários e disciplinas para fazer os estudantes mais produtivos, mesmo quando ocasionalmente a chuva penetrava através das pingueiras no teto do edifício, dentro das oficinas e salas de aulas.

As mercadorias que os estudantes produziam eram vendidas em toda parte de Paris e produzia a parte principal do rendimento. Por exemplo, entre outras habilidades, os estudantes teciam o tecido para os seus próprios uniformes, que dependendo da conta, eram azuis ou pretos.

Guillié conseguiu um contrato para a escola tecer lençóis para o enorme sistema de hospitais públicos de Paris. Isso foi mais um incidente de interesse subsidiário, pois o maior desses hospitais, La Salpêtrière, tinha capacidade para mais de 10.000 internos.

Os poucos patrocinadores ricos que permaneceram eram sempre levados em passeios turísticos à escola e à oficina; a leitura dos estudantes dos poucos livros impressos que havia era o ponto excitante do passeio.

O método original de imprimir livros de Haüy continuou imutável por três décadas. Usando papel encharcado para elevar a forma das letras, a forma tátil das letras permaneciam quando o papel secava. Então as folhas eram coladas de trás para frente, para formar uma folha frente e verso. Naturalmente, esses livros eram feitos vagarosamente e extremamente difíceis de serem lidos, uma vez que as letras grandes, ornadas, tinham que ser traçadas individualmente.

No tempo da admissão de Louis Braille, a escola, agora com cerca de trinta anos, tinha cem alunos e um total de quatorze livros impressos.

Em 1821, Dr. Guillié foi demitido depois de ter sido pego "numa relação íntima" com uma professora quando ela ficou grávida.

O novo diretor da escola, André Pignier, imediatamente resolveu melhorar as condições, primeiro instituindo dois passeios por semana, de modo que os estudantes pudessem respirar o ar fresco e fazer alguns exercícios fora das suas carteiras e da banca de trabalho.

Os estudantes começaram a passear pela cidade, todos agarrados numa longa corda que servia como guia, para freqüentar a missa no Domingo na igreja de São Nicolau du Chardonnet e ir na Quinta-Feira à tarde excursionar num parque botânico local.

Uma outra reforma de Pignier foi fazer uma celebração pública da história da escola em que o convidado de honra seria o fundador, Valentin Haüy. Haüy, agora um velho, não havia estado na escola há anos. Perdendo o controle da escola pelas conseqüências da revolução, ele foi forçado a fugir da França.

Antes da sua partida, ele resgatou um dos seus estudantes mais promissores, Remi Fournier, do caos do Quinze-Vingts. Juntos eles passaram cerca de uma década em exílio virtual trabalhando com estudantes cegos em outros países da Europa, inclusive a Rússia.

As escolas para o cego foram uma idéia que surgiu definitivamente com o tempo, com Liverpool (1791), Vienna (1804), Berlin e São Petersburg (1806), Amsterdam (1808), Dresden (1809), Zurique (1810), e Copenhague (1811), aparecendo em rápida sucessão usando muito as idéias e métodos de Haüy.

No seu retorno para a França, Haüy, exausto, destituído, e ele próprio quase cego, encontrou-se ainda banido da escola pelo antipático Dr. Guillié. 

No dia da cerimônia para homenagear Haüy, Louis Braille, agora com 12 anos, em companhia com outros estudantes deram um programa musical de canções dos dias antigos da escola e uma demonstração de leitura usando os livros originais impressos por Haüy, agora com 76 anos de idade.

De dia os dois se encontraram frente a frente, um ano antes da morte de Haüy. Louis Braille se lembraria da ocasião pelo resto de sua vida. No ano seguinte, ele foi um de um pequeno grupo da escola a ir ao pobre funeral de Haüy. (Nota: As letras impressa em alto relevo de Valentin Haüy eram largamente espaçadas e usadas com fontes ornamentadas)

MUITO DIFÍCIL PARA A ARTILHARIA?

Um outro visitante, pouco tempo depois, teria igualmente grande influência no futuro de Louis Braille.

Charles Barbier de la Serre foi outro esperto sobrevivente da agitação política que imergia a França. Barbier nasceu em Valenciennes, em 1767, seu pai era o controlador das fazendas do rei. Assim, Charles conseguiu admissão na Academia Real Militar em 1782, provavelmente em Brienne, que se verdade, ele teria sido um colega de escola de Napoleão Bonaparte.

Barbier fugiu da Revolução passando algum tempo nos Estados Unidos como superintendente de terras no território indiano e retornou à França em 1808, onde se juntou ao exército de Napoleão e publicou uma tabela para escrita rápida ou "expediografia", seguida um ano mais tarde de um livro descrevendo como escrever várias cópias de mensagens rapidamente.

O interesse de Barbier em escrita rápida e secreta foi fundamentado em experiências desagradáveis.

O exército Francês, sob o domínio de Napoleão, foi derrotado pela última vez em Waterloo, em 1815, mas antes disso, eles quase conquistaram a Europa e eram considerados até pelos seus inimigos os melhores artilheiros do mundo.

Nas suas próprias experiências de guerra, Barbier teria visto todas as tropas armadas em um posto a diante aniquiladas quando eles traíram suas posições, acendendo uma simples lanterna para ler uma mensagem. Um sistema tátil para enviar e receber mensagens seria útil não apenas à noite, mas em manter comunicações durante os combates com seus aterrorizantes grupos de artilharia. Fumaça densa, ofuscante e barulhos atroadores combinavam-se, criando uma confusão infernal.

Se a batalha deveria ir mal pelos cavalos que transportavam as armas enormes, a tripulação sobrevivente de artilharia se encontraria imobilizada num emaranhado de armas, arreios e animais mortos ou morrendo sem poder escapar das balas.

Barbier e os estudantes da Instituição para as Crianças Cegas provavelmente tiveram o primeiro encontro quando ambos estavam exibindo seus métodos de comunicação no Museu da Ciência e Indústria, atualmente localizado no Louvre. Barbier tinha um dispositivo que possibilitava ao escritor escrever mensagens no escuro.

Os estudantes estavam lendo com a angustiante vagarosidade de sempre os livros com letras impressas em alto relevo de Haüy.

Barbier decidiu levar seu próprio ponto-e-traço baseado em código de artilharia, chamado sonografia, para o Instituto Real Para as Crianças Cegas e entrou em contato com o Dr. Guillié, então ainda o diretor. Guillié, que seria demitido oito dias depois. Estava sem entusiasmo pela sonografia e suas possibilidades de uso pelos cegos. Despachou Barbier com pouco encorajamento.

Felizmente, Barbier era persistente. Ele retornou à escola após o escândalo sexual e interessou o Dr. Pignier, o novo diretor, sobre o seu sistema.

O Dr. Pignier organizou uma demonstração e passou algumas páginas impressas de pontos em alto relevo aos estudantes.

Louis Braille ficou estupefato quando tocou pela primeira vez nos pontos das amostras da sonografia. Ele sempre brincava com escrita tátil em casa nas férias de verão em Coupvray.

Os vizinhos recordaram mais tarde que quando era criança Louis tinha tentado o couro em várias formas e até organizado alfinetes de estofados em padrões, esperando descobrir um método de comunicação tátil praticável, todavia, sem sucesso. Quando tocou nos pontos, ele sabia que tinha encontrado o seu meio e rapidamente aprendeu a usar a régua de Barbier que se assemelha muito com a reglete de hoje. Ele, seu amigo Gabriel, e outros garotos na escola ensinaram o código uns aos outros escrevendo mensagens uns aos outros para cá e para lá.

Somente uma semana depois, Dr. Pignier escreveu a Barbier informando que a sonografia seria usado na escola como método de escrita suplementar.

Louis também foi rápido em perceber os problemas com o sistema de Barbier, que nunca foi realmente usado pelo exército por causa da sua complexidade. A sonografia usava uma cela de 12 pontos, que a ponta do dedo não podia cobrir e era trabalhoso escrever com um punção.

(Nota: Originalmente, o b tinha 4 pontos; o z tinha 9. A cela inteira tinha 12 pontos, era duas vezes mais alta que a cela Braille de hoje.)

Não havia sinais de pontuação, números ou sinais musicais e havia muita abreviação porque as celas representavam sons ao invés de letras.

Quando Louis se encontrou com o Capitão Barbier para falar sobre suas idéias para melhorar o código, o Capitão, agora, na sua meia idade, ciqüenta anos, à princípio ficou incrédulo e incomodado por ter as suas idéias questionadas por alguém tão jovem, sem experiência e cego. Em vez de discutir com o Capitão imperioso, Louis parou de pedir seus conselhos totalmente e em vez disso foi trabalhar experimentando o método sozinho.

Sobrava-lhe pouco tempo; naquele semestre ele ganhou prêmio em geografia, história, matemática, e piano, enquanto também trabalhava como encarregado da loja de chinelos da escola. Ainda tarde à noite, em casa em Coupvray durante o verão, Louis tentou várias modificações que permitiria os únicos símbolos das letras ajustarem-se debaixo da ponta do dedo.

Em Outubro de 1824, Louis agora com 15 anos de idade, revelou o seu novo alfabeto logo depois do início da escola. Ele descobriu sessenta e três maneiras de usar uma cela de seis pontos (embora alguns traços ainda fossem incluídos). Seu novo alfabeto foi recebido com entusiasmo pelos outros estudantes e pelo Dr. Pignier, que ordenou as regletes especiais que Louis projetou daquela original do Capitão barbier.

Gabriel Gauthier, ainda o melhor amigo de Louis, provavelmente foi a primeiríssima pessoa a ler o Braille.

A utilidade óbvia e a popularidade da invenção de Louis não fez a sua própria vida mais fácil. Os maus tempos na França em 1825 causaram o racionamento de alimentação na escola e a já escassa dieta foi reduzida a pão e sopa.

Os professores - todos videntes - ficaram indignados com o novo código, com suas exigências implícitas para que aprenderem algo tão estrangeiro. Preocupados com seus próprios empregos, eles reclamavam dizendo que o som da perfuração perturbava as aulas.

Finalmente, a escola conseguiu alguma estabilidade financeira com uma verba do governo vinda do Ministério do Interior, mas em 1826, o tesoureiro da escola fugiu depois de desviar uma quantidade igual a do orçamento anual.

O Dr. Pignier apelou para o governo repetidas vezes por muitos anos para o reconhecimento oficial do novo alfabeto bem como o conserto ou substituição do edifício deteriorado.

Seus pedidos foram negados, mas o diretor continuou seu apoio aos meninos no uso do novo código, movido por suas proficiências e entusiasmos. Ele prometeu a Louis que continuaria solicitando ao governo e enquanto isso ajudara para que Louis se tornasse o primeiro cego estudante de órgão da igreja de Sant'Ana.

A escola para o cego produziu muitos organistas; pelo tempo de Louis, cerca de cinqüenta graduados tocavam nas igrejas ao redor de Paris. Louis provou ser um músico excepcionalmente talentoso, foi ouvido (e elogiado) por Felix Mendelssohn, e poucos anos depois obteve o primeiro dos diversos empregos como organista de igreja.

OS PRIMEIROS LIVROS EM BRAILLE

O Dr. Pignier criou ainda uma outra oportunidade para Louis. Com 17 anos, ele designou Louis para ser o primeiro cego professor aprendiz da escola. Os outros professores ficaram zangados mas Pignier insistiu afirmando que "a consciência, a escolaridade e a paciência" de Louis o encaixava perfeitamente no trabalho.

Ele ensinou álgebra, gramática, música, e geografia.

Apesar do seu horário ocupado, ele continuou aperfeiçoando o código. Em 1828, ele descobriu um modo de copiar música no seu novo código (e eliminou os traços).

Em 1829, aos 20 anos de idade, ele publicou o método de escrever palavras, música e canções fáceis por meio de pontos, para o uso pelo cego e organizou para eles seu primeiro livro completo sobre o seu sistema.

Poucos anos depois, ele, Gabriel Gauthier e um outro amigo cego e um aluno antigo, Hippolyte Cotat, tornariam-se os primeiros cegos professores titulares da escola. Isso quer dizer que eles podiam sair da escola ocasionalmente sem pedir a permissão de alguém, ter os seus próprios quartos, e ostentavam fitas em seus uniformes escolares como marca de posição social.

Todos três professores usavam o novo alfabeto em todas as aulas.

No mesmo ano, Louis Braille foi convocado para o serviço militar e foi representado pelo seu pai.

O registro desse encontro sobreviveu e mostra que Louis foi isento do exército Francês porque era cego, por conseguinte, "não podia ler nem escrever," uma nota irônica para alguém que grandemente tinha resolvido um dos maiores problemas da alfabetização antes de terminar a sua adolescência.

Passando a maior parte da sua vida no úmido, sujo e frio edifício escolar e vivendo numa dieta pobre provavelmente levaria Louis a desenvolver uma tuberculose na metade dos seus vinte anos. O diagnóstico não o surpreendeu. Há anos seus colegas estudantes adoeceram em tão grande número que um visitante reclamou dizendo que os estudantes mal podiam suportar muito tempo à míngua tossindo e respirando com dificuldade, e funerais de estudantes era uma ocorrência tristemente freqüente.

Pelo resto de sua vida, Louis teria períodos de saúde e energia entremeados e colapsos quase fatais.

Ainda assim, apesar de sua doença, o cargo pedagógico e muitos empregos tocando órgão, ele continuou trabalhando no refinamento do código.

Embora o Francês não use a letra "w", Louis acrescentou-a mais tarde a pedido de um estudante Inglês, o filho cego do Senor George Hayter, pintor e retratista da família real Britânica.

Ele trabalhou árduo num código musical Braille também, provavelmente estimulado não só pelas suas habilidades musicais, mas também por aquelas dos seus amigos.

Gabriel Gauthier, que também adoeceu com tuberculose, era compositor tanto quanto organista, que eventualmente produziria seus próprios trabalhos entre os primeiros volumes da música Braille.

PRIMEIRA "IMPRESSÃO NO SISTEMA BRAILLE"

Louis era um professor muito popular, generoso tanto com tempo quanto com dinheiro, em ajudar seus alunos.

Ele dava muitos presentes pessoais e empréstimos do seu pequeno salário para ajudá-los a comprar roupas quentes e comida melhor.

Ele também economizou bastante para comprar um piano para si mesmo de maneira que ele pudesse praticar quando desejasse.

Porque os estudantes não tinham jeito de escrever para casa aos seus familiares sem ditar a carta para um professor vidente, Louis inventou a raphigraphy, que representa o alfabeto com letras grandes impressas composto de pontos Braille.

A raphigraphy era um sistema laborioso intensivo; para fazer uma letra impressa em alto relevo - só a letra "i" o braillista tinha que perfurar 16 pontos à mão.

Um cego inventor, François-Pierre Foucaut, foi um estudante da escola anterior no Quinze-vingts. Dias depois da Revolução, ele retornou em 1841 e quando viu o que Louis Braille estava fazendo, inventou uma máquina chamada "teclado de pistão", para perfurar os pontos completos das letras pressionando um único botão. Ironicamente, a primeira máquina de escrever foi feita em 1808 na Itália para ajudar uma condessa cega, Carolina Fantoni da Fivizzono, produz escrita legível para pessoas videntes, mas as máquinas de escrever não foram produzidas em nenhuma escala até o ano de 1870. Enquanto isso, o teclado de pistão (embora caro) tornou-se um aparelho comum na Europa.

Em 1834, o Dr. Pignier conseguiu um espaço para Louis demonstrar seu código na exposição da indústria de Paris, freqüentada por visitantes de toda parte do mundo.

O rei Louis Filipe da França presidiu a abertura da exposição e até falou com Louis sobre sua invenção, porém, como outros observadores contemporâneos, parecia não entender que o que ele via era essencialmente mais do que um truque divertido.

Louis revisou o livro do seu alfabeto em 1837, o mesmo ano em que a escola publicou o primeiro livro em Braille no mundo, história da França em três volumes.

O método publicado consistia de blocos de celas cheias. Enquanto arrumavam as páginas, os estudantes tiravam os pontos desnecessários de cada bloco para fazer as letras corretas.

(nota: a letra I em raphigraphy - 4 pontos horizontais em cima e em baixo, e 8 verticais em 2 filas no meio).

A imprensa na escola era administrada por Remi Fournier, o estudante que Valentin Haüy trouxe com ele na sua saída da França, quase trinta anos antes.

O QUE É MELHOR? E QUEM DECIDE?

Parece óbvio hoje que essas inspirações práticas devem ter sido vistas pelos grandes avanços importantes que tiveram.

Deve ter sido emocionante para os estudantes poder escrever e ler pela primeira vez com velocidade e precisão igualando-se às pessoas videntes e até mesmo mais rápidos de que certas pessoas e deve ter sido emocionante observar.

A extensão total desse triunfo enganou completamente as autoridades da época, todavia, o livro de Louis não era o mais divulgado projeto publicado na escola no ano de 1837.

O diretor assistente, P. Armand Dufau, um antigo professor de geografia da escola, publicou Considerações sobre o Cego sobre seus Estados Físico, Moral e Intelectual, com uma descrição completa dos meios para melhorar seu destino, instruções e trabalho.

O livro de Dufau ganhou o prestigioso prêmio da Académie Française, que um ano antes tinha sido dado a Aléxis de Tocqueville pelo seu famoso livro sobre a América.

Dufau, um forte oponente do Braille que acreditava que o Braille tornava o cego "muito independente," não fez menção da sua inovação subordinada em seu livro 

O prêmio da Academie significou que Dufau encontrou sua própria fortuna na elevação social, e usou sua nova influência numa boa causa.

Durante anos, relatos das autoridades médicas governamentais induzidas a visitarem a escola pelas súplicas constantes do Dr. Pignier, notaram que os estudantes lá sempre tinham a "aparência doentil" mas nada era feito.

Finalmente, em 1838, o poeta e historiador Alphonse de Lamartine em um passeio turístico pela escola, ficou horrorizado com as terríveis condições. Ele fez um forte apelo à Câmara dos deputados da França por um novo prédio, declarando: "Nenhuma descrição lhes daria uma idéia verdadeira desse edifício, que é pequeno, sujo e sombrio; daquelas passagens divididas para formar boxes dignificados pelo nome de oficinas ou salas de aulas, daquelas tortuosas, carcomidas escadarias... Se toda essa assembléia se levantasse agora e fosse em massa a esse lugar, o voto por essa causa seria unânime.!"

O discurso foi efetivo e começaram a fazer planos para um novo edifício escolar no centro da cidade, num local mais saldável no Boulevard dês Invalides.

Entretanto, enquanto o edifício estava em construção, Dufau coagia o Dr. Pignier a deixar a sua função como diretor, usando sua influência (e alguma imaginação) para convencer os ministros oficiais que o Dr. Pignier estava ensinando história com uma inclinação imprópria para o governo. O Dr. Pignier era vulnerável; ele teve uma educação católica na sua juventude (sempre suspeito na França pós revolucionária), e ele criou problemas crônicos com as autoridades adotando o código Braille e melhorando as pobres condições do edifício.

A piora da saúde de Louis forçou-o a rejeitar um emprego num local montês que poderia até mesmo ter alongado a sua vida se ele tivesse tido o vigor de empreender a viagem - tutor de um príncipe cego da família real Austríaca. Finalmente, ele tirou uma longa licença para recuperar a força em Coupvray.

Exausto pelo encargo do trabalho e e a luta frustrante para ter sua invenção aceita, ele descansava em casa enquanto seus amigos na escola escrevia-lhes resolutamente cartas animadoras. Quando Louis retornou à escola em Outubro de 1843, ele descobriu que estava para suportar outra derrota. Dufal estava implacável no trabalho fazendo ainda mais mudanças, tirando do curriculum assuntos como história, latim e geometria, deixando tempo para mais trabalho relacionado ao treinamento.

Uma vez que ganhou o prêmeo prestigioso alguns anos anteriores e engendrando a remoção de Pignier, Dufal teve apoio oficial suficiente para obter um grande aumento no orçamento da escola.

Ele decidiu revolucionar o meio de leitura padrão da escola - não usando o código Braille e sim adotando um sistema britânico inventado por John Alston do Asilo para o Cego em Glasgow. Outro sistema de impressão tátil.

Alston diferia de Haüy porque usava formas de letras bem simplificadas.

Alston imprimiu uma bíblia inteira (em 19 volumes) usando esse novo sistema há poucos anos antes, e Dufal gostou muito.

UM LIVRO QUEIMADO E UMA REBELIÃO

Para dramatizar e reforçar o novo sistema, Dufal fez uma fogueira no pátio de trás da escola e queimou não somente os livros em alto relevo criados pelos processos originais de Haüy, como também todo o livro impresso ou transcrito à mão no novo código de Louis - a biblioteca inteira da escola e o produto de quase cinqüenta anos de trabalho.

Para ter certeza que o Braille nunca mais seria usado novamente na escola, ele também queimou e confiscou as regletes, os punções e outros equipamentos de escrita Braille

Enfurecidos, os estudantes se rebelaram. Pelas costas de Dufal, eles escreviam o Braille mesmo sem regletes - enviando mensagens e mantendo diários secretos escritos com agulhas de crochê, garfos, ou pregos.

Os castigos de Dufal para quem usasse o Braille, que incluía levar palmadas nas mãos e ser mandado para cama sem jantar, foram completamente ineficazes.

Os estudantes mais velhos ensinavam o sistema aos estudantes mais jovens em segredo. O Braille uma vez aprendido seria impossível ser suprimido.

Finalmente, o assistente experto de Dufal, Joseph Guadet, havia oobservado os estudantes e tornou-se um um ardente protetor do Braille. Aprendeu sozinho a ler e escrever o código.

Ele persuadiu Dufal falando que se as pessoas poderosas do governo ouvissem que os estudantes estavam unidos obstinadamente desafiando a autoridade de Dufal, seu emprego poderia estar correndo risco.

Porém, se um estudante inventasse algo próspero, a escola compartilharia o crédito que Só poderia aumentar a reputação de seu líder.

Assim, quando a escola passou a seu novo edifício em novembro de 1843, PÁG.

Armand Dufau era um homem mudado, provendo cada estudante com uma Nova Reglete Braille.

Eufóricos por terem vencido a proibição do Braille, os estudantes fizeram uma petição e enviaram ao governo nomeando Louis Braille para a Legião Francesa de Honra por fazer com que a verdadeira comunicação seja possível ao cego.

Todavia, a petição foi ignorada.

A INVERSÃO DA FORTUNA

O triunfo público de Louis finalmente viria na dedicação de cerimônia do edifício no Fevereiro próximo.

Dufal descreveu brilhantemente o sistema de escrita com pontos elevados de Louis Braille para o povo, até havia uma estudante (uma das garotas novatas admitidas) que deu uma demonstração.

Um oficial na platéia gritou dizendo que isso tudo era um truque, que a criança que lia e escrevia em Braille deveria ter memorizado o texto antecipadamente.

Em resposta, Dufal pediu ao homem que encontrasse algum material escrito no seu bolso, que era um imgresso de teatro, e lê-lo para a estudante Braillista.

A garotinha reproduziu o texxto e o leu sem falhas antes mesmo que o homem tivesse chegado a sua cadeira.

O povo se convenceu, aplaldiu bastante por seis minutos completos.

Louis Braille passou os últimos oito anos da sua vida ensinando ocasionalmente e produzindo livros em Braille para a biblioteca da escola; escola onde ele batalhou com sua saúde declinada.

As pessoas estavam começando a chamar o sistema de pontos pelo seu nome, "Braille," e um número crescente de investigações sobre isto estava chegando à escola do mundo inteiro.

Quando Dufal publicou a segunda edição do seu influente livro em 1850, ele dedicou várias páginas com entusiasmo ao sistema Braille.

Ainda quando Louis Braille morreu em 6 de Janeiro de 1852, justamente dois dias após o seu quadragésimo aniversário, nenhum único jornal de Paris noticiou sua morte. Seu sistema sobreviveu, e em 1854, a França adotou o Braille como o seu sistema oficial de comunicação para as pessoas cegas.

O sistema Braille se espalhou pela Suíça logo depois mas encontrou tremenda resistência na Inglaterra, Alemanha e América, e sempre pela mesma razão: A falta de semelhança às letras impressas.

Pelo fato que os cegos poderiam querer escrever porque tinham algo para dizer, tanto quanto ler o que as outras pessoas escrevem, inacreditavelmente parecia nunca ter ocorrido para muitos desses educadores.

O fator escrita - Braille é fácil de escrever, enquanto formas de letras em alto relevo são virtualmente impossíveis - foi um enorme ponto que assegurou a duração do Braille na vida dos seus usuários.

Posteriormente, uma leitora de Braille, Helen Keller, escreveu:

"O Braille tem sido a ajuda mais preciosa para mim de muitas formas. Ele tornou possível a minha ida a faculdade - era o único método pelo qual eu podia tomar notas de palestras. Todas as minhas provas eram copiadas para mim nesse sistema. Eu uso Braille como a aranha usa sua teia - para captar os pensamentos que deslizam através da minha mente para falas, mensagens e manuscritos...

Se Louis Braille alguma vez tivesse tido o tempo para escrever os seus próprios pensamentos em resolver problemas, lidando comdificuldades, e perseverando através dos retrocessos, poucos discordariam que seria uma estória que valeria a pena ser lida.

Curiosamente, muitos educadores de cegos parecem ter feito uma missão altamente pessoal de inventar códigos contraditórios com aparentemente pouca consideração para as suas implicações práticas. Adesão feroz se desenvolveu sobre essessistemas de códigos. O Reino Unido parece ter sido a única exceção.

Thomas Rhodes Armitage, um físico rico que lutou com problemas de visão com ele mesmo, juntou-se a um comitê de outras pessoas cegas "com conhecimento de pelo menos três sistemas de símbolos em alto relevo e não teve interesse financeiro em nenhum" para evoluir os códigos e decidir qual seria o melhor para a Gran Bretanha.

O comitê deliberou durante dois anos, umas dúzias de leitores cegos sobreviveram e dois anos depois, em 1870, o Braille ganhou, embora levasse muitos anos ainda antes de ser totalmente implementado.

Os Estados Unidos só veio a usar o Braille completamente no século XX. Enquanto muitos códigos competentes não prosperaram passou o final do século XIX, os inovadores que eles atraíam geralmente se moviam para a publicação de Braille de maneiras inesperadas.

William Bell Wait, superintendente do INSTITUTO PARA O CEGO DE NOVA YOURK, apresentou um código agora quase esquecido chamado "ponto de nova yourk" em 1868.

Mais estável, Wait deu um eloqüente discurso no Comitê de Educação do Senado que ajudou a assegurar a primeira garantia anual do Congresso para livros em alto relevo para os cegos em 1879, assim garantindo um importante canal financeiro por cem anos de publicação para os cegos nos Estados Unidos.

Obsecado por economizar papel Braille, Wait, também criou o primeiro processo mecânico de imprimir em alto relevo simultaneamente dos dois lados para o PONTO DE NOVA YOURK no ano de 1890, dobrando a capacidade de informação que leva cada folha de papel num livro em Braille, assim inventou interpoint.

Em 1860, o primeiro instituto Americano a adotar o Braille foi, ironicamente, a ESCOLA PARA O CEGO, DE MISSOURI Localizado em São Lois - uma cidade com o nome de Lois IX da França, fundador do hospício Quinze-Vingts em Paris 600 anos antes

TEMPOS MODERNOS

O Quinze-vingts ainda existe hoje e agora é um hospital oftalmológico de alta tecnologia, bem como residência para os cegos.

Ironicamente, como Santa Apolônia aliviava dor de dente, e Santo Eutropius hidropisia, o propósito especial de Santo Ovid era curar surdez.

As baias de madeira e bancos usados na Feira de Santo Ovid foram destruídos pelo fogo em 1777.

Em 1792 a praça onde tinha acontecido foi renomeada "Praça de la Revolution" e por volta de 1793, o único espetáculo lá era a Guilhotina. Cerca de 1000 execuções foram realizadas lá, inclusive aquelas do rei Lois XVI e a rainha Marie Antoinette.

O edifício original do INSTITUTO REAL PARA AS CRIANÇAS CEGAS serviu como barracas do exército e depois um amazém. Foi derrubado no ano de 1930 e substituído por uma agência dos correios, agora desaparecida.

O último edifício que Louis Braille teria conhecido e onde morreu na Rue dês Invalides ainda é o local da Escola para o cego, hoje.

Valentin Haüy é um dos maiores humanitários (juntando-se entre outros, Abraham Lincoln, São Francisco de Assis, e Florence Nightingale) imortalizados nas esculturas de pedra que enfeitam a igreja de Riverside da cidade de Nova York.

Sua vida e trabalho também são lembrados num muuseu na Rue Duroc na moderna Paris, aberto Terças e quartas-Feiras, a partir das 2:30 da tarde, fechado de primeiro de Julho a 15 de Setembro anualmente. A entrada é grátis.

Louis Braille também não foi o único aluno experiente da escola dos dias antigos. Em 1830, Claude Montal, o primeiro cego afinador de piano e graduado pela Escola para o cego, começou a sua carreira em Paris.

Em 1834 ele publicou "COMO AFINAR O SEU PIANO VOCÊ MESMO" e continuou, abrindo a sua própria loja.

A escola também produziu um grupo de organistas famosos do mundo sem precedentes, que continua vivo através da memória de nosso próprio tempo, incluindo Llouis Viene, André archal e Jean Languais. O presente organista de CATEDRAL DE NOTRE-DAME, Jean-Pierre Leguay, também é cego.

O testamento de Louis Braille, ditado a um tabelião menos que uma semana antes da sua morte, incluía legados não só para sua família, mas também para o criado que limpava o Seu quarto, o ajudante de enfermaria, e o guarda noturno na escola. Suas roupas e pertences pessoais foram para os seus estudantes como recordação.

Ele fez um pedido estranho, instruindo aos amigos que queimassem uma caixa pequena que havia em seu quarto sem abri-la.

Depois da sua morte, eles não resistiram a procurar e encontraram a caixa cheia de coisas com vales em Braille dos estudantes que tinham pedido dinheiro emprestado ao seu generoso professor. Finalmente, as notas foram queimadas conforme seu desejo.

Com a morte de Louis Braille, Hippolyte Coltat herdou seu piano e trabalhou muito para adiantar o seu legado.

Suas vivas lembranças de seu professor e amigo num serviço memorial da escola, serviu como a pprimeira biografia de Braille.

Um outro amigo de Louis Braille Gabriel Gauthier, só sobreviveria pouco tempo depois dele. Ele também morreu de tuberculose.

A casa dos Braille em Coupvray, ainda no local, também foi transformada num museu.

Louis Braille foi verdadeiramente enterrado numa sepultura simples, num pequeno cemittério na sua cidade natal.

Em 1952, no centésimo aniversário de sua morte, cresceu um sentimento público querendo que seus restos mortais fossem removidos para o Panteão em Paris, onde os heróis nacionais da França eram enterrados.

O prefeito de Coupvray protestou dizendo que Louis Braille foi uma verdadeira criança da área e que algo dele deve permanecerr em sua aldeia. Suas mãos foram separadas dos braços e reenterradas separadamente em Coupvray. O resto do seu corpo foi enterrado no Panteão seguido de uma enorme cerimônia pública em Sorbonne freqüentada por dignitários de toda parte do mundo, inclusive Helen Keler, que deu um discurso em que o NEW YOURK TIME reportou como "gramática Francesa sem falhas" ela declarou a centenas de outros leitores do Braille numa grande recepção entusiasmada que "Nós, os cegos, estamos tão indébito com Louis Braille como a humanidade está com Gutenberg".

Quando o caixão foi carregado pelas ruas de Paris, emdireção ao Panteão, centenas de bengalas brancas batiam juntas atrás. O que o Times chamou sem aparente sugestão de ironia "Uma estranha, heróica procissão."

O Panteão fica no quinto Distrito Municipal de Paris, a poucos blocos da escola original para o cego.

O sistema de escrita de Louis Braille finalmente espalhou-se pelo mundo e, claro, ficou conhecido pelo seu nome.

Curiosamente considerando que o pai de Louis era um fabricante de arreios e selas, há uma palavra Inglesa, brail, que descreve uma corda usada para velejar, e é derivada de uma palavra Francesa do século XV braiel, que significa "correia". Assim parece razoável especular que o nome de família provavelmente foi derivado da semelhante ocupação de um antepassado.

Apesar do fato de que os pontos Braille ainda não se assemelham as letras impressas (uma queixa freqüentemente ouvida por esses dias), ele tem sido adaptado para quase todas as línguas na terra e permanece o maior meio de leitura para as pessoas cegas emtodos os lugares.

Desfazendo o mito que o Braille de alguma forma é "muito difícil" para os videntes aprenderem, os transcritores videntes foram por muito tempo os primeiros recursos de livros de ensino para estudantes cegos.

Milhares desses voluntários aprenderam o Braille como uma distração e produziram livros escrevendo uma cela por vez, na mesa da cozinha e no quarto, escritório, por todo lugar com pequena fanfarra.

Seus esforços nos Estados Unidos tem, se qualquer coisa se expandiu, a última década com a vinda da Era do computador e o fluxo de estudantes cegos na escola pública. Se por tradutores de softwares ou entrada direta, o Braille mostrou ser bem adequado a produção ajudada por computador devido a sua elegância e eficiência.

A utilização do Braille para navegar e ler texto de computador em tempo real tornou-se inteiramente possível como também confiável.

Assim, a Era do computador criou uma explosão contínua e sem precedentes na quantidade do Braille lido e publicado no mundo inteiro.”

Quer conhecer mais? leia este artigo no original, em inglês