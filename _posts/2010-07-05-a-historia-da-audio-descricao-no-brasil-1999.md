---
layout: article
title: A História da Áudio-descrição no Brasil, 1999.
except: 
urlsource: 
author: Illiam Schiff e Morton A. Heller
extrainfo: 
date: 2010-07-05
categories: article
tags:
- Acessibilidade
- Tecnologia

---

“...Após a realização do projeto, esta pesquisa foi iniciada com o objetivo de compreender se e como a participação em atividades culturais, entendida como um direito de cidadania, pode favorecer a inclusão social de pessoas com necessidades específicas. Ou mais exatamente, compreender como o projeto vídeo-narrado contribuiu para a inclusão dos participantes no ambiente familiar, escolar, profissional e social. Entendendo aqui a “inclusão como o processo de um movimento dinâmico e permanente que reconhece a diversidade humana e tem como fundamento a igualdade na participação e na construção do espaço social, compreendida como um direito.” (Kauchakje, 1999:204)...”

“...Pôde-se perceber o fator inclusivo que a participação no projeto desencadeou na vida de todos os freqüentadores do vídeo-narrado, uma nova postura e uma busca de acesso à participação na vida social, reivindicando na escola fitas dubladas, freqüentando locadoras, despertando interesse por filmes transmitidos por canais de televisão, adquirindo aparelho de vídeo, discutindo sobre temas e cenas de películas em diferentes grupos sociais. Vejamos alguns depoimentos:
“Achei o projeto inovador, nunca tinha visto nada semelhante antes, achei positivo pelo lado cultural que os filmes traziam e, além disso, a oportunidade de se estar em grupo discutindo sobre o filme.”
“Conversei com a minha mãe sobre ‘A Primeira Vista’, que contava a história de um rapaz cego. Com meus sobrinhos comentei muito sobre o ‘Pixote’, porque são muito jovens e há o perigo das drogas, um deles acabou vendo o filme e ficou mais fácil a gente conversar”
“Agora tenho mais vontade de ligar a televisão, antes era só rádio e faz falta assistir televisão, para conversar com os outros.” 

Leia este artigo na íntegra em: “Vendo filmes com o coração: O projeto vídeo-narrado”, 