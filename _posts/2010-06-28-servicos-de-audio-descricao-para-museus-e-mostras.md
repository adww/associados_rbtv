---
layout: article
title: Serviços de Áudio-Descrição para Museus e Mostras
except: 
urlsource: 
author: 
extrainfo: 
date: 2010-06-28
categories: article
tags:
- Acessibilidade
- Tecnologia

---

ou ler na Revista Brasileira de Tradução Visual
os artigos que tratam da temática da áudio-descrição.
Segundo a “Audio Description Solutions”, “A quantidade de áudio-descrição para museus e mostras depende do conteúdo daquilo que se está exibido. Também, o desenvolvimento da
tecnologia digital para a oferta da áudio-descrição influencia a
discussão a respeito da quantidade apropriada de áudio-descrição que deve ser oferecida.

Com o acesso, cada vez maior, a aparelhos que permitem repetir uma gravação, tantas vezes forem necessárias, o áudio-descritor deve descrever uma ampla amostra dos itens em exibição, de modo que as descrições estejam disponíveis, à medida que cada visitante faz suas escolhas pessoais em relação a que partes da amostra explorar e quais não.
Por exemplo, o total de áudio-descrição para uma dada mostra
de objetos e imagens triviais deve ter a duração de, ao menos, duas vezes a
quantidade de tempo que um visitante vidente típico passaria explorando o trabalho. Por outro lado, a áudio-descrição de uma casa de época (de significante importância histórica), ou de uma mostra que contenha artefatos de uma época, ou cultura que não sejam familiares à maioria dos visitantes iria provavelmente durar um total de pelo menos três ou quatro vezes a quantidade de tempo que um vidente típico gastaria visitando a exibição.

Se você quiser receber informações mais detalhadas sobre o
processo de preparação da áudio-descrição para museus e mostras, pode enviar uma solicitação por e-mail para: info@AudioDescriptionSolutions.com