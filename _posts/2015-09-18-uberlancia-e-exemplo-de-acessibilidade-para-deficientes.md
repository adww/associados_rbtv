---
layout: post
title: Uberlândia é exemplo de acessibilidade para deficientes
except: Com 600 mil habitantes, cidade tem rampas de acesso em todas esquinas. Projetos novos só são aprovados se tiver plano para facilitar a mobilidade.
urlsource: http://g1.globo.com/jornal-nacional/noticia/2015/09/uberlandia-e-exemplo-de-acessibilidade-para-deficientes.html
author: Jornal Nacional
date: 2016-02-22
categories: news
tags:
- Acessibilidade
- Tecnologia

---
Nesta semana, o Jornal Nacional está apresentando uma série especial de reportagens sobre o desrespeito do Brasil com as pessoas com deficiência. Mas, nesta sexta-feira (18), você vai ver como seria perfeitamente possível diminuir muito o problema da acessibilidade no nosso país. O exemplo é de uma cidade de Minas Gerais.

Cadeirantes para todo lado, cegos andando na rua sem esbarrar em nada, surdos interagindo com outras pessoas. Quem vê até pensa que essa cidade tem mais deficientes do que qualquer outra no Brasil. O que Uberlândia, no Triângulo Mineiro, tem mais que as outras, é acessibilidade.

“Hoje a pessoa tem a liberdade de ir e vir. Elas trabalham, estudam, tem lazer. Então, hoje elas saíram de casa”, afirmou Denise Rezende Faria, coordenadora da Associação Paraplégicos de Uberlândia.

Jornal Nacional: Mas, de onde veio toda essa consciência?

Denise: Isso foi, assim, uma luta árdua. Foi um dia a dia matando leão e onça todos os dias.

A luta da Associação dos Deficientes valeu a pena: Uberlândia, com 600 mil habitantes, tem rampas de acesso em todas esquinas, no centro e nos bairros, sem distinção: 100% da frota de ônibus com elevadores para quem tem dificuldade de locomoção. Piso tátil para orientar deficientes visuais em todas as calçadas, terminais rodoviários, lojas e prédios públicos. Lá, cada projeto novo de rua, prédio ou loteamento só é aprovado se tiver plano pra facilitar a mobilidade dos deficientes.

Esse programa de acessibilidade existe em Uberlândia há 20 anos, e vai muito além da infraestrutura. É um exercício diário de cidadania que começa nas escolas municipais, onde desde cedo alunos e professores aprendem a conviver com as diferenças.

Linguagem de sinais para quem é surdo poder acompanhar a aula. Máquina de braile, para quem é cego poder anotar tudo. Rampas para alunos cadeirantes e usuários de muletas e carinho, muito carinho.

“A Ana Júlia, ela surpreendeu, que ela é bem inteligente, bem esperta. Está sendo superfácil. Está aprendendo, começando a ler a escrever. Adora vir pra escola. Adora estudar”, comemorou Simone de Fátima Silva, mãe de aluna.

Quem sai de Uberlândia leva a pergunta: por que a acessibilidade tem que ser restrita a algumas cidades, uns poucos prédios, raríssimas iniciativas?

Em Araxá, no mesmo estado de Minas, o deficiente que entrou na Justiça contra uma companhia aérea por falta de acessibilidade em certos aviões precisou ser carregado dentro do fórum da cidade, para chegar à sala do juiz.

“É, no mínimo, irônico, porque a Justiça que deveria estar nos resguardando, cumprindo com a lei porque ela prega a lei e cobra dos outros, ela não seque o que ela está pregando. O que se fala não é o que se faz”, protestou Manoel Stefani Furtado, administrador de empresas.

Mas, justiça seja feita; na capital do Brasil, o prédio do STJ é considerado modelo. “É um paraíso. Aqui é um órgão que eu acho que Deus escolheu pra mim. Prestei concurso pro STJ e não sabia que tinha essa estrutura toda aqui para um cadeirante”, disse Fernanda Zago, técnica judiciária.

De um modo geral, todos os prédios do governo, do Legislativo e do Judiciário são acessíveis. Mas se a Fernanda precisar atravessar a rua para ir do Superior Tribunal de Justiça até o Tribunal Superior do Trabalho: "Não tem acessibilidade. A gente pensa: estou na capital do Brasil e aqui no plano piloto, do lado da Câmara e do Senado, junto dos três poderes, não é acessível", conta Fernanda.

Precisaria consciência e vontade política, segundo a Associação Brasileira de Normas Técnicas. A ABNT tem uma receita básica para ampliar a acessibilidade: rampas com pouca inclinação, no máximo 8%, e sempre construídas da porta para dentro, para não obstruir as calçadas. Escadas com corrimão, piso tátil, cerâmicas com retângulos e bolinhas em alto relevo e semáforo sonoro para guiar deficientes visuais. Transporte público com elevador e espaço exclusivo para cadeirantes, facilidades que podem fazer diferença não só para quem hoje precisa.

“O que a população esquece que é muito importante é que todos envelhecerão – aliás, é a vontade de todo mundo. E a pessoa, quando ficar velha, ela vai ter perda de mobilidade física, sensorial, visual, auditiva. Então, as pessoas não se preocupam com o futuro”, observou Edison Passafaro, instrutor e auditor das normas de acessibilidade da ABNT.

Segundo a ABNT, por maior que seja o custo para fazer tudo o que é necessário, esse preço nunca chegará nem perto do tamanho dos benefícios para todos.

O governo de Brasília reconheceu o problema da acessibilidade nas calçadas e afirmou que um programa de construção e recuperação está na fase final de orçamento. Sobre a falta de acesso para cadeirantes em Araxá, o Tribunal de Justiça de Minas Gerais declarou que vai abrir licitação no ano que vem para construir um fórum novo, com rampas.

*Confira a materia na integra*

[![Homem sentando na cadeira de rodas vestindo uma camisa roxa, calça jeans e vultos de pessoas caminhando de diversas direções][cadeirantes]][link-cadeirante]

[link-cadeirante]: http://g1.globo.com/jornal-nacional/noticia/2015/09/uberlandia-e-exemplo-de-acessibilidade-para-deficientes.html
[cadeirantes]: https://s02.video.glbimg.com/x360/4478370.jpg