---
layout: edition
title: Mãe relata como sua filha começou a aprender a desenhar e reconhecer desenhos táteis	
date: 2009-12-01
categories: edition 1
cover: 01.png
issue: 01
release: 2009
authors:
 - Silvana Aguiar 
tags:
--- 

_Prezado Editor,_

_Meu nome é Rosângela Gera. Sou médica e mãe de uma garotinha de sete anos que é cega._

_Gostaria de compartilhar com os demais leitores desta revista, minha experiência como mãe, vivenciando a escolarização da Laurae sua aprendizagem com desenhos, numa escola comum, aqui em Colatina, Espírito Santo._

_Como disse, a Laura é uma garotinha de 7 anos, que cursou no ano de 2009 o primeiro ano do ensino fundamental de 9 anos, já tendo concluído, portanto, a etapa daeducação infantil._

!['Foto 1: Laura com as   colegas na escola brincando com uma caixa surpresa, elas pegam um objeto dentro da caixa e tentam adivinhar o que é antes de retirá-lo.'][relato1]
<span>Foto 1: Laura com as   colegas na escola brincando com uma caixa surpresa, elas pegam um objeto dentro da caixa e tentam adivinhar o que é antes de retirá-lo.</span>

_Ela aprendeu a ler e escrever em braile, estudando na escola comum,junto com seus colegas, confirmando a assertiva de que a expectativa sobre uma criança cega quanto ao seu aprendizado pode ser a mesma de uma criança vidente, desde que para a primeira, sejam aplicadas as estratégias diferenciadas, necessárias para aquisição de habilidades._

_Desde que Laura entrou na escola, aos 2,5 anos, rasgar, cortar, colar, picotar, usar massinhas, palitinhos etc., fizeram parte do rico repertório de estímulos que recebe uma criança para desenvolver sua coordenaçãomotora, a concentração, atenção e tantas outras funções cognitivas. Porém, confesso que entre essas tantas atividades ofertadas a Laura para o seu desenvolvimento, uma lhe foi negligenciada, a do ato de desenhar, de levar a sério o seu potencial para reconhecerdesenhos._

_Eu não tinha (nunca recebi) informaçãosuficiente para acreditar que deveria investir nesse recurso com a minha filha._

_Por outro lado, na escola, as professoras de minha filha, até então nem sequer pensavam na possibilidade de que seria possível que ela tivesse êxito nessa habilidade._

!['Foto 2: Laura na escola em atividade de equilibrismo no projeto circo, andando em cima de uma corda e segurando um bastão.'][relato2]
<span>Foto 2: Laura na escola em atividade de equilibrismo no projeto circo, andando em cima de uma corda e segurando um bastão.</span>

_Na última semana de aula do ensino infantil, minha garotinha chegou em casa chorando, contando que os colegas lhe disseram que "cego só faz rabiscos ". _

_E foi assim, com ela empunhando o lápis como uma criança pequena que acabou de descobrir essa funcionalidadee com esse sentimento de impotência diante do ato de desenhar que começamos o ano letivo de 2009, primeiro ano dela no ensino fundamental._
 
_Mas a Laura desejava desenhar, queria usar o lápis de cor como seus colegas; a nova professora, com uma postura completamente diferente das professoras dos anos anteriores, acreditava que minha filha, mesmo tendo uma deficiência visual, poderia sim executar a atividade de desenhar e se propôs a estudar o tema para descobrir estratégias que permitissem ensinar essa habilidade à Laura._
 
!['Foto 3: Papel com linhas feitas em relevo, e um espaço entre elas de cerca de 3,0 cm e colocado em uma prancheta forrada que produz relevo no que é escrito sobre ela . Uma letra R em EVA foi colada no papel. E a Laura vai escrevendo a letra R observando o modelo e percebendo o traçado do que escreve.'][relato3]
<span>Foto 3: Papel com linhas feitas em relevo, e um espaço entre elas de cerca de 3,0 cm e colocado em uma prancheta forrada que produz relevo no que é escrito sobre ela . Uma letra R em EVA foi colada no papel. E a Laura vai escrevendo a letra R observando o modelo e percebendo o traçado do que escreve.</span>

_Começamos por aquilo que lhe motivava, o que desejava desenhar e uma atividade comum era que as crianças desenhassem na própria agenda referências sobre o tempo naquele dia, se estava ensolarado fazíamos um sol, se nublado, desenhávamos nuvens, e assim por diante._

_A professora, então, usou uma prancheta forrada com um material que proporcionava relevo aos riscos que executava, de maneira que Laura podia seguir com os dedos o desenho que ela havia feito. "Então o sol é redondo, e dele saem os raios." Concluia Laura ao examinar os desenhos da professora._

_De posse de um círculo, examinava seus contornos, contornava-o e em seguida repetia no papel.E começamos com as formas geométricas, descobrimos que delas várias figuras poderiam ser derivadas._

_Utilizamos material imantado com as formas geométricas de maneira que ela montava suas figuras numa tela antes de desenhá-las. Um dia fez um palhaço, seu chapéu que era um triângulo, seu corpo, como um quadrado, as pernas no formato de retângulos compridos, retângulos menores para os pése assim por diante._

!['Foto 5: Desenho de uma pessoa, feito cabeça, abdome, tórax e extremidades.'][relato5]
<span>Foto 5: Desenho de uma pessoa, feito cabeça, abdome, tórax e extremidades.</span>

_Também desenhou uma figura de mulher no dia das mães para ser colocado no mural e foi muito emocionante ver lá o seu desenho._

!['Foto 6: Desenho feito pela Laura para ser colocado no mural para o dia das mães. Descrição: O desenho foi feito numa folha de papel cor de rosa recortada na forma de coração. É a representação de uma mulher, cabeça, pescoço, tórax, abdome e extremidades com lápis de cera preto. A professora perguntou se iria deixar o seu desenho sem roupa então ela pintou o torax e abdome de amarelo e decidiu vestir calça então pintou de vermelho as pernas, depois pintou na região dos pés para fazer o calçado, também vermelho. Fez quatro traços marrons saindo da cabeça em direção vertical para baixo representando os cabelos. No canto superior esquerdo do papel está escrito meu nome Rosangela feito em braile.'][relato6]
<span>Foto 6: Desenho feito pela Laura para ser colocado no mural para o dia das mães. Descrição: O desenho foi feito numa folha de papel cor de rosa recortada na forma de coração. É a representação de uma mulher, cabeça, pescoço, tórax, abdome e extremidades com lápis de cera preto. A professora perguntou se iria deixar o seu desenho sem roupa então ela pintou o torax e abdome de amarelo e decidiu vestir calça então pintou de vermelho as pernas, depois pintou na região dos pés para fazer o calçado, também vermelho. Fez quatro traços marrons saindo da cabeça em direção vertical para baixo representando os cabelos. No canto superior esquerdo do papel está escrito meu nome Rosangela feito em braile.</span>

_Em pouco tempo, com tantos estímulos para desenvolver esta habilidade, elajá segurava no lápis apropriadamente e a professora sempre recomendava:_

_Rosângela não permita que Laura use o lápis de maneira incorreta em casa, porque ela já sabe como usá-lo, aqui na escola._
 
_Dessa maneira ela rapidamente aprendeu a reconhecer as letras em bastão em madeira e EVAdaí para escrevê-las também não demorou :elaas escreve com uma altura de cerca de 2,0cm e estamos em processo parareduzir suas dimensões._

_Também a utilização das formas geométricas para explicar o formato das letras foi essencial: "O A que é um triângulo cortado na metade e o V que é um triângulo de cabeça para baixo, sem o teto". "O B que é uma reta com dois semicírculos", e por aí foi"._

_Os seus livros passaram obrigatoriamente a ter que ter desenhos. Laura adora descobrir e saber como é o corpo do bichinho de quem falamos, o comprimento do rabo, o tamanho do chapéu, e quando não há desenhos,tal qual as outras crianças da sua idade, ela pergunta: "Esse livro não tem figura?"_

_E para que nenhum"desavisado"sobre o tema pergunte :"E pra que criança cega quer desenho ?"_

_Aqui, eu mesma tenho a resposta, pois é semelhante pergunta que faço quando vejo um livro adaptado para o braile (destinado a crianças cegas), sem as figuras e desenhos do livro original. São folhas e mais folhas impressas, transcritas, sem nenhum desenho, sem nada que possa despertar o interesse nesse sentido, e se fizéssemos o contrário ?,Se tirássemos os desenhos dos livros infantis que vão para as demais crianças? Alguma ia se interessar por ele?_

_Crianças cegas não são diferentes.Gostam de encontrar desenhos e outras figuras em seus livros e mais do que isso, precisam deles._

_Talvez a resposta seja a de que quem produz os livros não saiba de nada disso e já passou da hora de saber._

_A possibilidade de divulgar essas informações, de estudar mais sobre o tema, de descrever nossas experiências, e de poder demonstrá-las é fundamental para que esse assunto passe a fazer parte do cotidiano das escolas, das salas de aula, onde alunos cegos são excluídos de uma atividade tão prazerosa e significativa, como a do desenho._

_Não podemos continuar negando essa oportunidade a nossas crianças; elas não podem pagar o ônus da nossa ignorância sobre o tema. Eprecisamos desmistificar a idéia do professor que diz ao pequeno aluno que quer tocar em desenhos que acabaram de ser expostos no mural da sala:_

_"Tira a mão daí, menino. A gente vê com os olhos.!?!?!?"_

_Atenciosamente,_
_Rosângela Gera._


[relato1]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-01.jpg
[relato2]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-02.jpg
[relato3]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-03.jpg
[relato4]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-04.jpg
[relato5]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-05.jpg
[relato6]: {{site.baseurl}}/img/edition/01/rbtv-01-ed01-relato-experiencia-06.jpg