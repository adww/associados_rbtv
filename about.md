---
layout: page
title: Sobre
permalink: sobre
---

RBTV: Educação, Direito, Arte, Ciência e Tecnologia a serviço do Empoderamento da Pessoa com Deficiência.

A Revista Brasileira de Tradução Visual é uma publicação eletrônica, semestral, independente e de acesso gratuito.

Destina-se à divulgação Científica, Artística e Tecnológica, nos campos da Pesquisa e do Desenvolvimento, da Cultura e do Direito, das Letras/linguística e da Educação, da Psicologia e do Trabalho.

Publica temas que envolvem a arquitetura e o urbanismo, a fotografia, a pintura, a escultura, os desenhos e as figuras, em geral, bem como a produção/difusão da literatura, do cinema, da televisão e do teatro junto às pessoas com deficiência. São, ainda, temas abordados pela RBTV a Legendagem, a subtitulação, o “Closed Captioning”, a áudio-descrição, a transcrição pictórica para o sistema háptico, a produção/reconhecimento de padrões bidimensionais em relevo, a análise crítica de áudio-descrições, subtitulação e “closed captioning” feitas para filmes com tradução visual, entre outros temas do gênero.

---

**Pessoas**

- [Contato][pe1]
- [Equipe Editorial][pe2]
- [Comitê Editorial][pe3]
- [Conselho Editorial][pe4]
- [Equipe de Suporte Editorial][pe5]

**Políticas**

- [Foco e Escopo][p1]
- [Políticas de Seção][p2]
- [Processo de Avaliação por Pares][p3]
- [Política de Acesso Livre][p4]

**Submissões**

- [Submissões Online][su1]
- [Diretrizes para Autores][su2]
- [Declaração de Direito Autoral][su3]
- [Política de Privacidade][su4]

**Outro**

- [Patrocínio da Revista][ou1]
- [Mapa do Portal][ou2]
- [Sobre o Sistema de Publicação Eletrônica de Revistas][ou3]

---

## Políticas

### Foco e Escopo
A Revista Brasileira de Tradução Visual é uma publicação eletrônica, trimestral, independente e de acesso gratuito. Destina-se à divulgação científica, artística e tecnológica, nos campos da pesquisa e do desenvolvimento, da educação e do trabalho.

Conta com o apoio e colaboração de vários e reconhecidos profissionais (das áreas da ciência, da arte, da educação, do trabalho, dentre outras) que se dedicam às questões que envolvem as pessoas com deficiência, quanto às barreiras atitudinais, comunicacionais, programáticas, tecnológicas, metodológicas e outras que limitam e/ou impedem as pessoas com deficiência ao acesso à comunicação na cultura, na arte e na educação em geral.

Faz parte dos objetivos da Revista Brasileira de Tradução Visual a publicação de temas que envolvem a fotografia, a pintura, a escultura, os desenhos e as figuras, em geral, bem como a produção/difusão do cinema, da televisão e do teatro junto às pessoas com deficiência. Mais especificamente, serão temas abordados pela RBTV a Legendagem, a subtitulação, o “Closed Captioning”, a áudio-descrição, a transcrição pictórica para o sistema háptico, a produção/reconhecimento de padrões bidimensionais em relevo, a análise crítica de áudio-descrições, subtitulação e “closed captioning” feitas para filmes com tradução visual, entre outros temas do gênero.

Apenas serão aceitos para publicação na Revista Brasileira de Tradução Visual (RBTV), as produções que estejam relacionadas com o tema desta Revista e que tenham passado pela aprovação de, pelo menos, um avaliador, conforme descrito no item Política de Avaliação e em consonância com as Políticas de Seção.

---

## Políticas de Seção

### Editorial

Refere-se a assuntos selecionados em cada número da Revista RBTV pela sua importância para a comunidade científica. Os artigos desta categoria são redigidos pelo Editor chefe ou encomendados a especialistas de destaque na área de interesse.

Selecionado Submissões Abertas	Não Selecionado Indexado	Não Selecionado Avaliado por Pares

### Cartas ao Editor

Diálogo Científico em que se promovem comentários, críticas ou discussões a respeito dos temas abordados nos artigos publicados na própria Revista, ou que, tendo relação com o conteúdo desta, venham versar sobre outros temas de interesse geral. É possível uma réplica dos autores do artigo em comento, a qual será publicada junto à carta, ou em número subsequente da revista. O artigo, ao ser submetido nesta categoria, deve informar que é “ carta ao editor” (código 04).

- [x] Selecionado submissões abertas
- [] Selecionado indexado
- [] Selecionado avaliado por pares

### Seção Principal

Para esta seção, são aceitos textos nos seguintes formatos:

**Artigos originais** - Os artigos originais são artigos que não foram submetidos a nenhuma outra revista ou meio de publicação, seja na forma de comunicação oral, impressa ou de outra natureza. Os artigos originais devem conter, sempre que aplicável, as seguintes seções: Introdução, Métodos, Resultados, Discussão, Conclusões, Referências, Resumo e Abstract. O número de referências não deve exceder a quarenta. O artigo, ao ser submetido nesta categoria, deve informar que é "artigo original” (código 01). 

Artigos de revisão (a critério do corpo editorial da Revista) - Avaliações críticas e ordenadas da literatura de temas de importância científica são solicitadas para renomado Especialista. A Revista RBTV também aceita artigos de revisão enviados pela comunidade científica em geral, desde que constitua assunto de interesse para os leitores da RBTV e esteja dentro do padrão de qualidade da Revista. As Referências desses artigos devem ser atuais, preferencialmente publicadas nos últimos dez anos, em número máximo de quarenta referências. O artigo, ao ser submetido nesta categoria, deve informar que é “artigo de revisão” (código 02).

**Comunicação breve** - Pequenas experiências que tenham caráter de originalidade ou que sejam síntese de pesquisa original. O artigo, ao ser submetido nesta categoria, deve informar que é “comunicação breve” (código 03).

**Imagem, ciência e tecnologia** - análise da comunicabilidade/acessibilidade das Imagens – exame/análise crítico de imagens representadas por diversos meios – fotos, filmes, pinturas, desenhos, gravuras etc.; ciência e tecnologia aplicada ao tema da imagem e sua relação com a tradução visual; exame de equipamentos e softwares nessa área etc. O artigo, ao ser submetido nesta categoria, deve informar que é "Imagem, ciência & tecnologia” (código 05).

Artigo de reflexão ou de ponto de vista – É a apresentação de um artigo relevante publicado na especialidade. Especialistas são convidados, pelo corpo editorial, para comentar a respeito do referido artigo, seus pontos positivos e negativos, seus aspectos metodológicos e conceituais, sua relevância e aplicabilidade tecno-científica-pedagógica. Artigos de reflexão também podem ser submetidos pela comunidade científica em geral. Devem apresentar no máximo dez referências. O manuscrito, ao ser submetido, deve informar que é “artigo de reflexão ou de ponto de vista” (código 06).

Ensaios - revisão teórica, revisão crítica de bibliografia temática ou de obra específica. O artigo, ao ser submetido nesta categoria, deve informar que é “ensaio” (código 07).

**Estudo de caso** - análise de conceitos, procedimentos ou estratégias de pesquisa ou intervenção de ferramentas adotadas em trabalhos na área de interesse da RBTV O artigo, ao ser submetido nesta categoria, deve informar que é “estudo de caso” (código 08).

**Relato de Caso** - Descrição de casos envolvendo alunos/clientes de serviços, ou situações singulares de aplicação didático-pedagógica. O texto em questão aborda os aspectos relevantes que devem ser comparados com os disponíveis na literatura científica, não ultrapassando dez referências. O artigo, ao ser submetido nesta categoria, deve informar que é “relato de caso” (código 09).

**Análise de Material Didático** - exame crítico de produções voltadas para o ensino de conteúdos específicos em que imagens sejam utilizadas (com ou sem acessibilidade), considerando diferentes áreas e níveis de ensino. O artigo, ao ser submetido nesta categoria, deve informar que é “análise de material didático” (código 10).

**Artigo Especial** - Artigo Especial são aqueles que embora não classificáveis nas categorias anteriormente descritas, são, a critério do corpo editorial, considerados relevantes na especialidade. Os artigos especiais devem ser submetidos pela comunidade científica em geral, ou solicitados a especialistas. O artigo, ao ser submetido nesta categoria, deve informar que é “artigo especial” (código 11).

- [x] Selecionado submissões abertas
- [x] Selecionado indexado
- [x] Selecionado avaliado por pares

### Conteúdo

**Traduções** – a RBTV aceita a publicação de traduções nos temas que aborda (prioritariamente as traduções de artigos recentes e de interesse relevante, desde que acompanhadas de autorização de seu(s) autor(es) ou da revista em que foram originalmente publicadas. O artigo, ao ser submetido nesta categoria, deve informar que é “Tradução” (código 12).

**Entrevistas** - apenas serão aceitas para publicação entrevistas de reconhecido valor acadêmico, científico, artístico ou tecnológico, condizentes com a proposta da RBTV O artigo, ao ser submetido nesta categoria, deve informar que é "Entrevista” (código 13)

**Resenhas** - As resenhas devem ser de obras relevantes ao tema da RBTV e só serão publicadas se estiverem em conformidade com os padrões de qualidade da Revista. O artigo, ao ser submetido nesta categoria, deve informar que é “Resenha” (código 14).

- [x] Selecionado submissões abertas
- [x] Selecionado indexado
- [x] Selecionado avaliado por pares

### Relato de Experiência

**Relatos de experiência** – descrição/narração e análise de experiências com tradução visual tátil, oral ou visual. Os artigos aceitos nesta categoria não têm de ser necessariamente formais/acadêmicos. Podem ser relatos de experiências desenvolvidas/vividas em ambientes educacionais, culturais, de lazer e outros. Estimula-se aqui que estudantes e público em geral submetam seus trabalhos. O artigo, ao ser submetido nesta categoria, deve informar que é “relato de experiência” (código 15)

- [x] Selecionado submissões abertas
- [x] Selecionado indexado
- [x] Selecionado avaliado por pares

### Foto-Descrição

**Relatos de experiência** – descrição/narração e análise de experiências com tradução visual tátil, oral ou visual. Os artigos aceitos nesta categoria não têm de ser necessariamente formais/acadêmicos. Podem ser relatos de experiências desenvolvidas/vividas em ambientes educacionais, culturais, de lazer e outros. Estimula-se aqui que estudantes e público em geral submetam seus trabalhos. O artigo, ao ser submetido nesta categoria, deve informar que é “relato de experiência” (código 15)

- [x] Selecionado submissões abertas
- [x] Selecionado indexado
- [x] Selecionado avaliado por pares

## Processo de Avaliação por Pares

Os artigos submetidos serão avaliados por pelo menos um parecerista de reconhecido conhecimento na área da submissão.

O avaliador será designado pelo editor, não tendo, o primeiro, contato com a identificação do submetente.

Ao avaliar, o especialista de área poderá indicar o artigo para publicação; fazê-lo com sugestões de mudanças ou recusar a publicação, sempre de acordo com os requesitos de avaliação disponíveis no item Diretrizes de Avaliação e em conformidade com demais critérios da revista.

## Política de Acesso Livre

Esta revista oferece acesso livre imediato ao seu conteúdo, seguindo o princípio de que disponibilizar gratuitamente o conhecimento científico ao público proporciona maior democratização mundial do conhecimento.

[pe1]: #
[pe2]: #
[pe3]: #
[pe4]: #
[pe5]: #
[p1]: #
[p2]: #
[p3]: #
[p4]: #
[su1]: #
[su2]: #
[su3]: #
[su4]: #
[ou1]: #
[ou2]: #
[ou3]: #